#include <gtest/gtest.h>

#include <glow/common/log.hh>

int main(int argc, char **argv)
{
    // disable verbose info log for tests
    glow::setLogMask(0xFF - (uint8_t)glow::LogLevel::Info);

    testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
