#pragma once

#include <gtest/gtest.h>
#include <glow/gl.hh>
#include <glow/glow.hh>
#include <GLFW/glfw3.h>

#define GLOW_TEST(Suite, Case)                                                                 \
    namespace glow                                                                             \
    {                                                                                          \
    void render##Suite##__##Case();                                                            \
    }                                                                                          \
    TEST(Suite, Case)                                                                          \
    {                                                                                          \
        ASSERT_TRUE(glfwInit());                                                               \
                                                                                               \
        glfwWindowHint(GLFW_VISIBLE, GLFW_FALSE);                                              \
        glfwWindowHint(GLFW_OPENGL_DEBUG_CONTEXT, GLFW_TRUE);                                  \
        auto window = glfwCreateWindow(512, 512, "Test " #Suite "::" #Case, nullptr, nullptr); \
                                                                                               \
        ASSERT_NE(window, nullptr);                                                            \
                                                                                               \
        glfwMakeContextCurrent(window);                                                        \
                                                                                               \
        ASSERT_TRUE(glow::initGLOW());                                                         \
                                                                                               \
        glow::render##Suite##__##Case();                                                             \
                                                                                               \
        glfwSwapBuffers(window);                                                               \
                                                                                               \
        glfwDestroyWindow(window);                                                             \
        glfwTerminate();                                                                       \
    }                                                                                          \
    void glow::render##Suite##__##Case()
