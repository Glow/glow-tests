#include "tests.hh"

#include <glm/glm.hpp>

#include <glow-extras/geometry/Cube.hh>

GLOW_TEST(ExtrasGeometry, Cube)
{
    struct MyCubeVertex
    {
        static std::vector<ArrayBufferAttribute> attributes()
        {
            return {
                {&MyCubeVertex::pos, "aPosition"}, //
                {&MyCubeVertex::normal, "aNormal"},
            };
        }

        glm::vec3 pos;
        glm::vec3 normal;

        MyCubeVertex() = default;
        MyCubeVertex(glm::vec3 p, glm::vec3 n, glm::vec3, glm::vec2) : pos(p), normal(n) {}
    };

    auto cube0 = geometry::Cube<>().generate();
    auto cube1 = geometry::Cube<MyCubeVertex>().generate();
}
