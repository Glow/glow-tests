#include "tests.hh"

#include <glow/common/log.hh>
#include <glow/common/str_utils.hh>

#include <glow-extras/assimp/Importer.hh>

#include <common/svg/svg_writer.hh>

#include <random>

#include <glm/glm.hpp>

#include <polymesh/Mesh.hh>
#include <polymesh/algorithms.hh>
#include <polymesh/algorithms/geodesics/gsf.hh>
#include <polymesh/algorithms/stats.hh>
#include <polymesh/debug.hh>
#include <polymesh/formats/obj.hh>
#include <polymesh/formats/off.hh>

TEST(PolyMesh, GeodesicNNF)
{
    using namespace polymesh;

    Mesh m;
    auto pos = m.vertices().make_attribute<glm::vec3>();
    read_off(glow::util::pathOf(__FILE__) + "/iphigenie_200k.off", m, pos);

    // m.assert_consistency();

    print_stats(std::cout, m, &pos);

    write_obj("/tmp/iphigenie.obj", m, pos);
}
