#pragma once

#include <glm/glm.hpp>

#include <common/svg/svg_writer.hh>

#include <polymesh/Mesh.hh>
#include <polymesh/algorithms/properties.hh>

/// writes a mesh to an svg
/// uses proj matrix to convert vertex positions to screen coordinates
/// (proj * v).xy / (proj * v).w is used as pixel coordinate
inline void write_mesh(svg::svg_writer& svg, polymesh::Mesh const& m, polymesh::vertex_attribute<glm::vec3> position, glm::mat4 const& proj)
{
    using namespace polymesh;
    using namespace svg;

    auto face_color = 0x03A9F4;
    auto halfedge_color = 0x000000;
    auto vertex_color = 0x512DA8;

    auto pos2D = [&](glm::vec3 p) {
        auto p4 = proj * glm::vec4(p, 1);
        p4 /= p4.w;
        return glm::vec2(p4.x, p4.y);
    };

    // write face bg
    for (auto f : m.faces())
    {
        std::vector<glm::vec2> vs;
        for (auto v : f.vertices())
            vs.push_back(pos2D(v[position]));
        svg << polygon(vs).fill_opacity(.3f).fill(face_color).stroke(0, 0, 0).stroke_width(1).stroke_dasharray({1, 3});
    }

    // write vertices
    for (auto v : m.vertices())
    {
        auto p = pos2D(v[position]);

        svg << circle(p, 5).fill(1, 1, 1).stroke(vertex_color).stroke_width(2);
    }

    // write halfedges
    for (auto h : m.halfedges())
    {
        auto p0 = pos2D(h.vertex_from()[position]);
        auto p1 = pos2D(h.vertex_to()[position]);

        auto d = normalize(p1 - p0);
        auto n = glm::vec2(-d.y, d.x); // TODO: always show towards face centroid

        auto l0 = p0 + n * 5 + d * 30;
        auto l1 = p1 + n * 5 - d * 30;
        auto l2 = l1 + (n - d) * 5;
        svg << polyline({l0, l1, l2}).stroke(halfedge_color).stroke_width(2).fill(nullptr);

        auto c = (p0 + p1) / 2.0f;
        text::anchor_dir dir = text::middle;
        if (glm::abs(n.x) * 3 > glm::abs(n.y))
            dir = n.x < 0 ? text::end : text::start;
        auto offset = glm::vec2(0,0);
        if (n.y > 0)
            offset.y += n.y * 12;
        svg << text(c + n * 10 + offset, "h" + std::to_string(h.idx.value)).font_family("Arial").anchor(dir).fill(halfedge_color);
    }

    // write vertices
    for (auto v : m.vertices())
    {
        auto p = pos2D(v[position]);

        svg << text(p + glm::vec2(7, -7), "v" + std::to_string(v.idx.value)).font_family("Arial").fill(vertex_color);
    }

    // write faces
    for (auto f : m.faces())
    {
        auto p = pos2D(face_centroid(f, position));

        svg << text(p + glm::vec2(0, 5), "f" + std::to_string(f.idx.value)).font_family("Arial").anchor_middle().fill(face_color);
    }
}

inline void write_mesh(std::string const& filename, polymesh::Mesh const& m, polymesh::vertex_attribute<glm::vec3> position, glm::mat4 const& proj)
{
    svg::svg_writer svg(filename);
    write_mesh(svg, m, position, proj);
}
