#include "tests.hh"

#include <fstream>

#include <glow/objects/Shader.hh>

GLOW_TEST(Shader, Create)
{
    auto vs = Shader::createFromSource(GL_VERTEX_SHADER, "#version 430 core\n"
                                                   "void main() { }");
    ASSERT_TRUE(vs->isCompiledWithoutErrors());

    auto fs = Shader::createFromSource(GL_FRAGMENT_SHADER, "#version 430 core\n"
                                                     "void main() { }");
    ASSERT_TRUE(fs->isCompiledWithoutErrors());

    auto gs = Shader::createFromSource(GL_GEOMETRY_SHADER, "#version 430 core\n"
                                                     "void main() { }");
    ASSERT_TRUE(gs->isCompiledWithoutErrors());

    auto tcs = Shader::createFromSource(GL_TESS_CONTROL_SHADER, "#version 430 core\n"
                                                          "void main() { }");
    ASSERT_TRUE(tcs->isCompiledWithoutErrors());

    auto tes = Shader::createFromSource(GL_TESS_EVALUATION_SHADER, "#version 430 core\n"
                                                             "void main() { }");
    ASSERT_TRUE(tes->isCompiledWithoutErrors());

    auto cs = Shader::createFromSource(GL_COMPUTE_SHADER, "#version 430 core\n"
                                                    "void main() { }");
    ASSERT_TRUE(cs->isCompiledWithoutErrors());
}

GLOW_TEST(Shader, FromFile)
{
    {
        std::ofstream("/tmp/shader.glsl") << "#version 430 core\n"
                                             "void main() { }\n";
    }

    auto vs = Shader::createFromFile(GL_VERTEX_SHADER, "/tmp/shader.glsl");
    ASSERT_TRUE(vs->isCompiledWithoutErrors());
}
