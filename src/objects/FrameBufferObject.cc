#include "tests.hh"

#include <glow/objects/Framebuffer.hh>
#include <glow/objects/Texture2D.hh>
#include <glow/objects/TextureRectangle.hh>

GLOW_TEST(Framebuffer, Binding)
{
    auto fbo0 = Framebuffer::create();
    ASSERT_EQ(Framebuffer::getCurrentBuffer(), nullptr);

    {
        auto bfbo0 = fbo0->bind();
        ASSERT_EQ(Framebuffer::getCurrentBuffer(), &bfbo0);

        auto fbo1 = Framebuffer::create();
        auto fbo2 = Framebuffer::create();
        ASSERT_EQ(Framebuffer::getCurrentBuffer(), &bfbo0);

        {
            auto bfbo1 = fbo1->bind();
            ASSERT_EQ(Framebuffer::getCurrentBuffer(), &bfbo1);

            auto bfbo2 = fbo2->bind();
            ASSERT_EQ(Framebuffer::getCurrentBuffer(), &bfbo2);
        }

        ASSERT_EQ(Framebuffer::getCurrentBuffer(), &bfbo0);
    }

    ASSERT_EQ(Framebuffer::getCurrentBuffer(), nullptr);
}

GLOW_TEST(Framebuffer, Attach)
{
    auto tex1 = Texture2D::create();
    auto tex2 = Texture2D::create();
    auto fbo = Framebuffer::create();
    auto bfbo = fbo->bind();
    bfbo.attachColor("A", tex1);
    bfbo.attachColor("B", tex2);

    ASSERT_TRUE(bfbo.checkComplete());
}

GLOW_TEST(Framebuffer, AttachCtor)
{
    auto tex1 = Texture2D::create();
    auto tex2 = Texture2D::create();
    auto fbo = Framebuffer::create({{"A", tex1}, {"B", tex2}});
    auto bfbo = fbo->bind();

    ASSERT_TRUE(bfbo.checkComplete());
}

GLOW_TEST(Framebuffer, StorageImmutable)
{
    auto tex = TextureRectangle::createStorageImmutable(4, 4, GL_RGBA32F);
    auto fbo = Framebuffer::create();
    auto bfbo = fbo->bind();
    bfbo.attachColor("A", tex);

    ASSERT_TRUE(bfbo.checkComplete());
}
