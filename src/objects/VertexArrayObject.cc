#include "tests.hh"

#include <glow/objects/ArrayBuffer.hh>
#include <glow/objects/Framebuffer.hh>
#include <glow/objects/Program.hh>
#include <glow/objects/Shader.hh>
#include <glow/objects/Texture2D.hh>
#include <glow/objects/VertexArray.hh>

#include <glow/util/LocationMapping.hh>

GLOW_TEST(VertexArray, Binding)
{
    auto vao0 = VertexArray::create();
    ASSERT_EQ(VertexArray::getCurrentVAO(), nullptr);

    {
        auto bvao0 = vao0->bind();
        ASSERT_EQ(VertexArray::getCurrentVAO(), &bvao0);

        auto vao1 = VertexArray::create();
        auto vao2 = VertexArray::create();
        ASSERT_EQ(VertexArray::getCurrentVAO(), &bvao0);

        {
            auto bvao1 = vao1->bind();
            ASSERT_EQ(VertexArray::getCurrentVAO(), &bvao1);

            auto bvao2 = vao2->bind();
            ASSERT_EQ(VertexArray::getCurrentVAO(), &bvao2);
        }

        ASSERT_EQ(VertexArray::getCurrentVAO(), &bvao0);
    }

    ASSERT_EQ(VertexArray::getCurrentVAO(), nullptr);
}

GLOW_TEST(VertexArray, Locations)
{
    auto ab0 = ArrayBuffer::create();
    ab0->defineAttribute<float>("A");
    ab0->defineAttribute<float>("B");
    ab0->bind().setData({glm::vec2(0, 0)});

    auto ab1 = ArrayBuffer::create();
    ab1->defineAttribute<float>("D");
    ab1->defineAttribute<float>("C");
    ab1->bind().setData({glm::vec2(0, 0)});

    auto ab2 = ArrayBuffer::create();
    ab2->defineAttribute<float>("E");
    ab2->bind().setData({0.0f});

    auto vs0 = Shader::createFromSource(GL_VERTEX_SHADER, "#version 430 core\n"
                                                          "in float E;\n"
                                                          "in float B;\n"
                                                          "in float C;\n"
                                                          "in float A;\n"
                                                          "in float D;\n"
                                                          "out float x;\n"
                                                          "void main() { x = A + B + C + D + E; }");

    auto vs1 = Shader::createFromSource(GL_VERTEX_SHADER, "#version 430 core\n"
                                                          "in float E;\n"
                                                          "in float A;\n"
                                                          "in float D;\n"
                                                          "in float C;\n"
                                                          "out float x;\n"
                                                          "void main() { x = A + C + D + E; }");

    auto fs = Shader::createFromSource(GL_FRAGMENT_SHADER, "#version 430 core\n"
                                                           "in float x;\n"
                                                           "out float y;\n"
                                                           "out float z;\n"
                                                           "void main() { y = x; z = x / 2; }");

    auto prog0 = Program::create({vs0, fs});
    auto prog1 = Program::create({vs1, fs});

    auto vao0 = VertexArray::create({ab0, ab1, ab2});
    auto vao1 = VertexArray::create({ab2, ab1, ab0});

    auto tex1 = Texture2D::create();
    auto tex2 = Texture2D::create();
    auto fbo = Framebuffer::create({{"z", tex1}, {"y", tex2}});

    ASSERT_EQ(vao0->getAttributeMapping()->count(), 5);
    ASSERT_EQ(vao1->getAttributeMapping()->count(), 5);
    ASSERT_EQ(prog0->getAttributeMapping()->count(), 5);
    ASSERT_EQ(prog1->getAttributeMapping()->count(), 4);

    ASSERT_EQ(vao0->getAttributeMapping()->queryLocation("A"), 0);
    ASSERT_EQ(vao0->getAttributeMapping()->queryLocation("B"), 1);
    ASSERT_EQ(vao0->getAttributeMapping()->queryLocation("D"), 2);
    ASSERT_EQ(vao0->getAttributeMapping()->queryLocation("C"), 3);
    ASSERT_EQ(vao0->getAttributeMapping()->queryLocation("E"), 4);

    ASSERT_EQ(vao1->getAttributeMapping()->queryLocation("E"), 0);
    ASSERT_EQ(vao1->getAttributeMapping()->queryLocation("D"), 1);
    ASSERT_EQ(vao1->getAttributeMapping()->queryLocation("C"), 2);
    ASSERT_EQ(vao1->getAttributeMapping()->queryLocation("A"), 3);
    ASSERT_EQ(vao1->getAttributeMapping()->queryLocation("B"), 4);

    ASSERT_NE(prog0->getAttributeMapping()->queryLocation("A"), -1);
    ASSERT_NE(prog0->getAttributeMapping()->queryLocation("B"), -1);
    ASSERT_NE(prog0->getAttributeMapping()->queryLocation("C"), -1);
    ASSERT_NE(prog0->getAttributeMapping()->queryLocation("D"), -1);
    ASSERT_NE(prog0->getAttributeMapping()->queryLocation("E"), -1);

    ASSERT_NE(prog1->getAttributeMapping()->queryLocation("A"), -1);
    ASSERT_EQ(prog1->getAttributeMapping()->queryLocation("B"), -1);
    ASSERT_NE(prog1->getAttributeMapping()->queryLocation("C"), -1);
    ASSERT_NE(prog1->getAttributeMapping()->queryLocation("D"), -1);
    ASSERT_NE(prog1->getAttributeMapping()->queryLocation("E"), -1);

    ASSERT_EQ(fbo->getFragmentMapping()->queryLocation("z"), 0);
    ASSERT_EQ(fbo->getFragmentMapping()->queryLocation("y"), 1);

    // "draw"
    for (int i = 0; i < 10; ++i)
    {
        {
            auto p0 = prog0->use();

            vao0->bind().draw();
            ASSERT_EQ(vao0->getAttributeMapping(), prog0->getAttributeMapping());
        }

        {
            auto fb = fbo->bind();

            auto p1 = prog1->use();

            vao0->bind().draw();
            vao1->bind().draw();
        }
    }

    auto refMap = vao0->getAttributeMapping();
    ASSERT_EQ(refMap, vao0->getAttributeMapping());
    ASSERT_EQ(refMap, vao1->getAttributeMapping());
    ASSERT_EQ(refMap, prog0->getAttributeMapping());
    ASSERT_EQ(refMap, prog1->getAttributeMapping());

    ASSERT_EQ(prog1->getFragmentMapping(), fbo->getFragmentMapping());

    ASSERT_TRUE(refMap->queryLocation("A") >= 0);
    ASSERT_TRUE(refMap->queryLocation("B") >= 0);
    ASSERT_TRUE(refMap->queryLocation("C") >= 0);
    ASSERT_TRUE(refMap->queryLocation("D") >= 0);
    ASSERT_TRUE(refMap->queryLocation("E") >= 0);
}

GLOW_TEST(VertexArray, LocationsFuzzy)
{
    // TODO
}
