#include "tests.hh"

#include <fstream>

#include <glow/objects/Program.hh>
#include <glow/objects/Shader.hh>

GLOW_TEST(Program, Usage)
{
    auto vs = Shader::createFromSource(GL_VERTEX_SHADER, "#version 430 core\n"
                                                   "void main() { }");
    auto fs = Shader::createFromSource(GL_FRAGMENT_SHADER, "#version 430 core\n"
                                                     "void main() { }");

    auto prog0 = Program::create({vs, fs});
    ASSERT_EQ(Program::getCurrentProgram(), nullptr);

    {
        auto usedProg0 = prog0->use();
        ASSERT_EQ(Program::getCurrentProgram(), &usedProg0);

        auto prog1 = Program::create({vs, fs});
        auto prog2 = Program::create({vs, fs});
        ASSERT_EQ(Program::getCurrentProgram(), &usedProg0);

        {
            auto usedProg1 = prog1->use();
            ASSERT_EQ(Program::getCurrentProgram(), &usedProg1);

            auto usedProg2 = prog2->use();
            ASSERT_EQ(Program::getCurrentProgram(), &usedProg2);
        }

        ASSERT_EQ(Program::getCurrentProgram(), &usedProg0);
    }

    ASSERT_EQ(Program::getCurrentProgram(), nullptr);
}

GLOW_TEST(Program, UniformRestore)
{
    auto vs = Shader::createFromSource(GL_VERTEX_SHADER, "#version 430 core\n"
                                                   "void main() { }");
    auto fs = Shader::createFromSource(GL_FRAGMENT_SHADER, "#version 430 core\n"
                                                     "uniform float A;\n"
                                                     "out float f;\n"
                                                     "void main() { f = A; }");

    auto prog = Program::create({vs, fs});

    auto p = prog->use();
    auto const val = 1.2345f;
    p.setUniform("A", val);
    ASSERT_EQ(prog->getUniform<float>("A"), val);
    auto uniforms = prog->getUniforms();
    prog->link();
    p.setUniforms(uniforms);
    ASSERT_EQ(prog->getUniform<float>("A"), val);
}

GLOW_TEST(Program, FromFile)
{
    {
        std::ofstream("/tmp/shader.vsh") << "#version 430 core\n"
                                            "void main() { }";
        std::ofstream("/tmp/shader.fsh") << "#version 430 core\n"
                                            "void main() { }";
    }

    auto prog1 = Program::createFromFile("/tmp/shader.vsh");
    ASSERT_EQ(prog1->getShader().size(), 1u);
    ASSERT_EQ(prog1->getShader()[0]->getType(), (GLenum)GL_VERTEX_SHADER);

    auto prog2 = Program::createFromFile("/tmp/shader");
    ASSERT_GE(prog2->getShader().size(), 2u);

    auto vs = std::find_if(begin(prog2->getShader()), end(prog2->getShader()), [](SharedShader const& s)
                           {
                               return s->getType() == GL_VERTEX_SHADER;
                           });
    ASSERT_NE(vs, end(prog2->getShader()));

    auto fs = std::find_if(begin(prog2->getShader()), end(prog2->getShader()), [](SharedShader const& s)
                           {
                               return s->getType() == GL_FRAGMENT_SHADER;
                           });
    ASSERT_NE(fs, end(prog2->getShader()));

    auto prog3 = Program::createFromFiles({"/tmp/shader.vsh", "/tmp/shader.fsh"});
    ASSERT_EQ(prog3->getShader().size(), 2u);
    ASSERT_EQ(prog3->getShader()[0]->getType(), (GLenum)GL_VERTEX_SHADER);
    ASSERT_EQ(prog3->getShader()[1]->getType(), (GLenum)GL_FRAGMENT_SHADER);
}
