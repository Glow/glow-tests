#include "tests.hh"

#include <glm/vec4.hpp>

#include <random>

#include <glow/objects/Shader.hh>
#include <glow/objects/Program.hh>
#include <glow/objects/ShaderStorageBuffer.hh>

static std::string csSource = R"src(#version 430 core

layout(local_size_x = 1, local_size_y = 1, local_size_z = 1) in;

layout(std140) buffer ssb {
    vec4 data[];
};

void main() {
    uint x = gl_WorkGroupID.x;
    data[x] = 1.f - data[x];
}

)src";


GLOW_TEST(ShaderStorageBuffer, SimpleRandom)
{
    std::default_random_engine engine(123456);
    std::uniform_real_distribution<float> uniform01(0.f, 1.f);

    auto cs = Shader::createFromSource(GL_COMPUTE_SHADER, csSource);
    ASSERT_TRUE(cs->isCompiledWithoutErrors());

    auto prog = Program::create(cs);

    std::vector<glm::vec4> inData;
    for (int i = 0; i < 16; ++i)
        inData.push_back({uniform01(engine), uniform01(engine), uniform01(engine), uniform01(engine)});

    auto ssb = ShaderStorageBuffer::create();
    ssb->bind().setData(inData);
    prog->setShaderStorageBuffer("ssb", ssb);

    prog->use().compute(16);

    auto outData = ssb->bind().getData<glm::vec4>();

    for (int i = 0; i < 16; ++i)
        ASSERT_EQ(outData[i], 1.f - inData[i]);
}
