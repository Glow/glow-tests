// This file is auto-generated and should not be modified directly.
#include "tests.hh"

#include <glm/glm.hpp>

#include <glow/objects/TextureBuffer.hh>

GLOW_TEST(TextureBuffer, Binding)
{
    auto tex0 = TextureBuffer::create();
    ASSERT_EQ(TextureBuffer::getCurrentTexture(), nullptr);

    {
        auto btex0 = tex0->bind();
        ASSERT_EQ(TextureBuffer::getCurrentTexture(), &btex0);

        auto tex1 = TextureBuffer::create();
        auto tex2 = TextureBuffer::create();
        ASSERT_EQ(TextureBuffer::getCurrentTexture(), &btex0);

        {
            auto btex1 = tex1->bind();
            ASSERT_EQ(TextureBuffer::getCurrentTexture(), &btex1);

            auto btex2 = tex2->bind();
            ASSERT_EQ(TextureBuffer::getCurrentTexture(), &btex2);
        }

        ASSERT_EQ(TextureBuffer::getCurrentTexture(), &btex0);
    }

    ASSERT_EQ(TextureBuffer::getCurrentTexture(), nullptr);
}
