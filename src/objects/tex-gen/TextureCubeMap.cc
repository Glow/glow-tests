// This file is auto-generated and should not be modified directly.
#include "tests.hh"

#include <glm/glm.hpp>

#include <glow/objects/TextureCubeMap.hh>

GLOW_TEST(TextureCubeMap, Binding)
{
    auto tex0 = TextureCubeMap::create();
    ASSERT_EQ(TextureCubeMap::getCurrentTexture(), nullptr);

    {
        auto btex0 = tex0->bind();
        ASSERT_EQ(TextureCubeMap::getCurrentTexture(), &btex0);

        auto tex1 = TextureCubeMap::create();
        auto tex2 = TextureCubeMap::create();
        ASSERT_EQ(TextureCubeMap::getCurrentTexture(), &btex0);

        {
            auto btex1 = tex1->bind();
            ASSERT_EQ(TextureCubeMap::getCurrentTexture(), &btex1);

            auto btex2 = tex2->bind();
            ASSERT_EQ(TextureCubeMap::getCurrentTexture(), &btex2);
        }

        ASSERT_EQ(TextureCubeMap::getCurrentTexture(), &btex0);
    }

    ASSERT_EQ(TextureCubeMap::getCurrentTexture(), nullptr);
}
