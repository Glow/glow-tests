// This file is auto-generated and should not be modified directly.
#include "tests.hh"

#include <glm/glm.hpp>

#include <glow/objects/TextureCubeMapArray.hh>

GLOW_TEST(TextureCubeMapArray, Binding)
{
    auto tex0 = TextureCubeMapArray::create();
    ASSERT_EQ(TextureCubeMapArray::getCurrentTexture(), nullptr);

    {
        auto btex0 = tex0->bind();
        ASSERT_EQ(TextureCubeMapArray::getCurrentTexture(), &btex0);

        auto tex1 = TextureCubeMapArray::create();
        auto tex2 = TextureCubeMapArray::create();
        ASSERT_EQ(TextureCubeMapArray::getCurrentTexture(), &btex0);

        {
            auto btex1 = tex1->bind();
            ASSERT_EQ(TextureCubeMapArray::getCurrentTexture(), &btex1);

            auto btex2 = tex2->bind();
            ASSERT_EQ(TextureCubeMapArray::getCurrentTexture(), &btex2);
        }

        ASSERT_EQ(TextureCubeMapArray::getCurrentTexture(), &btex0);
    }

    ASSERT_EQ(TextureCubeMapArray::getCurrentTexture(), nullptr);
}
