// This file is auto-generated and should not be modified directly.
#include "tests.hh"

#include <glm/glm.hpp>

#include <glow/objects/Texture2D.hh>

GLOW_TEST(Texture2D, Binding)
{
    auto tex0 = Texture2D::create();
    ASSERT_EQ(Texture2D::getCurrentTexture(), nullptr);

    {
        auto btex0 = tex0->bind();
        ASSERT_EQ(Texture2D::getCurrentTexture(), &btex0);

        auto tex1 = Texture2D::create();
        auto tex2 = Texture2D::create();
        ASSERT_EQ(Texture2D::getCurrentTexture(), &btex0);

        {
            auto btex1 = tex1->bind();
            ASSERT_EQ(Texture2D::getCurrentTexture(), &btex1);

            auto btex2 = tex2->bind();
            ASSERT_EQ(Texture2D::getCurrentTexture(), &btex2);
        }

        ASSERT_EQ(Texture2D::getCurrentTexture(), &btex0);
    }

    ASSERT_EQ(Texture2D::getCurrentTexture(), nullptr);
}
