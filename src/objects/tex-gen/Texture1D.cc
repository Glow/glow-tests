// This file is auto-generated and should not be modified directly.
#include "tests.hh"

#include <glm/glm.hpp>

#include <glow/objects/Texture1D.hh>

GLOW_TEST(Texture1D, Binding)
{
    auto tex0 = Texture1D::create();
    ASSERT_EQ(Texture1D::getCurrentTexture(), nullptr);

    {
        auto btex0 = tex0->bind();
        ASSERT_EQ(Texture1D::getCurrentTexture(), &btex0);

        auto tex1 = Texture1D::create();
        auto tex2 = Texture1D::create();
        ASSERT_EQ(Texture1D::getCurrentTexture(), &btex0);

        {
            auto btex1 = tex1->bind();
            ASSERT_EQ(Texture1D::getCurrentTexture(), &btex1);

            auto btex2 = tex2->bind();
            ASSERT_EQ(Texture1D::getCurrentTexture(), &btex2);
        }

        ASSERT_EQ(Texture1D::getCurrentTexture(), &btex0);
    }

    ASSERT_EQ(Texture1D::getCurrentTexture(), nullptr);
}
