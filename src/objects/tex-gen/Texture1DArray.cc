// This file is auto-generated and should not be modified directly.
#include "tests.hh"

#include <glm/glm.hpp>

#include <glow/objects/Texture1DArray.hh>

GLOW_TEST(Texture1DArray, Binding)
{
    auto tex0 = Texture1DArray::create();
    ASSERT_EQ(Texture1DArray::getCurrentTexture(), nullptr);

    {
        auto btex0 = tex0->bind();
        ASSERT_EQ(Texture1DArray::getCurrentTexture(), &btex0);

        auto tex1 = Texture1DArray::create();
        auto tex2 = Texture1DArray::create();
        ASSERT_EQ(Texture1DArray::getCurrentTexture(), &btex0);

        {
            auto btex1 = tex1->bind();
            ASSERT_EQ(Texture1DArray::getCurrentTexture(), &btex1);

            auto btex2 = tex2->bind();
            ASSERT_EQ(Texture1DArray::getCurrentTexture(), &btex2);
        }

        ASSERT_EQ(Texture1DArray::getCurrentTexture(), &btex0);
    }

    ASSERT_EQ(Texture1DArray::getCurrentTexture(), nullptr);
}
