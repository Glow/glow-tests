// This file is auto-generated and should not be modified directly.
#include "tests.hh"

#include <glm/glm.hpp>

#include <glow/objects/Texture2DArray.hh>

GLOW_TEST(Texture2DArray, Binding)
{
    auto tex0 = Texture2DArray::create();
    ASSERT_EQ(Texture2DArray::getCurrentTexture(), nullptr);

    {
        auto btex0 = tex0->bind();
        ASSERT_EQ(Texture2DArray::getCurrentTexture(), &btex0);

        auto tex1 = Texture2DArray::create();
        auto tex2 = Texture2DArray::create();
        ASSERT_EQ(Texture2DArray::getCurrentTexture(), &btex0);

        {
            auto btex1 = tex1->bind();
            ASSERT_EQ(Texture2DArray::getCurrentTexture(), &btex1);

            auto btex2 = tex2->bind();
            ASSERT_EQ(Texture2DArray::getCurrentTexture(), &btex2);
        }

        ASSERT_EQ(Texture2DArray::getCurrentTexture(), &btex0);
    }

    ASSERT_EQ(Texture2DArray::getCurrentTexture(), nullptr);
}
