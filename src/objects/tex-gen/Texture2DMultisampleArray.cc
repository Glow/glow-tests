// This file is auto-generated and should not be modified directly.
#include "tests.hh"

#include <glm/glm.hpp>

#include <glow/objects/Texture2DMultisampleArray.hh>

GLOW_TEST(Texture2DMultisampleArray, Binding)
{
    auto tex0 = Texture2DMultisampleArray::create();
    ASSERT_EQ(Texture2DMultisampleArray::getCurrentTexture(), nullptr);

    {
        auto btex0 = tex0->bind();
        ASSERT_EQ(Texture2DMultisampleArray::getCurrentTexture(), &btex0);

        auto tex1 = Texture2DMultisampleArray::create();
        auto tex2 = Texture2DMultisampleArray::create();
        ASSERT_EQ(Texture2DMultisampleArray::getCurrentTexture(), &btex0);

        {
            auto btex1 = tex1->bind();
            ASSERT_EQ(Texture2DMultisampleArray::getCurrentTexture(), &btex1);

            auto btex2 = tex2->bind();
            ASSERT_EQ(Texture2DMultisampleArray::getCurrentTexture(), &btex2);
        }

        ASSERT_EQ(Texture2DMultisampleArray::getCurrentTexture(), &btex0);
    }

    ASSERT_EQ(Texture2DMultisampleArray::getCurrentTexture(), nullptr);
}
