// This file is auto-generated and should not be modified directly.
#include "tests.hh"

#include <glm/glm.hpp>

#include <glow/objects/TextureRectangle.hh>

GLOW_TEST(TextureRectangle, Binding)
{
    auto tex0 = TextureRectangle::create();
    ASSERT_EQ(TextureRectangle::getCurrentTexture(), nullptr);

    {
        auto btex0 = tex0->bind();
        ASSERT_EQ(TextureRectangle::getCurrentTexture(), &btex0);

        auto tex1 = TextureRectangle::create();
        auto tex2 = TextureRectangle::create();
        ASSERT_EQ(TextureRectangle::getCurrentTexture(), &btex0);

        {
            auto btex1 = tex1->bind();
            ASSERT_EQ(TextureRectangle::getCurrentTexture(), &btex1);

            auto btex2 = tex2->bind();
            ASSERT_EQ(TextureRectangle::getCurrentTexture(), &btex2);
        }

        ASSERT_EQ(TextureRectangle::getCurrentTexture(), &btex0);
    }

    ASSERT_EQ(TextureRectangle::getCurrentTexture(), nullptr);
}
