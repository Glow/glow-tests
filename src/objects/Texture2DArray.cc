#include "tests.hh"

#include <vector>

#include <glow/common/str_utils.hh>

#include <glm/ext.hpp>

#include <glow/data/SurfaceData.hh>
#include <glow/data/TextureData.hh>
#include <glow/objects/Texture2D.hh>
#include <glow/objects/Texture2DArray.hh>

GLOW_TEST(Texture2DArray, Data)
{
    auto tex = Texture2DArray::create();
    auto t = tex->bind();

    glm::ivec4 d[] = { {1,2,3,4} };
    t.setData(GL_RGBA32I, 1, 1, 1, d);
    ASSERT_EQ(t.getData<glm::ivec4>()[0], glm::ivec4(1, 2, 3, 4));

    t.setData(GL_R32I, 2, 2, 2, std::vector<int>({1, 2, 3, 4, 5, 6, 7, 8}));
    ASSERT_EQ(t.getData<int>(), std::vector<int>({1, 2, 3, 4, 5, 6, 7, 8}));
}

GLOW_TEST(Texture2DArray, File)
{
    auto tex0 = TextureData::createFromFile(util::pathOf(__FILE__) + "/textures/test-rgb.png", ColorSpace::Linear);
    auto tex1 = TextureData::createFromFile(util::pathOf(__FILE__) + "/textures/test-grey.png", ColorSpace::Linear);

    auto data0 = Texture2D::createFromData(tex0)->bind().getData<glm::vec3>();
    auto data1 = Texture2D::createFromData(tex1)->bind().getData<glm::vec3>();

    auto s0 = tex0->getSurfaces()[0];
    auto s1 = tex1->getSurfaces()[0];

    s0->setOffsetZ(0);
    s1->setOffsetZ(1);

    tex0->addSurface(s1);
    tex0->setTarget(GL_TEXTURE_2D_ARRAY);
    tex0->setDepth(2);

    auto tex = Texture2DArray::createFromData(tex0);
    auto data = tex->bind().getData<glm::vec3>();
    ASSERT_EQ(data0.size(), 3 * 2u);
    ASSERT_EQ(data1.size(), 3 * 2u);
    ASSERT_EQ(data.size(), 3 * 2 * 2u);

    for (auto i = 0u; i < data0.size(); ++i)
        ASSERT_EQ(data[i], data0[i]);
    for (auto i = 0u; i < data1.size(); ++i)
        ASSERT_EQ(data[i + data0.size()], data1[i]);
}
