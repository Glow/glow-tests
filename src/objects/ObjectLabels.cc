#include "tests.hh"

#include <glow/objects/ArrayBuffer.hh>
#include <glow/objects/ElementArrayBuffer.hh>
#include <glow/objects/Framebuffer.hh>
#include <glow/objects/Program.hh>
#include <glow/objects/TimerQuery.hh>
#include <glow/objects/Shader.hh>
#include <glow/objects/VertexArray.hh>

using namespace glow;

GLOW_TEST(Objects, Labels)
{
    {
        auto va = VertexArray::create();
        auto label = "MyVAO";
        va->setObjectLabel(label);
        auto l = va->getObjectLabel();

        ASSERT_EQ(label, l);
    }
    {
        auto ab = ArrayBuffer::create();
        auto label = "MyAB";
        ab->setObjectLabel(label);
        auto l = ab->getObjectLabel();

        ASSERT_EQ(label, l);
    }
    {
        auto eab = ElementArrayBuffer::create();
        auto label = "MyEAB";
        eab->setObjectLabel(label);
        auto l = eab->getObjectLabel();

        ASSERT_EQ(label, l);
    }
    {
        auto shader = Shader::createFromSource(GL_FRAGMENT_SHADER, "void main() { }");
        auto label = "MyShader";
        shader->setObjectLabel(label);
        auto l = shader->getObjectLabel();

        ASSERT_EQ(label, l);
    }
    {
        auto s1 = Shader::createFromSource(GL_VERTEX_SHADER, "void main() { }");
        auto s2 = Shader::createFromSource(GL_FRAGMENT_SHADER, "void main() { }");
        auto p = Program::create({s1, s2});
        auto label = "MyProgram";
        p->setObjectLabel(label);
        auto l = p->getObjectLabel();

        ASSERT_EQ(label, l);
    }
    {
        auto fb = Framebuffer::create();
        auto label = "MyFB";
        fb->setObjectLabel(label);
        auto l = fb->getObjectLabel();

        ASSERT_EQ(label, l);
    }
    {
        auto q = TimerQuery::create();
        auto label = "MyQuery";
        q->setObjectLabel(label);
        auto l = q->getObjectLabel();

        ASSERT_EQ(label, l);
    }
}
