#include "tests.hh"

#include <glow/objects/ElementArrayBuffer.hh>

GLOW_TEST(ElementArrayBuffer, Binding)
{
    auto eab0 = ElementArrayBuffer::create();
    ASSERT_EQ(ElementArrayBuffer::getCurrentBuffer(), nullptr);

    {
        auto beab0 = eab0->bind();
        ASSERT_EQ(ElementArrayBuffer::getCurrentBuffer(), &beab0);

        auto eab1 = ElementArrayBuffer::create();
        auto eab2 = ElementArrayBuffer::create();
        ASSERT_EQ(ElementArrayBuffer::getCurrentBuffer(), &beab0);

        {
            auto beab1 = eab1->bind();
            ASSERT_EQ(ElementArrayBuffer::getCurrentBuffer(), &beab1);

            auto beab2 = eab2->bind();
            ASSERT_EQ(ElementArrayBuffer::getCurrentBuffer(), &beab2);
        }

        ASSERT_EQ(ElementArrayBuffer::getCurrentBuffer(), &beab0);
    }

    ASSERT_EQ(ElementArrayBuffer::getCurrentBuffer(), nullptr);
}
