// This file is auto-generated and should not be modified directly.
#include "tests.hh"

#include <glow/std140.hh>
#include <glow/objects/UniformBuffer.hh>
#include <glow/objects/Shader.hh>
#include <glow/objects/Program.hh>


GLOW_TEST(Std140Gen, Test1)
{
    {
        struct sTest_1_0
        {
            std140double f0;
            std140dvec2 f1;
            std140float f2;
            std140mat3 f3;
        };

        auto ub = UniformBuffer::create();
        ub->addVerification({
            {&sTest_1_0::f0, "f0"}, //
            {&sTest_1_0::f1, "f1"}, //
            {&sTest_1_0::f2, "f2"}, //
            {&sTest_1_0::f3, "f3"}, //
        });

        auto shader = Shader::createFromSource(GL_VERTEX_SHADER,
            "layout(std140) uniform Buffer {\n"
            "  double f0;\n"
            "  dvec2 f1;\n"
            "  float f2;\n"
            "  mat3 f3;\n"
            "};\n"
            "out float x;\n"
            "void main() {\n"
            "   x = 0\n"
            "     + float(f0)\n"
            "     + float(f1.x)\n"
            "     + float(f2)\n"
            "     + float(f3[0][0])\n"
            "   ;\n"
            "}\n"
         );
        ASSERT_TRUE(shader->isCompiledWithoutErrors());

        auto prog = Program::create(shader);
        ASSERT_TRUE(prog->verifyUniformBuffer("Buffer", ub));
    }
    {
        struct sTest_1_1
        {
            std140vec3 f0;
            std140bvec3 f1;
            std140dmat2 f2;
            std140ivec2 f3;
        };

        auto ub = UniformBuffer::create();
        ub->addVerification({
            {&sTest_1_1::f0, "f0"}, //
            {&sTest_1_1::f1, "f1"}, //
            {&sTest_1_1::f2, "f2"}, //
            {&sTest_1_1::f3, "f3"}, //
        });

        auto shader = Shader::createFromSource(GL_VERTEX_SHADER,
            "layout(std140) uniform Buffer {\n"
            "  vec3 f0;\n"
            "  bvec3 f1;\n"
            "  dmat2 f2;\n"
            "  ivec2 f3;\n"
            "};\n"
            "out float x;\n"
            "void main() {\n"
            "   x = 0\n"
            "     + float(f0.x)\n"
            "     + float(f1.x)\n"
            "     + float(f2[0][0])\n"
            "     + float(f3.x)\n"
            "   ;\n"
            "}\n"
         );
        ASSERT_TRUE(shader->isCompiledWithoutErrors());

        auto prog = Program::create(shader);
        ASSERT_TRUE(prog->verifyUniformBuffer("Buffer", ub));
    }
    {
        struct sTest_1_2
        {
            std140mat3x3 f0;
            std140mat3 f1;
            std140uvec2 f2;
            std140uvec3 f3;
        };

        auto ub = UniformBuffer::create();
        ub->addVerification({
            {&sTest_1_2::f0, "f0"}, //
            {&sTest_1_2::f1, "f1"}, //
            {&sTest_1_2::f2, "f2"}, //
            {&sTest_1_2::f3, "f3"}, //
        });

        auto shader = Shader::createFromSource(GL_VERTEX_SHADER,
            "layout(std140) uniform Buffer {\n"
            "  mat3x3 f0;\n"
            "  mat3 f1;\n"
            "  uvec2 f2;\n"
            "  uvec3 f3;\n"
            "};\n"
            "out float x;\n"
            "void main() {\n"
            "   x = 0\n"
            "     + float(f0[0][0])\n"
            "     + float(f1[0][0])\n"
            "     + float(f2.x)\n"
            "     + float(f3.x)\n"
            "   ;\n"
            "}\n"
         );
        ASSERT_TRUE(shader->isCompiledWithoutErrors());

        auto prog = Program::create(shader);
        ASSERT_TRUE(prog->verifyUniformBuffer("Buffer", ub));
    }
    {
        struct sTest_1_3
        {
            std140uint f0;
            std140double f1;
            std140dmat2 f2;
            std140bvec3 f3;
        };

        auto ub = UniformBuffer::create();
        ub->addVerification({
            {&sTest_1_3::f0, "f0"}, //
            {&sTest_1_3::f1, "f1"}, //
            {&sTest_1_3::f2, "f2"}, //
            {&sTest_1_3::f3, "f3"}, //
        });

        auto shader = Shader::createFromSource(GL_VERTEX_SHADER,
            "layout(std140) uniform Buffer {\n"
            "  uint f0;\n"
            "  double f1;\n"
            "  dmat2 f2;\n"
            "  bvec3 f3;\n"
            "};\n"
            "out float x;\n"
            "void main() {\n"
            "   x = 0\n"
            "     + float(f0)\n"
            "     + float(f1)\n"
            "     + float(f2[0][0])\n"
            "     + float(f3.x)\n"
            "   ;\n"
            "}\n"
         );
        ASSERT_TRUE(shader->isCompiledWithoutErrors());

        auto prog = Program::create(shader);
        ASSERT_TRUE(prog->verifyUniformBuffer("Buffer", ub));
    }
    {
        struct sTest_1_4
        {
            std140uint f0;
            std140double f1;
            std140dmat4 f2;
            std140uvec2 f3;
        };

        auto ub = UniformBuffer::create();
        ub->addVerification({
            {&sTest_1_4::f0, "f0"}, //
            {&sTest_1_4::f1, "f1"}, //
            {&sTest_1_4::f2, "f2"}, //
            {&sTest_1_4::f3, "f3"}, //
        });

        auto shader = Shader::createFromSource(GL_VERTEX_SHADER,
            "layout(std140) uniform Buffer {\n"
            "  uint f0;\n"
            "  double f1;\n"
            "  dmat4 f2;\n"
            "  uvec2 f3;\n"
            "};\n"
            "out float x;\n"
            "void main() {\n"
            "   x = 0\n"
            "     + float(f0)\n"
            "     + float(f1)\n"
            "     + float(f2[0][0])\n"
            "     + float(f3.x)\n"
            "   ;\n"
            "}\n"
         );
        ASSERT_TRUE(shader->isCompiledWithoutErrors());

        auto prog = Program::create(shader);
        ASSERT_TRUE(prog->verifyUniformBuffer("Buffer", ub));
    }
    {
        struct sTest_1_5
        {
            std140vec2 f0;
            std140mat4 f1;
            std140mat4x4 f2;
            std140bvec2 f3;
        };

        auto ub = UniformBuffer::create();
        ub->addVerification({
            {&sTest_1_5::f0, "f0"}, //
            {&sTest_1_5::f1, "f1"}, //
            {&sTest_1_5::f2, "f2"}, //
            {&sTest_1_5::f3, "f3"}, //
        });

        auto shader = Shader::createFromSource(GL_VERTEX_SHADER,
            "layout(std140) uniform Buffer {\n"
            "  vec2 f0;\n"
            "  mat4 f1;\n"
            "  mat4x4 f2;\n"
            "  bvec2 f3;\n"
            "};\n"
            "out float x;\n"
            "void main() {\n"
            "   x = 0\n"
            "     + float(f0.x)\n"
            "     + float(f1[0][0])\n"
            "     + float(f2[0][0])\n"
            "     + float(f3.x)\n"
            "   ;\n"
            "}\n"
         );
        ASSERT_TRUE(shader->isCompiledWithoutErrors());

        auto prog = Program::create(shader);
        ASSERT_TRUE(prog->verifyUniformBuffer("Buffer", ub));
    }
    {
        struct sTest_1_6
        {
            std140double f0;
            std140uvec3 f1;
            std140uint f2;
            std140dmat3 f3;
        };

        auto ub = UniformBuffer::create();
        ub->addVerification({
            {&sTest_1_6::f0, "f0"}, //
            {&sTest_1_6::f1, "f1"}, //
            {&sTest_1_6::f2, "f2"}, //
            {&sTest_1_6::f3, "f3"}, //
        });

        auto shader = Shader::createFromSource(GL_VERTEX_SHADER,
            "layout(std140) uniform Buffer {\n"
            "  double f0;\n"
            "  uvec3 f1;\n"
            "  uint f2;\n"
            "  dmat3 f3;\n"
            "};\n"
            "out float x;\n"
            "void main() {\n"
            "   x = 0\n"
            "     + float(f0)\n"
            "     + float(f1.x)\n"
            "     + float(f2)\n"
            "     + float(f3[0][0])\n"
            "   ;\n"
            "}\n"
         );
        ASSERT_TRUE(shader->isCompiledWithoutErrors());

        auto prog = Program::create(shader);
        ASSERT_TRUE(prog->verifyUniformBuffer("Buffer", ub));
    }
    {
        struct sTest_1_7
        {
            std140bool f0;
            std140dmat3 f1;
            std140bvec3 f2;
            std140bool f3;
        };

        auto ub = UniformBuffer::create();
        ub->addVerification({
            {&sTest_1_7::f0, "f0"}, //
            {&sTest_1_7::f1, "f1"}, //
            {&sTest_1_7::f2, "f2"}, //
            {&sTest_1_7::f3, "f3"}, //
        });

        auto shader = Shader::createFromSource(GL_VERTEX_SHADER,
            "layout(std140) uniform Buffer {\n"
            "  bool f0;\n"
            "  dmat3 f1;\n"
            "  bvec3 f2;\n"
            "  bool f3;\n"
            "};\n"
            "out float x;\n"
            "void main() {\n"
            "   x = 0\n"
            "     + float(f0)\n"
            "     + float(f1[0][0])\n"
            "     + float(f2.x)\n"
            "     + float(f3)\n"
            "   ;\n"
            "}\n"
         );
        ASSERT_TRUE(shader->isCompiledWithoutErrors());

        auto prog = Program::create(shader);
        ASSERT_TRUE(prog->verifyUniformBuffer("Buffer", ub));
    }
    {
        struct sTest_1_8
        {
            std140int f0;
            std140mat4 f1;
            std140bvec3 f2;
            std140mat2 f3;
        };

        auto ub = UniformBuffer::create();
        ub->addVerification({
            {&sTest_1_8::f0, "f0"}, //
            {&sTest_1_8::f1, "f1"}, //
            {&sTest_1_8::f2, "f2"}, //
            {&sTest_1_8::f3, "f3"}, //
        });

        auto shader = Shader::createFromSource(GL_VERTEX_SHADER,
            "layout(std140) uniform Buffer {\n"
            "  int f0;\n"
            "  mat4 f1;\n"
            "  bvec3 f2;\n"
            "  mat2 f3;\n"
            "};\n"
            "out float x;\n"
            "void main() {\n"
            "   x = 0\n"
            "     + float(f0)\n"
            "     + float(f1[0][0])\n"
            "     + float(f2.x)\n"
            "     + float(f3[0][0])\n"
            "   ;\n"
            "}\n"
         );
        ASSERT_TRUE(shader->isCompiledWithoutErrors());

        auto prog = Program::create(shader);
        ASSERT_TRUE(prog->verifyUniformBuffer("Buffer", ub));
    }
    {
        struct sTest_1_9
        {
            std140float f0;
            std140dvec3 f1;
            std140dmat2 f2;
            std140int f3;
        };

        auto ub = UniformBuffer::create();
        ub->addVerification({
            {&sTest_1_9::f0, "f0"}, //
            {&sTest_1_9::f1, "f1"}, //
            {&sTest_1_9::f2, "f2"}, //
            {&sTest_1_9::f3, "f3"}, //
        });

        auto shader = Shader::createFromSource(GL_VERTEX_SHADER,
            "layout(std140) uniform Buffer {\n"
            "  float f0;\n"
            "  dvec3 f1;\n"
            "  dmat2 f2;\n"
            "  int f3;\n"
            "};\n"
            "out float x;\n"
            "void main() {\n"
            "   x = 0\n"
            "     + float(f0)\n"
            "     + float(f1.x)\n"
            "     + float(f2[0][0])\n"
            "     + float(f3)\n"
            "   ;\n"
            "}\n"
         );
        ASSERT_TRUE(shader->isCompiledWithoutErrors());

        auto prog = Program::create(shader);
        ASSERT_TRUE(prog->verifyUniformBuffer("Buffer", ub));
    }
    {
        struct sTest_1_10
        {
            std140mat4x3 f0;
            std140mat2x2 f1;
            std140mat3x2 f2;
            std140dmat4x3 f3;
        };

        auto ub = UniformBuffer::create();
        ub->addVerification({
            {&sTest_1_10::f0, "f0"}, //
            {&sTest_1_10::f1, "f1"}, //
            {&sTest_1_10::f2, "f2"}, //
            {&sTest_1_10::f3, "f3"}, //
        });

        auto shader = Shader::createFromSource(GL_VERTEX_SHADER,
            "layout(std140) uniform Buffer {\n"
            "  mat4x3 f0;\n"
            "  mat2x2 f1;\n"
            "  mat3x2 f2;\n"
            "  dmat4x3 f3;\n"
            "};\n"
            "out float x;\n"
            "void main() {\n"
            "   x = 0\n"
            "     + float(f0[0][0])\n"
            "     + float(f1[0][0])\n"
            "     + float(f2[0][0])\n"
            "     + float(f3[0][0])\n"
            "   ;\n"
            "}\n"
         );
        ASSERT_TRUE(shader->isCompiledWithoutErrors());

        auto prog = Program::create(shader);
        ASSERT_TRUE(prog->verifyUniformBuffer("Buffer", ub));
    }
    {
        struct sTest_1_11
        {
            std140mat4 f0;
            std140mat4 f1;
            std140int f2;
            std140uint f3;
        };

        auto ub = UniformBuffer::create();
        ub->addVerification({
            {&sTest_1_11::f0, "f0"}, //
            {&sTest_1_11::f1, "f1"}, //
            {&sTest_1_11::f2, "f2"}, //
            {&sTest_1_11::f3, "f3"}, //
        });

        auto shader = Shader::createFromSource(GL_VERTEX_SHADER,
            "layout(std140) uniform Buffer {\n"
            "  mat4 f0;\n"
            "  mat4 f1;\n"
            "  int f2;\n"
            "  uint f3;\n"
            "};\n"
            "out float x;\n"
            "void main() {\n"
            "   x = 0\n"
            "     + float(f0[0][0])\n"
            "     + float(f1[0][0])\n"
            "     + float(f2)\n"
            "     + float(f3)\n"
            "   ;\n"
            "}\n"
         );
        ASSERT_TRUE(shader->isCompiledWithoutErrors());

        auto prog = Program::create(shader);
        ASSERT_TRUE(prog->verifyUniformBuffer("Buffer", ub));
    }
    {
        struct sTest_1_12
        {
            std140vec3 f0;
            std140mat4 f1;
            std140dvec3 f2;
            std140uint f3;
        };

        auto ub = UniformBuffer::create();
        ub->addVerification({
            {&sTest_1_12::f0, "f0"}, //
            {&sTest_1_12::f1, "f1"}, //
            {&sTest_1_12::f2, "f2"}, //
            {&sTest_1_12::f3, "f3"}, //
        });

        auto shader = Shader::createFromSource(GL_VERTEX_SHADER,
            "layout(std140) uniform Buffer {\n"
            "  vec3 f0;\n"
            "  mat4 f1;\n"
            "  dvec3 f2;\n"
            "  uint f3;\n"
            "};\n"
            "out float x;\n"
            "void main() {\n"
            "   x = 0\n"
            "     + float(f0.x)\n"
            "     + float(f1[0][0])\n"
            "     + float(f2.x)\n"
            "     + float(f3)\n"
            "   ;\n"
            "}\n"
         );
        ASSERT_TRUE(shader->isCompiledWithoutErrors());

        auto prog = Program::create(shader);
        ASSERT_TRUE(prog->verifyUniformBuffer("Buffer", ub));
    }
    {
        struct sTest_1_13
        {
            std140uint f0;
            std140mat2x3 f1;
            std140mat2x3 f2;
            std140ivec4 f3;
        };

        auto ub = UniformBuffer::create();
        ub->addVerification({
            {&sTest_1_13::f0, "f0"}, //
            {&sTest_1_13::f1, "f1"}, //
            {&sTest_1_13::f2, "f2"}, //
            {&sTest_1_13::f3, "f3"}, //
        });

        auto shader = Shader::createFromSource(GL_VERTEX_SHADER,
            "layout(std140) uniform Buffer {\n"
            "  uint f0;\n"
            "  mat2x3 f1;\n"
            "  mat2x3 f2;\n"
            "  ivec4 f3;\n"
            "};\n"
            "out float x;\n"
            "void main() {\n"
            "   x = 0\n"
            "     + float(f0)\n"
            "     + float(f1[0][0])\n"
            "     + float(f2[0][0])\n"
            "     + float(f3.x)\n"
            "   ;\n"
            "}\n"
         );
        ASSERT_TRUE(shader->isCompiledWithoutErrors());

        auto prog = Program::create(shader);
        ASSERT_TRUE(prog->verifyUniformBuffer("Buffer", ub));
    }
    {
        struct sTest_1_14
        {
            std140bool f0;
            std140bool f1;
            std140vec2 f2;
            std140uvec3 f3;
        };

        auto ub = UniformBuffer::create();
        ub->addVerification({
            {&sTest_1_14::f0, "f0"}, //
            {&sTest_1_14::f1, "f1"}, //
            {&sTest_1_14::f2, "f2"}, //
            {&sTest_1_14::f3, "f3"}, //
        });

        auto shader = Shader::createFromSource(GL_VERTEX_SHADER,
            "layout(std140) uniform Buffer {\n"
            "  bool f0;\n"
            "  bool f1;\n"
            "  vec2 f2;\n"
            "  uvec3 f3;\n"
            "};\n"
            "out float x;\n"
            "void main() {\n"
            "   x = 0\n"
            "     + float(f0)\n"
            "     + float(f1)\n"
            "     + float(f2.x)\n"
            "     + float(f3.x)\n"
            "   ;\n"
            "}\n"
         );
        ASSERT_TRUE(shader->isCompiledWithoutErrors());

        auto prog = Program::create(shader);
        ASSERT_TRUE(prog->verifyUniformBuffer("Buffer", ub));
    }
    {
        struct sTest_1_15
        {
            std140vec3 f0;
            std140mat3 f1;
            std140dmat3 f2;
            std140uint f3;
        };

        auto ub = UniformBuffer::create();
        ub->addVerification({
            {&sTest_1_15::f0, "f0"}, //
            {&sTest_1_15::f1, "f1"}, //
            {&sTest_1_15::f2, "f2"}, //
            {&sTest_1_15::f3, "f3"}, //
        });

        auto shader = Shader::createFromSource(GL_VERTEX_SHADER,
            "layout(std140) uniform Buffer {\n"
            "  vec3 f0;\n"
            "  mat3 f1;\n"
            "  dmat3 f2;\n"
            "  uint f3;\n"
            "};\n"
            "out float x;\n"
            "void main() {\n"
            "   x = 0\n"
            "     + float(f0.x)\n"
            "     + float(f1[0][0])\n"
            "     + float(f2[0][0])\n"
            "     + float(f3)\n"
            "   ;\n"
            "}\n"
         );
        ASSERT_TRUE(shader->isCompiledWithoutErrors());

        auto prog = Program::create(shader);
        ASSERT_TRUE(prog->verifyUniformBuffer("Buffer", ub));
    }
    {
        struct sTest_1_16
        {
            std140bvec2 f0;
            std140mat4 f1;
            std140float f2;
            std140bvec3 f3;
        };

        auto ub = UniformBuffer::create();
        ub->addVerification({
            {&sTest_1_16::f0, "f0"}, //
            {&sTest_1_16::f1, "f1"}, //
            {&sTest_1_16::f2, "f2"}, //
            {&sTest_1_16::f3, "f3"}, //
        });

        auto shader = Shader::createFromSource(GL_VERTEX_SHADER,
            "layout(std140) uniform Buffer {\n"
            "  bvec2 f0;\n"
            "  mat4 f1;\n"
            "  float f2;\n"
            "  bvec3 f3;\n"
            "};\n"
            "out float x;\n"
            "void main() {\n"
            "   x = 0\n"
            "     + float(f0.x)\n"
            "     + float(f1[0][0])\n"
            "     + float(f2)\n"
            "     + float(f3.x)\n"
            "   ;\n"
            "}\n"
         );
        ASSERT_TRUE(shader->isCompiledWithoutErrors());

        auto prog = Program::create(shader);
        ASSERT_TRUE(prog->verifyUniformBuffer("Buffer", ub));
    }
    {
        struct sTest_1_17
        {
            std140dmat2 f0;
            std140uvec4 f1;
            std140ivec4 f2;
            std140mat2 f3;
        };

        auto ub = UniformBuffer::create();
        ub->addVerification({
            {&sTest_1_17::f0, "f0"}, //
            {&sTest_1_17::f1, "f1"}, //
            {&sTest_1_17::f2, "f2"}, //
            {&sTest_1_17::f3, "f3"}, //
        });

        auto shader = Shader::createFromSource(GL_VERTEX_SHADER,
            "layout(std140) uniform Buffer {\n"
            "  dmat2 f0;\n"
            "  uvec4 f1;\n"
            "  ivec4 f2;\n"
            "  mat2 f3;\n"
            "};\n"
            "out float x;\n"
            "void main() {\n"
            "   x = 0\n"
            "     + float(f0[0][0])\n"
            "     + float(f1.x)\n"
            "     + float(f2.x)\n"
            "     + float(f3[0][0])\n"
            "   ;\n"
            "}\n"
         );
        ASSERT_TRUE(shader->isCompiledWithoutErrors());

        auto prog = Program::create(shader);
        ASSERT_TRUE(prog->verifyUniformBuffer("Buffer", ub));
    }
    {
        struct sTest_1_18
        {
            std140mat4x3 f0;
            std140uint f1;
            std140mat3x4 f2;
            std140dmat2 f3;
        };

        auto ub = UniformBuffer::create();
        ub->addVerification({
            {&sTest_1_18::f0, "f0"}, //
            {&sTest_1_18::f1, "f1"}, //
            {&sTest_1_18::f2, "f2"}, //
            {&sTest_1_18::f3, "f3"}, //
        });

        auto shader = Shader::createFromSource(GL_VERTEX_SHADER,
            "layout(std140) uniform Buffer {\n"
            "  mat4x3 f0;\n"
            "  uint f1;\n"
            "  mat3x4 f2;\n"
            "  dmat2 f3;\n"
            "};\n"
            "out float x;\n"
            "void main() {\n"
            "   x = 0\n"
            "     + float(f0[0][0])\n"
            "     + float(f1)\n"
            "     + float(f2[0][0])\n"
            "     + float(f3[0][0])\n"
            "   ;\n"
            "}\n"
         );
        ASSERT_TRUE(shader->isCompiledWithoutErrors());

        auto prog = Program::create(shader);
        ASSERT_TRUE(prog->verifyUniformBuffer("Buffer", ub));
    }
    {
        struct sTest_1_19
        {
            std140mat2x4 f0;
            std140mat4 f1;
            std140float f2;
            std140uint f3;
        };

        auto ub = UniformBuffer::create();
        ub->addVerification({
            {&sTest_1_19::f0, "f0"}, //
            {&sTest_1_19::f1, "f1"}, //
            {&sTest_1_19::f2, "f2"}, //
            {&sTest_1_19::f3, "f3"}, //
        });

        auto shader = Shader::createFromSource(GL_VERTEX_SHADER,
            "layout(std140) uniform Buffer {\n"
            "  mat2x4 f0;\n"
            "  mat4 f1;\n"
            "  float f2;\n"
            "  uint f3;\n"
            "};\n"
            "out float x;\n"
            "void main() {\n"
            "   x = 0\n"
            "     + float(f0[0][0])\n"
            "     + float(f1[0][0])\n"
            "     + float(f2)\n"
            "     + float(f3)\n"
            "   ;\n"
            "}\n"
         );
        ASSERT_TRUE(shader->isCompiledWithoutErrors());

        auto prog = Program::create(shader);
        ASSERT_TRUE(prog->verifyUniformBuffer("Buffer", ub));
    }
    {
        struct sTest_1_20
        {
            std140float f0;
            std140vec4 f1;
            std140dvec4 f2;
            std140uint f3;
        };

        auto ub = UniformBuffer::create();
        ub->addVerification({
            {&sTest_1_20::f0, "f0"}, //
            {&sTest_1_20::f1, "f1"}, //
            {&sTest_1_20::f2, "f2"}, //
            {&sTest_1_20::f3, "f3"}, //
        });

        auto shader = Shader::createFromSource(GL_VERTEX_SHADER,
            "layout(std140) uniform Buffer {\n"
            "  float f0;\n"
            "  vec4 f1;\n"
            "  dvec4 f2;\n"
            "  uint f3;\n"
            "};\n"
            "out float x;\n"
            "void main() {\n"
            "   x = 0\n"
            "     + float(f0)\n"
            "     + float(f1.x)\n"
            "     + float(f2.x)\n"
            "     + float(f3)\n"
            "   ;\n"
            "}\n"
         );
        ASSERT_TRUE(shader->isCompiledWithoutErrors());

        auto prog = Program::create(shader);
        ASSERT_TRUE(prog->verifyUniformBuffer("Buffer", ub));
    }
    {
        struct sTest_1_21
        {
            std140uint f0;
            std140bvec4 f1;
            std140ivec4 f2;
            std140dmat2x4 f3;
        };

        auto ub = UniformBuffer::create();
        ub->addVerification({
            {&sTest_1_21::f0, "f0"}, //
            {&sTest_1_21::f1, "f1"}, //
            {&sTest_1_21::f2, "f2"}, //
            {&sTest_1_21::f3, "f3"}, //
        });

        auto shader = Shader::createFromSource(GL_VERTEX_SHADER,
            "layout(std140) uniform Buffer {\n"
            "  uint f0;\n"
            "  bvec4 f1;\n"
            "  ivec4 f2;\n"
            "  dmat2x4 f3;\n"
            "};\n"
            "out float x;\n"
            "void main() {\n"
            "   x = 0\n"
            "     + float(f0)\n"
            "     + float(f1.x)\n"
            "     + float(f2.x)\n"
            "     + float(f3[0][0])\n"
            "   ;\n"
            "}\n"
         );
        ASSERT_TRUE(shader->isCompiledWithoutErrors());

        auto prog = Program::create(shader);
        ASSERT_TRUE(prog->verifyUniformBuffer("Buffer", ub));
    }
    {
        struct sTest_1_22
        {
            std140mat2 f0;
            std140mat2 f1;
            std140uvec2 f2;
            std140dmat3 f3;
        };

        auto ub = UniformBuffer::create();
        ub->addVerification({
            {&sTest_1_22::f0, "f0"}, //
            {&sTest_1_22::f1, "f1"}, //
            {&sTest_1_22::f2, "f2"}, //
            {&sTest_1_22::f3, "f3"}, //
        });

        auto shader = Shader::createFromSource(GL_VERTEX_SHADER,
            "layout(std140) uniform Buffer {\n"
            "  mat2 f0;\n"
            "  mat2 f1;\n"
            "  uvec2 f2;\n"
            "  dmat3 f3;\n"
            "};\n"
            "out float x;\n"
            "void main() {\n"
            "   x = 0\n"
            "     + float(f0[0][0])\n"
            "     + float(f1[0][0])\n"
            "     + float(f2.x)\n"
            "     + float(f3[0][0])\n"
            "   ;\n"
            "}\n"
         );
        ASSERT_TRUE(shader->isCompiledWithoutErrors());

        auto prog = Program::create(shader);
        ASSERT_TRUE(prog->verifyUniformBuffer("Buffer", ub));
    }
    {
        struct sTest_1_23
        {
            std140mat4 f0;
            std140ivec3 f1;
            std140dmat2x4 f2;
            std140double f3;
        };

        auto ub = UniformBuffer::create();
        ub->addVerification({
            {&sTest_1_23::f0, "f0"}, //
            {&sTest_1_23::f1, "f1"}, //
            {&sTest_1_23::f2, "f2"}, //
            {&sTest_1_23::f3, "f3"}, //
        });

        auto shader = Shader::createFromSource(GL_VERTEX_SHADER,
            "layout(std140) uniform Buffer {\n"
            "  mat4 f0;\n"
            "  ivec3 f1;\n"
            "  dmat2x4 f2;\n"
            "  double f3;\n"
            "};\n"
            "out float x;\n"
            "void main() {\n"
            "   x = 0\n"
            "     + float(f0[0][0])\n"
            "     + float(f1.x)\n"
            "     + float(f2[0][0])\n"
            "     + float(f3)\n"
            "   ;\n"
            "}\n"
         );
        ASSERT_TRUE(shader->isCompiledWithoutErrors());

        auto prog = Program::create(shader);
        ASSERT_TRUE(prog->verifyUniformBuffer("Buffer", ub));
    }
    {
        struct sTest_1_24
        {
            std140mat3 f0;
            std140dvec2 f1;
            std140bvec4 f2;
            std140float f3;
        };

        auto ub = UniformBuffer::create();
        ub->addVerification({
            {&sTest_1_24::f0, "f0"}, //
            {&sTest_1_24::f1, "f1"}, //
            {&sTest_1_24::f2, "f2"}, //
            {&sTest_1_24::f3, "f3"}, //
        });

        auto shader = Shader::createFromSource(GL_VERTEX_SHADER,
            "layout(std140) uniform Buffer {\n"
            "  mat3 f0;\n"
            "  dvec2 f1;\n"
            "  bvec4 f2;\n"
            "  float f3;\n"
            "};\n"
            "out float x;\n"
            "void main() {\n"
            "   x = 0\n"
            "     + float(f0[0][0])\n"
            "     + float(f1.x)\n"
            "     + float(f2.x)\n"
            "     + float(f3)\n"
            "   ;\n"
            "}\n"
         );
        ASSERT_TRUE(shader->isCompiledWithoutErrors());

        auto prog = Program::create(shader);
        ASSERT_TRUE(prog->verifyUniformBuffer("Buffer", ub));
    }
    {
        struct sTest_1_25
        {
            std140dmat4x4 f0;
            std140bool f1;
            std140double f2;
            std140mat4 f3;
        };

        auto ub = UniformBuffer::create();
        ub->addVerification({
            {&sTest_1_25::f0, "f0"}, //
            {&sTest_1_25::f1, "f1"}, //
            {&sTest_1_25::f2, "f2"}, //
            {&sTest_1_25::f3, "f3"}, //
        });

        auto shader = Shader::createFromSource(GL_VERTEX_SHADER,
            "layout(std140) uniform Buffer {\n"
            "  dmat4x4 f0;\n"
            "  bool f1;\n"
            "  double f2;\n"
            "  mat4 f3;\n"
            "};\n"
            "out float x;\n"
            "void main() {\n"
            "   x = 0\n"
            "     + float(f0[0][0])\n"
            "     + float(f1)\n"
            "     + float(f2)\n"
            "     + float(f3[0][0])\n"
            "   ;\n"
            "}\n"
         );
        ASSERT_TRUE(shader->isCompiledWithoutErrors());

        auto prog = Program::create(shader);
        ASSERT_TRUE(prog->verifyUniformBuffer("Buffer", ub));
    }
    {
        struct sTest_1_26
        {
            std140float f0;
            std140vec3 f1;
            std140bool f2;
            std140mat2 f3;
        };

        auto ub = UniformBuffer::create();
        ub->addVerification({
            {&sTest_1_26::f0, "f0"}, //
            {&sTest_1_26::f1, "f1"}, //
            {&sTest_1_26::f2, "f2"}, //
            {&sTest_1_26::f3, "f3"}, //
        });

        auto shader = Shader::createFromSource(GL_VERTEX_SHADER,
            "layout(std140) uniform Buffer {\n"
            "  float f0;\n"
            "  vec3 f1;\n"
            "  bool f2;\n"
            "  mat2 f3;\n"
            "};\n"
            "out float x;\n"
            "void main() {\n"
            "   x = 0\n"
            "     + float(f0)\n"
            "     + float(f1.x)\n"
            "     + float(f2)\n"
            "     + float(f3[0][0])\n"
            "   ;\n"
            "}\n"
         );
        ASSERT_TRUE(shader->isCompiledWithoutErrors());

        auto prog = Program::create(shader);
        ASSERT_TRUE(prog->verifyUniformBuffer("Buffer", ub));
    }
    {
        struct sTest_1_27
        {
            std140dmat4x3 f0;
            std140dvec2 f1;
            std140mat4x3 f2;
            std140mat4x4 f3;
        };

        auto ub = UniformBuffer::create();
        ub->addVerification({
            {&sTest_1_27::f0, "f0"}, //
            {&sTest_1_27::f1, "f1"}, //
            {&sTest_1_27::f2, "f2"}, //
            {&sTest_1_27::f3, "f3"}, //
        });

        auto shader = Shader::createFromSource(GL_VERTEX_SHADER,
            "layout(std140) uniform Buffer {\n"
            "  dmat4x3 f0;\n"
            "  dvec2 f1;\n"
            "  mat4x3 f2;\n"
            "  mat4x4 f3;\n"
            "};\n"
            "out float x;\n"
            "void main() {\n"
            "   x = 0\n"
            "     + float(f0[0][0])\n"
            "     + float(f1.x)\n"
            "     + float(f2[0][0])\n"
            "     + float(f3[0][0])\n"
            "   ;\n"
            "}\n"
         );
        ASSERT_TRUE(shader->isCompiledWithoutErrors());

        auto prog = Program::create(shader);
        ASSERT_TRUE(prog->verifyUniformBuffer("Buffer", ub));
    }
    {
        struct sTest_1_28
        {
            std140bool f0;
            std140mat2 f1;
            std140dvec4 f2;
            std140ivec3 f3;
        };

        auto ub = UniformBuffer::create();
        ub->addVerification({
            {&sTest_1_28::f0, "f0"}, //
            {&sTest_1_28::f1, "f1"}, //
            {&sTest_1_28::f2, "f2"}, //
            {&sTest_1_28::f3, "f3"}, //
        });

        auto shader = Shader::createFromSource(GL_VERTEX_SHADER,
            "layout(std140) uniform Buffer {\n"
            "  bool f0;\n"
            "  mat2 f1;\n"
            "  dvec4 f2;\n"
            "  ivec3 f3;\n"
            "};\n"
            "out float x;\n"
            "void main() {\n"
            "   x = 0\n"
            "     + float(f0)\n"
            "     + float(f1[0][0])\n"
            "     + float(f2.x)\n"
            "     + float(f3.x)\n"
            "   ;\n"
            "}\n"
         );
        ASSERT_TRUE(shader->isCompiledWithoutErrors());

        auto prog = Program::create(shader);
        ASSERT_TRUE(prog->verifyUniformBuffer("Buffer", ub));
    }
    {
        struct sTest_1_29
        {
            std140uint f0;
            std140dmat3x3 f1;
            std140uint f2;
            std140uvec3 f3;
        };

        auto ub = UniformBuffer::create();
        ub->addVerification({
            {&sTest_1_29::f0, "f0"}, //
            {&sTest_1_29::f1, "f1"}, //
            {&sTest_1_29::f2, "f2"}, //
            {&sTest_1_29::f3, "f3"}, //
        });

        auto shader = Shader::createFromSource(GL_VERTEX_SHADER,
            "layout(std140) uniform Buffer {\n"
            "  uint f0;\n"
            "  dmat3x3 f1;\n"
            "  uint f2;\n"
            "  uvec3 f3;\n"
            "};\n"
            "out float x;\n"
            "void main() {\n"
            "   x = 0\n"
            "     + float(f0)\n"
            "     + float(f1[0][0])\n"
            "     + float(f2)\n"
            "     + float(f3.x)\n"
            "   ;\n"
            "}\n"
         );
        ASSERT_TRUE(shader->isCompiledWithoutErrors());

        auto prog = Program::create(shader);
        ASSERT_TRUE(prog->verifyUniformBuffer("Buffer", ub));
    }
}

