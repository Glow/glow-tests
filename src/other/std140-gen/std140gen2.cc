// This file is auto-generated and should not be modified directly.
#include "tests.hh"

#include <glow/std140.hh>
#include <glow/objects/UniformBuffer.hh>
#include <glow/objects/Shader.hh>
#include <glow/objects/Program.hh>


GLOW_TEST(Std140Gen, Test2)
{
    {
        struct sTest_2_0
        {
            std140bool f0;
            std140mat3x2 f1;
            std140vec4 f2;
            std140dmat3x2 f3;
            std140dvec3 f4;
        };

        auto ub = UniformBuffer::create();
        ub->addVerification({
            {&sTest_2_0::f0, "f0"}, //
            {&sTest_2_0::f1, "f1"}, //
            {&sTest_2_0::f2, "f2"}, //
            {&sTest_2_0::f3, "f3"}, //
            {&sTest_2_0::f4, "f4"}, //
        });

        auto shader = Shader::createFromSource(GL_VERTEX_SHADER,
            "layout(std140) uniform Buffer {\n"
            "  bool f0;\n"
            "  mat3x2 f1;\n"
            "  vec4 f2;\n"
            "  dmat3x2 f3;\n"
            "  dvec3 f4;\n"
            "};\n"
            "out float x;\n"
            "void main() {\n"
            "   x = 0\n"
            "     + float(f0)\n"
            "     + float(f1[0][0])\n"
            "     + float(f2.x)\n"
            "     + float(f3[0][0])\n"
            "     + float(f4.x)\n"
            "   ;\n"
            "}\n"
         );
        ASSERT_TRUE(shader->isCompiledWithoutErrors());

        auto prog = Program::create(shader);
        ASSERT_TRUE(prog->verifyUniformBuffer("Buffer", ub));
    }
    {
        struct sTest_2_1
        {
            std140mat2x3 f0;
            std140mat3 f1;
            std140vec2 f2;
            std140dmat4x3 f3;
            std140bvec2 f4;
        };

        auto ub = UniformBuffer::create();
        ub->addVerification({
            {&sTest_2_1::f0, "f0"}, //
            {&sTest_2_1::f1, "f1"}, //
            {&sTest_2_1::f2, "f2"}, //
            {&sTest_2_1::f3, "f3"}, //
            {&sTest_2_1::f4, "f4"}, //
        });

        auto shader = Shader::createFromSource(GL_VERTEX_SHADER,
            "layout(std140) uniform Buffer {\n"
            "  mat2x3 f0;\n"
            "  mat3 f1;\n"
            "  vec2 f2;\n"
            "  dmat4x3 f3;\n"
            "  bvec2 f4;\n"
            "};\n"
            "out float x;\n"
            "void main() {\n"
            "   x = 0\n"
            "     + float(f0[0][0])\n"
            "     + float(f1[0][0])\n"
            "     + float(f2.x)\n"
            "     + float(f3[0][0])\n"
            "     + float(f4.x)\n"
            "   ;\n"
            "}\n"
         );
        ASSERT_TRUE(shader->isCompiledWithoutErrors());

        auto prog = Program::create(shader);
        ASSERT_TRUE(prog->verifyUniformBuffer("Buffer", ub));
    }
    {
        struct sTest_2_2
        {
            std140bvec2 f0;
            std140mat3 f1;
            std140mat4x2 f2;
            std140mat3x2 f3;
            std140bvec3 f4;
        };

        auto ub = UniformBuffer::create();
        ub->addVerification({
            {&sTest_2_2::f0, "f0"}, //
            {&sTest_2_2::f1, "f1"}, //
            {&sTest_2_2::f2, "f2"}, //
            {&sTest_2_2::f3, "f3"}, //
            {&sTest_2_2::f4, "f4"}, //
        });

        auto shader = Shader::createFromSource(GL_VERTEX_SHADER,
            "layout(std140) uniform Buffer {\n"
            "  bvec2 f0;\n"
            "  mat3 f1;\n"
            "  mat4x2 f2;\n"
            "  mat3x2 f3;\n"
            "  bvec3 f4;\n"
            "};\n"
            "out float x;\n"
            "void main() {\n"
            "   x = 0\n"
            "     + float(f0.x)\n"
            "     + float(f1[0][0])\n"
            "     + float(f2[0][0])\n"
            "     + float(f3[0][0])\n"
            "     + float(f4.x)\n"
            "   ;\n"
            "}\n"
         );
        ASSERT_TRUE(shader->isCompiledWithoutErrors());

        auto prog = Program::create(shader);
        ASSERT_TRUE(prog->verifyUniformBuffer("Buffer", ub));
    }
    {
        struct sTest_2_3
        {
            std140bool f0;
            std140vec2 f1;
            std140double f2;
            std140dmat3 f3;
            std140mat4x4 f4;
        };

        auto ub = UniformBuffer::create();
        ub->addVerification({
            {&sTest_2_3::f0, "f0"}, //
            {&sTest_2_3::f1, "f1"}, //
            {&sTest_2_3::f2, "f2"}, //
            {&sTest_2_3::f3, "f3"}, //
            {&sTest_2_3::f4, "f4"}, //
        });

        auto shader = Shader::createFromSource(GL_VERTEX_SHADER,
            "layout(std140) uniform Buffer {\n"
            "  bool f0;\n"
            "  vec2 f1;\n"
            "  double f2;\n"
            "  dmat3 f3;\n"
            "  mat4x4 f4;\n"
            "};\n"
            "out float x;\n"
            "void main() {\n"
            "   x = 0\n"
            "     + float(f0)\n"
            "     + float(f1.x)\n"
            "     + float(f2)\n"
            "     + float(f3[0][0])\n"
            "     + float(f4[0][0])\n"
            "   ;\n"
            "}\n"
         );
        ASSERT_TRUE(shader->isCompiledWithoutErrors());

        auto prog = Program::create(shader);
        ASSERT_TRUE(prog->verifyUniformBuffer("Buffer", ub));
    }
    {
        struct sTest_2_4
        {
            std140mat2x4 f0;
            std140mat2 f1;
            std140dvec4 f2;
            std140bool f3;
            std140bvec2 f4;
        };

        auto ub = UniformBuffer::create();
        ub->addVerification({
            {&sTest_2_4::f0, "f0"}, //
            {&sTest_2_4::f1, "f1"}, //
            {&sTest_2_4::f2, "f2"}, //
            {&sTest_2_4::f3, "f3"}, //
            {&sTest_2_4::f4, "f4"}, //
        });

        auto shader = Shader::createFromSource(GL_VERTEX_SHADER,
            "layout(std140) uniform Buffer {\n"
            "  mat2x4 f0;\n"
            "  mat2 f1;\n"
            "  dvec4 f2;\n"
            "  bool f3;\n"
            "  bvec2 f4;\n"
            "};\n"
            "out float x;\n"
            "void main() {\n"
            "   x = 0\n"
            "     + float(f0[0][0])\n"
            "     + float(f1[0][0])\n"
            "     + float(f2.x)\n"
            "     + float(f3)\n"
            "     + float(f4.x)\n"
            "   ;\n"
            "}\n"
         );
        ASSERT_TRUE(shader->isCompiledWithoutErrors());

        auto prog = Program::create(shader);
        ASSERT_TRUE(prog->verifyUniformBuffer("Buffer", ub));
    }
    {
        struct sTest_2_5
        {
            std140mat3x4 f0;
            std140dvec4 f1;
            std140uvec4 f2;
            std140ivec2 f3;
            std140float f4;
        };

        auto ub = UniformBuffer::create();
        ub->addVerification({
            {&sTest_2_5::f0, "f0"}, //
            {&sTest_2_5::f1, "f1"}, //
            {&sTest_2_5::f2, "f2"}, //
            {&sTest_2_5::f3, "f3"}, //
            {&sTest_2_5::f4, "f4"}, //
        });

        auto shader = Shader::createFromSource(GL_VERTEX_SHADER,
            "layout(std140) uniform Buffer {\n"
            "  mat3x4 f0;\n"
            "  dvec4 f1;\n"
            "  uvec4 f2;\n"
            "  ivec2 f3;\n"
            "  float f4;\n"
            "};\n"
            "out float x;\n"
            "void main() {\n"
            "   x = 0\n"
            "     + float(f0[0][0])\n"
            "     + float(f1.x)\n"
            "     + float(f2.x)\n"
            "     + float(f3.x)\n"
            "     + float(f4)\n"
            "   ;\n"
            "}\n"
         );
        ASSERT_TRUE(shader->isCompiledWithoutErrors());

        auto prog = Program::create(shader);
        ASSERT_TRUE(prog->verifyUniformBuffer("Buffer", ub));
    }
    {
        struct sTest_2_6
        {
            std140bvec3 f0;
            std140float f1;
            std140bool f2;
            std140bvec2 f3;
            std140mat3x3 f4;
        };

        auto ub = UniformBuffer::create();
        ub->addVerification({
            {&sTest_2_6::f0, "f0"}, //
            {&sTest_2_6::f1, "f1"}, //
            {&sTest_2_6::f2, "f2"}, //
            {&sTest_2_6::f3, "f3"}, //
            {&sTest_2_6::f4, "f4"}, //
        });

        auto shader = Shader::createFromSource(GL_VERTEX_SHADER,
            "layout(std140) uniform Buffer {\n"
            "  bvec3 f0;\n"
            "  float f1;\n"
            "  bool f2;\n"
            "  bvec2 f3;\n"
            "  mat3x3 f4;\n"
            "};\n"
            "out float x;\n"
            "void main() {\n"
            "   x = 0\n"
            "     + float(f0.x)\n"
            "     + float(f1)\n"
            "     + float(f2)\n"
            "     + float(f3.x)\n"
            "     + float(f4[0][0])\n"
            "   ;\n"
            "}\n"
         );
        ASSERT_TRUE(shader->isCompiledWithoutErrors());

        auto prog = Program::create(shader);
        ASSERT_TRUE(prog->verifyUniformBuffer("Buffer", ub));
    }
    {
        struct sTest_2_7
        {
            std140uvec2 f0;
            std140mat4 f1;
            std140dvec2 f2;
            std140ivec3 f3;
            std140uint f4;
        };

        auto ub = UniformBuffer::create();
        ub->addVerification({
            {&sTest_2_7::f0, "f0"}, //
            {&sTest_2_7::f1, "f1"}, //
            {&sTest_2_7::f2, "f2"}, //
            {&sTest_2_7::f3, "f3"}, //
            {&sTest_2_7::f4, "f4"}, //
        });

        auto shader = Shader::createFromSource(GL_VERTEX_SHADER,
            "layout(std140) uniform Buffer {\n"
            "  uvec2 f0;\n"
            "  mat4 f1;\n"
            "  dvec2 f2;\n"
            "  ivec3 f3;\n"
            "  uint f4;\n"
            "};\n"
            "out float x;\n"
            "void main() {\n"
            "   x = 0\n"
            "     + float(f0.x)\n"
            "     + float(f1[0][0])\n"
            "     + float(f2.x)\n"
            "     + float(f3.x)\n"
            "     + float(f4)\n"
            "   ;\n"
            "}\n"
         );
        ASSERT_TRUE(shader->isCompiledWithoutErrors());

        auto prog = Program::create(shader);
        ASSERT_TRUE(prog->verifyUniformBuffer("Buffer", ub));
    }
    {
        struct sTest_2_8
        {
            std140uvec3 f0;
            std140double f1;
            std140uvec2 f2;
            std140int f3;
            std140mat2 f4;
        };

        auto ub = UniformBuffer::create();
        ub->addVerification({
            {&sTest_2_8::f0, "f0"}, //
            {&sTest_2_8::f1, "f1"}, //
            {&sTest_2_8::f2, "f2"}, //
            {&sTest_2_8::f3, "f3"}, //
            {&sTest_2_8::f4, "f4"}, //
        });

        auto shader = Shader::createFromSource(GL_VERTEX_SHADER,
            "layout(std140) uniform Buffer {\n"
            "  uvec3 f0;\n"
            "  double f1;\n"
            "  uvec2 f2;\n"
            "  int f3;\n"
            "  mat2 f4;\n"
            "};\n"
            "out float x;\n"
            "void main() {\n"
            "   x = 0\n"
            "     + float(f0.x)\n"
            "     + float(f1)\n"
            "     + float(f2.x)\n"
            "     + float(f3)\n"
            "     + float(f4[0][0])\n"
            "   ;\n"
            "}\n"
         );
        ASSERT_TRUE(shader->isCompiledWithoutErrors());

        auto prog = Program::create(shader);
        ASSERT_TRUE(prog->verifyUniformBuffer("Buffer", ub));
    }
    {
        struct sTest_2_9
        {
            std140mat4x4 f0;
            std140dmat2x2 f1;
            std140uint f2;
            std140ivec4 f3;
            std140bvec2 f4;
        };

        auto ub = UniformBuffer::create();
        ub->addVerification({
            {&sTest_2_9::f0, "f0"}, //
            {&sTest_2_9::f1, "f1"}, //
            {&sTest_2_9::f2, "f2"}, //
            {&sTest_2_9::f3, "f3"}, //
            {&sTest_2_9::f4, "f4"}, //
        });

        auto shader = Shader::createFromSource(GL_VERTEX_SHADER,
            "layout(std140) uniform Buffer {\n"
            "  mat4x4 f0;\n"
            "  dmat2x2 f1;\n"
            "  uint f2;\n"
            "  ivec4 f3;\n"
            "  bvec2 f4;\n"
            "};\n"
            "out float x;\n"
            "void main() {\n"
            "   x = 0\n"
            "     + float(f0[0][0])\n"
            "     + float(f1[0][0])\n"
            "     + float(f2)\n"
            "     + float(f3.x)\n"
            "     + float(f4.x)\n"
            "   ;\n"
            "}\n"
         );
        ASSERT_TRUE(shader->isCompiledWithoutErrors());

        auto prog = Program::create(shader);
        ASSERT_TRUE(prog->verifyUniformBuffer("Buffer", ub));
    }
    {
        struct sTest_2_10
        {
            std140float f0;
            std140dmat3 f1;
            std140uint f2;
            std140int f3;
            std140bool f4;
        };

        auto ub = UniformBuffer::create();
        ub->addVerification({
            {&sTest_2_10::f0, "f0"}, //
            {&sTest_2_10::f1, "f1"}, //
            {&sTest_2_10::f2, "f2"}, //
            {&sTest_2_10::f3, "f3"}, //
            {&sTest_2_10::f4, "f4"}, //
        });

        auto shader = Shader::createFromSource(GL_VERTEX_SHADER,
            "layout(std140) uniform Buffer {\n"
            "  float f0;\n"
            "  dmat3 f1;\n"
            "  uint f2;\n"
            "  int f3;\n"
            "  bool f4;\n"
            "};\n"
            "out float x;\n"
            "void main() {\n"
            "   x = 0\n"
            "     + float(f0)\n"
            "     + float(f1[0][0])\n"
            "     + float(f2)\n"
            "     + float(f3)\n"
            "     + float(f4)\n"
            "   ;\n"
            "}\n"
         );
        ASSERT_TRUE(shader->isCompiledWithoutErrors());

        auto prog = Program::create(shader);
        ASSERT_TRUE(prog->verifyUniformBuffer("Buffer", ub));
    }
    {
        struct sTest_2_11
        {
            std140uint f0;
            std140int f1;
            std140mat2x2 f2;
            std140double f3;
            std140dvec4 f4;
        };

        auto ub = UniformBuffer::create();
        ub->addVerification({
            {&sTest_2_11::f0, "f0"}, //
            {&sTest_2_11::f1, "f1"}, //
            {&sTest_2_11::f2, "f2"}, //
            {&sTest_2_11::f3, "f3"}, //
            {&sTest_2_11::f4, "f4"}, //
        });

        auto shader = Shader::createFromSource(GL_VERTEX_SHADER,
            "layout(std140) uniform Buffer {\n"
            "  uint f0;\n"
            "  int f1;\n"
            "  mat2x2 f2;\n"
            "  double f3;\n"
            "  dvec4 f4;\n"
            "};\n"
            "out float x;\n"
            "void main() {\n"
            "   x = 0\n"
            "     + float(f0)\n"
            "     + float(f1)\n"
            "     + float(f2[0][0])\n"
            "     + float(f3)\n"
            "     + float(f4.x)\n"
            "   ;\n"
            "}\n"
         );
        ASSERT_TRUE(shader->isCompiledWithoutErrors());

        auto prog = Program::create(shader);
        ASSERT_TRUE(prog->verifyUniformBuffer("Buffer", ub));
    }
    {
        struct sTest_2_12
        {
            std140int f0;
            std140int f1;
            std140float f2;
            std140dvec4 f3;
            std140ivec2 f4;
        };

        auto ub = UniformBuffer::create();
        ub->addVerification({
            {&sTest_2_12::f0, "f0"}, //
            {&sTest_2_12::f1, "f1"}, //
            {&sTest_2_12::f2, "f2"}, //
            {&sTest_2_12::f3, "f3"}, //
            {&sTest_2_12::f4, "f4"}, //
        });

        auto shader = Shader::createFromSource(GL_VERTEX_SHADER,
            "layout(std140) uniform Buffer {\n"
            "  int f0;\n"
            "  int f1;\n"
            "  float f2;\n"
            "  dvec4 f3;\n"
            "  ivec2 f4;\n"
            "};\n"
            "out float x;\n"
            "void main() {\n"
            "   x = 0\n"
            "     + float(f0)\n"
            "     + float(f1)\n"
            "     + float(f2)\n"
            "     + float(f3.x)\n"
            "     + float(f4.x)\n"
            "   ;\n"
            "}\n"
         );
        ASSERT_TRUE(shader->isCompiledWithoutErrors());

        auto prog = Program::create(shader);
        ASSERT_TRUE(prog->verifyUniformBuffer("Buffer", ub));
    }
    {
        struct sTest_2_13
        {
            std140dmat4x3 f0;
            std140bool f1;
            std140mat2x4 f2;
            std140bvec4 f3;
            std140uvec3 f4;
        };

        auto ub = UniformBuffer::create();
        ub->addVerification({
            {&sTest_2_13::f0, "f0"}, //
            {&sTest_2_13::f1, "f1"}, //
            {&sTest_2_13::f2, "f2"}, //
            {&sTest_2_13::f3, "f3"}, //
            {&sTest_2_13::f4, "f4"}, //
        });

        auto shader = Shader::createFromSource(GL_VERTEX_SHADER,
            "layout(std140) uniform Buffer {\n"
            "  dmat4x3 f0;\n"
            "  bool f1;\n"
            "  mat2x4 f2;\n"
            "  bvec4 f3;\n"
            "  uvec3 f4;\n"
            "};\n"
            "out float x;\n"
            "void main() {\n"
            "   x = 0\n"
            "     + float(f0[0][0])\n"
            "     + float(f1)\n"
            "     + float(f2[0][0])\n"
            "     + float(f3.x)\n"
            "     + float(f4.x)\n"
            "   ;\n"
            "}\n"
         );
        ASSERT_TRUE(shader->isCompiledWithoutErrors());

        auto prog = Program::create(shader);
        ASSERT_TRUE(prog->verifyUniformBuffer("Buffer", ub));
    }
    {
        struct sTest_2_14
        {
            std140ivec2 f0;
            std140mat2 f1;
            std140dmat3x2 f2;
            std140vec3 f3;
            std140dmat2x4 f4;
        };

        auto ub = UniformBuffer::create();
        ub->addVerification({
            {&sTest_2_14::f0, "f0"}, //
            {&sTest_2_14::f1, "f1"}, //
            {&sTest_2_14::f2, "f2"}, //
            {&sTest_2_14::f3, "f3"}, //
            {&sTest_2_14::f4, "f4"}, //
        });

        auto shader = Shader::createFromSource(GL_VERTEX_SHADER,
            "layout(std140) uniform Buffer {\n"
            "  ivec2 f0;\n"
            "  mat2 f1;\n"
            "  dmat3x2 f2;\n"
            "  vec3 f3;\n"
            "  dmat2x4 f4;\n"
            "};\n"
            "out float x;\n"
            "void main() {\n"
            "   x = 0\n"
            "     + float(f0.x)\n"
            "     + float(f1[0][0])\n"
            "     + float(f2[0][0])\n"
            "     + float(f3.x)\n"
            "     + float(f4[0][0])\n"
            "   ;\n"
            "}\n"
         );
        ASSERT_TRUE(shader->isCompiledWithoutErrors());

        auto prog = Program::create(shader);
        ASSERT_TRUE(prog->verifyUniformBuffer("Buffer", ub));
    }
    {
        struct sTest_2_15
        {
            std140dmat4x2 f0;
            std140mat2x2 f1;
            std140dvec2 f2;
            std140dmat4x4 f3;
            std140uint f4;
        };

        auto ub = UniformBuffer::create();
        ub->addVerification({
            {&sTest_2_15::f0, "f0"}, //
            {&sTest_2_15::f1, "f1"}, //
            {&sTest_2_15::f2, "f2"}, //
            {&sTest_2_15::f3, "f3"}, //
            {&sTest_2_15::f4, "f4"}, //
        });

        auto shader = Shader::createFromSource(GL_VERTEX_SHADER,
            "layout(std140) uniform Buffer {\n"
            "  dmat4x2 f0;\n"
            "  mat2x2 f1;\n"
            "  dvec2 f2;\n"
            "  dmat4x4 f3;\n"
            "  uint f4;\n"
            "};\n"
            "out float x;\n"
            "void main() {\n"
            "   x = 0\n"
            "     + float(f0[0][0])\n"
            "     + float(f1[0][0])\n"
            "     + float(f2.x)\n"
            "     + float(f3[0][0])\n"
            "     + float(f4)\n"
            "   ;\n"
            "}\n"
         );
        ASSERT_TRUE(shader->isCompiledWithoutErrors());

        auto prog = Program::create(shader);
        ASSERT_TRUE(prog->verifyUniformBuffer("Buffer", ub));
    }
    {
        struct sTest_2_16
        {
            std140ivec2 f0;
            std140dmat2 f1;
            std140vec3 f2;
            std140uint f3;
            std140vec4 f4;
        };

        auto ub = UniformBuffer::create();
        ub->addVerification({
            {&sTest_2_16::f0, "f0"}, //
            {&sTest_2_16::f1, "f1"}, //
            {&sTest_2_16::f2, "f2"}, //
            {&sTest_2_16::f3, "f3"}, //
            {&sTest_2_16::f4, "f4"}, //
        });

        auto shader = Shader::createFromSource(GL_VERTEX_SHADER,
            "layout(std140) uniform Buffer {\n"
            "  ivec2 f0;\n"
            "  dmat2 f1;\n"
            "  vec3 f2;\n"
            "  uint f3;\n"
            "  vec4 f4;\n"
            "};\n"
            "out float x;\n"
            "void main() {\n"
            "   x = 0\n"
            "     + float(f0.x)\n"
            "     + float(f1[0][0])\n"
            "     + float(f2.x)\n"
            "     + float(f3)\n"
            "     + float(f4.x)\n"
            "   ;\n"
            "}\n"
         );
        ASSERT_TRUE(shader->isCompiledWithoutErrors());

        auto prog = Program::create(shader);
        ASSERT_TRUE(prog->verifyUniformBuffer("Buffer", ub));
    }
    {
        struct sTest_2_17
        {
            std140uint f0;
            std140dmat3x4 f1;
            std140dmat4 f2;
            std140ivec3 f3;
            std140vec3 f4;
        };

        auto ub = UniformBuffer::create();
        ub->addVerification({
            {&sTest_2_17::f0, "f0"}, //
            {&sTest_2_17::f1, "f1"}, //
            {&sTest_2_17::f2, "f2"}, //
            {&sTest_2_17::f3, "f3"}, //
            {&sTest_2_17::f4, "f4"}, //
        });

        auto shader = Shader::createFromSource(GL_VERTEX_SHADER,
            "layout(std140) uniform Buffer {\n"
            "  uint f0;\n"
            "  dmat3x4 f1;\n"
            "  dmat4 f2;\n"
            "  ivec3 f3;\n"
            "  vec3 f4;\n"
            "};\n"
            "out float x;\n"
            "void main() {\n"
            "   x = 0\n"
            "     + float(f0)\n"
            "     + float(f1[0][0])\n"
            "     + float(f2[0][0])\n"
            "     + float(f3.x)\n"
            "     + float(f4.x)\n"
            "   ;\n"
            "}\n"
         );
        ASSERT_TRUE(shader->isCompiledWithoutErrors());

        auto prog = Program::create(shader);
        ASSERT_TRUE(prog->verifyUniformBuffer("Buffer", ub));
    }
    {
        struct sTest_2_18
        {
            std140vec2 f0;
            std140uint f1;
            std140mat3 f2;
            std140dmat2 f3;
            std140mat4 f4;
        };

        auto ub = UniformBuffer::create();
        ub->addVerification({
            {&sTest_2_18::f0, "f0"}, //
            {&sTest_2_18::f1, "f1"}, //
            {&sTest_2_18::f2, "f2"}, //
            {&sTest_2_18::f3, "f3"}, //
            {&sTest_2_18::f4, "f4"}, //
        });

        auto shader = Shader::createFromSource(GL_VERTEX_SHADER,
            "layout(std140) uniform Buffer {\n"
            "  vec2 f0;\n"
            "  uint f1;\n"
            "  mat3 f2;\n"
            "  dmat2 f3;\n"
            "  mat4 f4;\n"
            "};\n"
            "out float x;\n"
            "void main() {\n"
            "   x = 0\n"
            "     + float(f0.x)\n"
            "     + float(f1)\n"
            "     + float(f2[0][0])\n"
            "     + float(f3[0][0])\n"
            "     + float(f4[0][0])\n"
            "   ;\n"
            "}\n"
         );
        ASSERT_TRUE(shader->isCompiledWithoutErrors());

        auto prog = Program::create(shader);
        ASSERT_TRUE(prog->verifyUniformBuffer("Buffer", ub));
    }
    {
        struct sTest_2_19
        {
            std140dmat3 f0;
            std140dmat2 f1;
            std140ivec4 f2;
            std140ivec2 f3;
            std140mat2 f4;
        };

        auto ub = UniformBuffer::create();
        ub->addVerification({
            {&sTest_2_19::f0, "f0"}, //
            {&sTest_2_19::f1, "f1"}, //
            {&sTest_2_19::f2, "f2"}, //
            {&sTest_2_19::f3, "f3"}, //
            {&sTest_2_19::f4, "f4"}, //
        });

        auto shader = Shader::createFromSource(GL_VERTEX_SHADER,
            "layout(std140) uniform Buffer {\n"
            "  dmat3 f0;\n"
            "  dmat2 f1;\n"
            "  ivec4 f2;\n"
            "  ivec2 f3;\n"
            "  mat2 f4;\n"
            "};\n"
            "out float x;\n"
            "void main() {\n"
            "   x = 0\n"
            "     + float(f0[0][0])\n"
            "     + float(f1[0][0])\n"
            "     + float(f2.x)\n"
            "     + float(f3.x)\n"
            "     + float(f4[0][0])\n"
            "   ;\n"
            "}\n"
         );
        ASSERT_TRUE(shader->isCompiledWithoutErrors());

        auto prog = Program::create(shader);
        ASSERT_TRUE(prog->verifyUniformBuffer("Buffer", ub));
    }
    {
        struct sTest_2_20
        {
            std140bvec3 f0;
            std140bvec4 f1;
            std140double f2;
            std140float f3;
            std140float f4;
        };

        auto ub = UniformBuffer::create();
        ub->addVerification({
            {&sTest_2_20::f0, "f0"}, //
            {&sTest_2_20::f1, "f1"}, //
            {&sTest_2_20::f2, "f2"}, //
            {&sTest_2_20::f3, "f3"}, //
            {&sTest_2_20::f4, "f4"}, //
        });

        auto shader = Shader::createFromSource(GL_VERTEX_SHADER,
            "layout(std140) uniform Buffer {\n"
            "  bvec3 f0;\n"
            "  bvec4 f1;\n"
            "  double f2;\n"
            "  float f3;\n"
            "  float f4;\n"
            "};\n"
            "out float x;\n"
            "void main() {\n"
            "   x = 0\n"
            "     + float(f0.x)\n"
            "     + float(f1.x)\n"
            "     + float(f2)\n"
            "     + float(f3)\n"
            "     + float(f4)\n"
            "   ;\n"
            "}\n"
         );
        ASSERT_TRUE(shader->isCompiledWithoutErrors());

        auto prog = Program::create(shader);
        ASSERT_TRUE(prog->verifyUniformBuffer("Buffer", ub));
    }
    {
        struct sTest_2_21
        {
            std140dmat3x4 f0;
            std140vec2 f1;
            std140bool f2;
            std140double f3;
            std140vec2 f4;
        };

        auto ub = UniformBuffer::create();
        ub->addVerification({
            {&sTest_2_21::f0, "f0"}, //
            {&sTest_2_21::f1, "f1"}, //
            {&sTest_2_21::f2, "f2"}, //
            {&sTest_2_21::f3, "f3"}, //
            {&sTest_2_21::f4, "f4"}, //
        });

        auto shader = Shader::createFromSource(GL_VERTEX_SHADER,
            "layout(std140) uniform Buffer {\n"
            "  dmat3x4 f0;\n"
            "  vec2 f1;\n"
            "  bool f2;\n"
            "  double f3;\n"
            "  vec2 f4;\n"
            "};\n"
            "out float x;\n"
            "void main() {\n"
            "   x = 0\n"
            "     + float(f0[0][0])\n"
            "     + float(f1.x)\n"
            "     + float(f2)\n"
            "     + float(f3)\n"
            "     + float(f4.x)\n"
            "   ;\n"
            "}\n"
         );
        ASSERT_TRUE(shader->isCompiledWithoutErrors());

        auto prog = Program::create(shader);
        ASSERT_TRUE(prog->verifyUniformBuffer("Buffer", ub));
    }
    {
        struct sTest_2_22
        {
            std140uint f0;
            std140mat4x2 f1;
            std140ivec2 f2;
            std140mat2 f3;
            std140mat2 f4;
        };

        auto ub = UniformBuffer::create();
        ub->addVerification({
            {&sTest_2_22::f0, "f0"}, //
            {&sTest_2_22::f1, "f1"}, //
            {&sTest_2_22::f2, "f2"}, //
            {&sTest_2_22::f3, "f3"}, //
            {&sTest_2_22::f4, "f4"}, //
        });

        auto shader = Shader::createFromSource(GL_VERTEX_SHADER,
            "layout(std140) uniform Buffer {\n"
            "  uint f0;\n"
            "  mat4x2 f1;\n"
            "  ivec2 f2;\n"
            "  mat2 f3;\n"
            "  mat2 f4;\n"
            "};\n"
            "out float x;\n"
            "void main() {\n"
            "   x = 0\n"
            "     + float(f0)\n"
            "     + float(f1[0][0])\n"
            "     + float(f2.x)\n"
            "     + float(f3[0][0])\n"
            "     + float(f4[0][0])\n"
            "   ;\n"
            "}\n"
         );
        ASSERT_TRUE(shader->isCompiledWithoutErrors());

        auto prog = Program::create(shader);
        ASSERT_TRUE(prog->verifyUniformBuffer("Buffer", ub));
    }
    {
        struct sTest_2_23
        {
            std140float f0;
            std140double f1;
            std140double f2;
            std140double f3;
            std140vec2 f4;
        };

        auto ub = UniformBuffer::create();
        ub->addVerification({
            {&sTest_2_23::f0, "f0"}, //
            {&sTest_2_23::f1, "f1"}, //
            {&sTest_2_23::f2, "f2"}, //
            {&sTest_2_23::f3, "f3"}, //
            {&sTest_2_23::f4, "f4"}, //
        });

        auto shader = Shader::createFromSource(GL_VERTEX_SHADER,
            "layout(std140) uniform Buffer {\n"
            "  float f0;\n"
            "  double f1;\n"
            "  double f2;\n"
            "  double f3;\n"
            "  vec2 f4;\n"
            "};\n"
            "out float x;\n"
            "void main() {\n"
            "   x = 0\n"
            "     + float(f0)\n"
            "     + float(f1)\n"
            "     + float(f2)\n"
            "     + float(f3)\n"
            "     + float(f4.x)\n"
            "   ;\n"
            "}\n"
         );
        ASSERT_TRUE(shader->isCompiledWithoutErrors());

        auto prog = Program::create(shader);
        ASSERT_TRUE(prog->verifyUniformBuffer("Buffer", ub));
    }
    {
        struct sTest_2_24
        {
            std140uvec2 f0;
            std140dvec2 f1;
            std140dvec4 f2;
            std140dmat2 f3;
            std140ivec2 f4;
        };

        auto ub = UniformBuffer::create();
        ub->addVerification({
            {&sTest_2_24::f0, "f0"}, //
            {&sTest_2_24::f1, "f1"}, //
            {&sTest_2_24::f2, "f2"}, //
            {&sTest_2_24::f3, "f3"}, //
            {&sTest_2_24::f4, "f4"}, //
        });

        auto shader = Shader::createFromSource(GL_VERTEX_SHADER,
            "layout(std140) uniform Buffer {\n"
            "  uvec2 f0;\n"
            "  dvec2 f1;\n"
            "  dvec4 f2;\n"
            "  dmat2 f3;\n"
            "  ivec2 f4;\n"
            "};\n"
            "out float x;\n"
            "void main() {\n"
            "   x = 0\n"
            "     + float(f0.x)\n"
            "     + float(f1.x)\n"
            "     + float(f2.x)\n"
            "     + float(f3[0][0])\n"
            "     + float(f4.x)\n"
            "   ;\n"
            "}\n"
         );
        ASSERT_TRUE(shader->isCompiledWithoutErrors());

        auto prog = Program::create(shader);
        ASSERT_TRUE(prog->verifyUniformBuffer("Buffer", ub));
    }
    {
        struct sTest_2_25
        {
            std140mat4 f0;
            std140dmat3 f1;
            std140mat4 f2;
            std140vec3 f3;
            std140uint f4;
        };

        auto ub = UniformBuffer::create();
        ub->addVerification({
            {&sTest_2_25::f0, "f0"}, //
            {&sTest_2_25::f1, "f1"}, //
            {&sTest_2_25::f2, "f2"}, //
            {&sTest_2_25::f3, "f3"}, //
            {&sTest_2_25::f4, "f4"}, //
        });

        auto shader = Shader::createFromSource(GL_VERTEX_SHADER,
            "layout(std140) uniform Buffer {\n"
            "  mat4 f0;\n"
            "  dmat3 f1;\n"
            "  mat4 f2;\n"
            "  vec3 f3;\n"
            "  uint f4;\n"
            "};\n"
            "out float x;\n"
            "void main() {\n"
            "   x = 0\n"
            "     + float(f0[0][0])\n"
            "     + float(f1[0][0])\n"
            "     + float(f2[0][0])\n"
            "     + float(f3.x)\n"
            "     + float(f4)\n"
            "   ;\n"
            "}\n"
         );
        ASSERT_TRUE(shader->isCompiledWithoutErrors());

        auto prog = Program::create(shader);
        ASSERT_TRUE(prog->verifyUniformBuffer("Buffer", ub));
    }
    {
        struct sTest_2_26
        {
            std140bvec4 f0;
            std140dmat2x4 f1;
            std140int f2;
            std140uint f3;
            std140mat3 f4;
        };

        auto ub = UniformBuffer::create();
        ub->addVerification({
            {&sTest_2_26::f0, "f0"}, //
            {&sTest_2_26::f1, "f1"}, //
            {&sTest_2_26::f2, "f2"}, //
            {&sTest_2_26::f3, "f3"}, //
            {&sTest_2_26::f4, "f4"}, //
        });

        auto shader = Shader::createFromSource(GL_VERTEX_SHADER,
            "layout(std140) uniform Buffer {\n"
            "  bvec4 f0;\n"
            "  dmat2x4 f1;\n"
            "  int f2;\n"
            "  uint f3;\n"
            "  mat3 f4;\n"
            "};\n"
            "out float x;\n"
            "void main() {\n"
            "   x = 0\n"
            "     + float(f0.x)\n"
            "     + float(f1[0][0])\n"
            "     + float(f2)\n"
            "     + float(f3)\n"
            "     + float(f4[0][0])\n"
            "   ;\n"
            "}\n"
         );
        ASSERT_TRUE(shader->isCompiledWithoutErrors());

        auto prog = Program::create(shader);
        ASSERT_TRUE(prog->verifyUniformBuffer("Buffer", ub));
    }
    {
        struct sTest_2_27
        {
            std140dmat4x2 f0;
            std140int f1;
            std140vec2 f2;
            std140dvec4 f3;
            std140mat4x2 f4;
        };

        auto ub = UniformBuffer::create();
        ub->addVerification({
            {&sTest_2_27::f0, "f0"}, //
            {&sTest_2_27::f1, "f1"}, //
            {&sTest_2_27::f2, "f2"}, //
            {&sTest_2_27::f3, "f3"}, //
            {&sTest_2_27::f4, "f4"}, //
        });

        auto shader = Shader::createFromSource(GL_VERTEX_SHADER,
            "layout(std140) uniform Buffer {\n"
            "  dmat4x2 f0;\n"
            "  int f1;\n"
            "  vec2 f2;\n"
            "  dvec4 f3;\n"
            "  mat4x2 f4;\n"
            "};\n"
            "out float x;\n"
            "void main() {\n"
            "   x = 0\n"
            "     + float(f0[0][0])\n"
            "     + float(f1)\n"
            "     + float(f2.x)\n"
            "     + float(f3.x)\n"
            "     + float(f4[0][0])\n"
            "   ;\n"
            "}\n"
         );
        ASSERT_TRUE(shader->isCompiledWithoutErrors());

        auto prog = Program::create(shader);
        ASSERT_TRUE(prog->verifyUniformBuffer("Buffer", ub));
    }
    {
        struct sTest_2_28
        {
            std140dmat2 f0;
            std140bool f1;
            std140uint f2;
            std140dmat4 f3;
            std140double f4;
        };

        auto ub = UniformBuffer::create();
        ub->addVerification({
            {&sTest_2_28::f0, "f0"}, //
            {&sTest_2_28::f1, "f1"}, //
            {&sTest_2_28::f2, "f2"}, //
            {&sTest_2_28::f3, "f3"}, //
            {&sTest_2_28::f4, "f4"}, //
        });

        auto shader = Shader::createFromSource(GL_VERTEX_SHADER,
            "layout(std140) uniform Buffer {\n"
            "  dmat2 f0;\n"
            "  bool f1;\n"
            "  uint f2;\n"
            "  dmat4 f3;\n"
            "  double f4;\n"
            "};\n"
            "out float x;\n"
            "void main() {\n"
            "   x = 0\n"
            "     + float(f0[0][0])\n"
            "     + float(f1)\n"
            "     + float(f2)\n"
            "     + float(f3[0][0])\n"
            "     + float(f4)\n"
            "   ;\n"
            "}\n"
         );
        ASSERT_TRUE(shader->isCompiledWithoutErrors());

        auto prog = Program::create(shader);
        ASSERT_TRUE(prog->verifyUniformBuffer("Buffer", ub));
    }
    {
        struct sTest_2_29
        {
            std140mat2x3 f0;
            std140mat2x4 f1;
            std140double f2;
            std140mat2 f3;
            std140bvec2 f4;
        };

        auto ub = UniformBuffer::create();
        ub->addVerification({
            {&sTest_2_29::f0, "f0"}, //
            {&sTest_2_29::f1, "f1"}, //
            {&sTest_2_29::f2, "f2"}, //
            {&sTest_2_29::f3, "f3"}, //
            {&sTest_2_29::f4, "f4"}, //
        });

        auto shader = Shader::createFromSource(GL_VERTEX_SHADER,
            "layout(std140) uniform Buffer {\n"
            "  mat2x3 f0;\n"
            "  mat2x4 f1;\n"
            "  double f2;\n"
            "  mat2 f3;\n"
            "  bvec2 f4;\n"
            "};\n"
            "out float x;\n"
            "void main() {\n"
            "   x = 0\n"
            "     + float(f0[0][0])\n"
            "     + float(f1[0][0])\n"
            "     + float(f2)\n"
            "     + float(f3[0][0])\n"
            "     + float(f4.x)\n"
            "   ;\n"
            "}\n"
         );
        ASSERT_TRUE(shader->isCompiledWithoutErrors());

        auto prog = Program::create(shader);
        ASSERT_TRUE(prog->verifyUniformBuffer("Buffer", ub));
    }
}

