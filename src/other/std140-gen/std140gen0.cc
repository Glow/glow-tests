// This file is auto-generated and should not be modified directly.
#include "tests.hh"

#include <glow/std140.hh>
#include <glow/objects/UniformBuffer.hh>
#include <glow/objects/Shader.hh>
#include <glow/objects/Program.hh>


GLOW_TEST(Std140Gen, Test0)
{
    {
        struct sTest_0_0
        {
            std140vec4 f0;
            std140uint f1;
            std140float f2;
        };

        auto ub = UniformBuffer::create();
        ub->addVerification({
            {&sTest_0_0::f0, "f0"}, //
            {&sTest_0_0::f1, "f1"}, //
            {&sTest_0_0::f2, "f2"}, //
        });

        auto shader = Shader::createFromSource(GL_VERTEX_SHADER,
            "layout(std140) uniform Buffer {\n"
            "  vec4 f0;\n"
            "  uint f1;\n"
            "  float f2;\n"
            "};\n"
            "out float x;\n"
            "void main() {\n"
            "   x = 0\n"
            "     + float(f0.x)\n"
            "     + float(f1)\n"
            "     + float(f2)\n"
            "   ;\n"
            "}\n"
         );
        ASSERT_TRUE(shader->isCompiledWithoutErrors());

        auto prog = Program::create(shader);
        ASSERT_TRUE(prog->verifyUniformBuffer("Buffer", ub));
    }
    {
        struct sTest_0_1
        {
            std140bvec3 f0;
            std140mat4x3 f1;
            std140uvec4 f2;
        };

        auto ub = UniformBuffer::create();
        ub->addVerification({
            {&sTest_0_1::f0, "f0"}, //
            {&sTest_0_1::f1, "f1"}, //
            {&sTest_0_1::f2, "f2"}, //
        });

        auto shader = Shader::createFromSource(GL_VERTEX_SHADER,
            "layout(std140) uniform Buffer {\n"
            "  bvec3 f0;\n"
            "  mat4x3 f1;\n"
            "  uvec4 f2;\n"
            "};\n"
            "out float x;\n"
            "void main() {\n"
            "   x = 0\n"
            "     + float(f0.x)\n"
            "     + float(f1[0][0])\n"
            "     + float(f2.x)\n"
            "   ;\n"
            "}\n"
         );
        ASSERT_TRUE(shader->isCompiledWithoutErrors());

        auto prog = Program::create(shader);
        ASSERT_TRUE(prog->verifyUniformBuffer("Buffer", ub));
    }
    {
        struct sTest_0_2
        {
            std140dvec3 f0;
            std140ivec3 f1;
            std140dvec3 f2;
        };

        auto ub = UniformBuffer::create();
        ub->addVerification({
            {&sTest_0_2::f0, "f0"}, //
            {&sTest_0_2::f1, "f1"}, //
            {&sTest_0_2::f2, "f2"}, //
        });

        auto shader = Shader::createFromSource(GL_VERTEX_SHADER,
            "layout(std140) uniform Buffer {\n"
            "  dvec3 f0;\n"
            "  ivec3 f1;\n"
            "  dvec3 f2;\n"
            "};\n"
            "out float x;\n"
            "void main() {\n"
            "   x = 0\n"
            "     + float(f0.x)\n"
            "     + float(f1.x)\n"
            "     + float(f2.x)\n"
            "   ;\n"
            "}\n"
         );
        ASSERT_TRUE(shader->isCompiledWithoutErrors());

        auto prog = Program::create(shader);
        ASSERT_TRUE(prog->verifyUniformBuffer("Buffer", ub));
    }
    {
        struct sTest_0_3
        {
            std140bool f0;
            std140uvec2 f1;
            std140bool f2;
        };

        auto ub = UniformBuffer::create();
        ub->addVerification({
            {&sTest_0_3::f0, "f0"}, //
            {&sTest_0_3::f1, "f1"}, //
            {&sTest_0_3::f2, "f2"}, //
        });

        auto shader = Shader::createFromSource(GL_VERTEX_SHADER,
            "layout(std140) uniform Buffer {\n"
            "  bool f0;\n"
            "  uvec2 f1;\n"
            "  bool f2;\n"
            "};\n"
            "out float x;\n"
            "void main() {\n"
            "   x = 0\n"
            "     + float(f0)\n"
            "     + float(f1.x)\n"
            "     + float(f2)\n"
            "   ;\n"
            "}\n"
         );
        ASSERT_TRUE(shader->isCompiledWithoutErrors());

        auto prog = Program::create(shader);
        ASSERT_TRUE(prog->verifyUniformBuffer("Buffer", ub));
    }
    {
        struct sTest_0_4
        {
            std140bool f0;
            std140dvec4 f1;
            std140uint f2;
        };

        auto ub = UniformBuffer::create();
        ub->addVerification({
            {&sTest_0_4::f0, "f0"}, //
            {&sTest_0_4::f1, "f1"}, //
            {&sTest_0_4::f2, "f2"}, //
        });

        auto shader = Shader::createFromSource(GL_VERTEX_SHADER,
            "layout(std140) uniform Buffer {\n"
            "  bool f0;\n"
            "  dvec4 f1;\n"
            "  uint f2;\n"
            "};\n"
            "out float x;\n"
            "void main() {\n"
            "   x = 0\n"
            "     + float(f0)\n"
            "     + float(f1.x)\n"
            "     + float(f2)\n"
            "   ;\n"
            "}\n"
         );
        ASSERT_TRUE(shader->isCompiledWithoutErrors());

        auto prog = Program::create(shader);
        ASSERT_TRUE(prog->verifyUniformBuffer("Buffer", ub));
    }
    {
        struct sTest_0_5
        {
            std140bool f0;
            std140bvec3 f1;
            std140bvec4 f2;
        };

        auto ub = UniformBuffer::create();
        ub->addVerification({
            {&sTest_0_5::f0, "f0"}, //
            {&sTest_0_5::f1, "f1"}, //
            {&sTest_0_5::f2, "f2"}, //
        });

        auto shader = Shader::createFromSource(GL_VERTEX_SHADER,
            "layout(std140) uniform Buffer {\n"
            "  bool f0;\n"
            "  bvec3 f1;\n"
            "  bvec4 f2;\n"
            "};\n"
            "out float x;\n"
            "void main() {\n"
            "   x = 0\n"
            "     + float(f0)\n"
            "     + float(f1.x)\n"
            "     + float(f2.x)\n"
            "   ;\n"
            "}\n"
         );
        ASSERT_TRUE(shader->isCompiledWithoutErrors());

        auto prog = Program::create(shader);
        ASSERT_TRUE(prog->verifyUniformBuffer("Buffer", ub));
    }
    {
        struct sTest_0_6
        {
            std140uvec3 f0;
            std140float f1;
            std140mat2x2 f2;
        };

        auto ub = UniformBuffer::create();
        ub->addVerification({
            {&sTest_0_6::f0, "f0"}, //
            {&sTest_0_6::f1, "f1"}, //
            {&sTest_0_6::f2, "f2"}, //
        });

        auto shader = Shader::createFromSource(GL_VERTEX_SHADER,
            "layout(std140) uniform Buffer {\n"
            "  uvec3 f0;\n"
            "  float f1;\n"
            "  mat2x2 f2;\n"
            "};\n"
            "out float x;\n"
            "void main() {\n"
            "   x = 0\n"
            "     + float(f0.x)\n"
            "     + float(f1)\n"
            "     + float(f2[0][0])\n"
            "   ;\n"
            "}\n"
         );
        ASSERT_TRUE(shader->isCompiledWithoutErrors());

        auto prog = Program::create(shader);
        ASSERT_TRUE(prog->verifyUniformBuffer("Buffer", ub));
    }
    {
        struct sTest_0_7
        {
            std140vec3 f0;
            std140mat2 f1;
            std140double f2;
        };

        auto ub = UniformBuffer::create();
        ub->addVerification({
            {&sTest_0_7::f0, "f0"}, //
            {&sTest_0_7::f1, "f1"}, //
            {&sTest_0_7::f2, "f2"}, //
        });

        auto shader = Shader::createFromSource(GL_VERTEX_SHADER,
            "layout(std140) uniform Buffer {\n"
            "  vec3 f0;\n"
            "  mat2 f1;\n"
            "  double f2;\n"
            "};\n"
            "out float x;\n"
            "void main() {\n"
            "   x = 0\n"
            "     + float(f0.x)\n"
            "     + float(f1[0][0])\n"
            "     + float(f2)\n"
            "   ;\n"
            "}\n"
         );
        ASSERT_TRUE(shader->isCompiledWithoutErrors());

        auto prog = Program::create(shader);
        ASSERT_TRUE(prog->verifyUniformBuffer("Buffer", ub));
    }
    {
        struct sTest_0_8
        {
            std140ivec3 f0;
            std140dmat3x3 f1;
            std140dmat2 f2;
        };

        auto ub = UniformBuffer::create();
        ub->addVerification({
            {&sTest_0_8::f0, "f0"}, //
            {&sTest_0_8::f1, "f1"}, //
            {&sTest_0_8::f2, "f2"}, //
        });

        auto shader = Shader::createFromSource(GL_VERTEX_SHADER,
            "layout(std140) uniform Buffer {\n"
            "  ivec3 f0;\n"
            "  dmat3x3 f1;\n"
            "  dmat2 f2;\n"
            "};\n"
            "out float x;\n"
            "void main() {\n"
            "   x = 0\n"
            "     + float(f0.x)\n"
            "     + float(f1[0][0])\n"
            "     + float(f2[0][0])\n"
            "   ;\n"
            "}\n"
         );
        ASSERT_TRUE(shader->isCompiledWithoutErrors());

        auto prog = Program::create(shader);
        ASSERT_TRUE(prog->verifyUniformBuffer("Buffer", ub));
    }
    {
        struct sTest_0_9
        {
            std140uint f0;
            std140dvec3 f1;
            std140double f2;
        };

        auto ub = UniformBuffer::create();
        ub->addVerification({
            {&sTest_0_9::f0, "f0"}, //
            {&sTest_0_9::f1, "f1"}, //
            {&sTest_0_9::f2, "f2"}, //
        });

        auto shader = Shader::createFromSource(GL_VERTEX_SHADER,
            "layout(std140) uniform Buffer {\n"
            "  uint f0;\n"
            "  dvec3 f1;\n"
            "  double f2;\n"
            "};\n"
            "out float x;\n"
            "void main() {\n"
            "   x = 0\n"
            "     + float(f0)\n"
            "     + float(f1.x)\n"
            "     + float(f2)\n"
            "   ;\n"
            "}\n"
         );
        ASSERT_TRUE(shader->isCompiledWithoutErrors());

        auto prog = Program::create(shader);
        ASSERT_TRUE(prog->verifyUniformBuffer("Buffer", ub));
    }
    {
        struct sTest_0_10
        {
            std140mat4 f0;
            std140int f1;
            std140dmat3x2 f2;
        };

        auto ub = UniformBuffer::create();
        ub->addVerification({
            {&sTest_0_10::f0, "f0"}, //
            {&sTest_0_10::f1, "f1"}, //
            {&sTest_0_10::f2, "f2"}, //
        });

        auto shader = Shader::createFromSource(GL_VERTEX_SHADER,
            "layout(std140) uniform Buffer {\n"
            "  mat4 f0;\n"
            "  int f1;\n"
            "  dmat3x2 f2;\n"
            "};\n"
            "out float x;\n"
            "void main() {\n"
            "   x = 0\n"
            "     + float(f0[0][0])\n"
            "     + float(f1)\n"
            "     + float(f2[0][0])\n"
            "   ;\n"
            "}\n"
         );
        ASSERT_TRUE(shader->isCompiledWithoutErrors());

        auto prog = Program::create(shader);
        ASSERT_TRUE(prog->verifyUniformBuffer("Buffer", ub));
    }
    {
        struct sTest_0_11
        {
            std140dmat4 f0;
            std140dvec4 f1;
            std140uint f2;
        };

        auto ub = UniformBuffer::create();
        ub->addVerification({
            {&sTest_0_11::f0, "f0"}, //
            {&sTest_0_11::f1, "f1"}, //
            {&sTest_0_11::f2, "f2"}, //
        });

        auto shader = Shader::createFromSource(GL_VERTEX_SHADER,
            "layout(std140) uniform Buffer {\n"
            "  dmat4 f0;\n"
            "  dvec4 f1;\n"
            "  uint f2;\n"
            "};\n"
            "out float x;\n"
            "void main() {\n"
            "   x = 0\n"
            "     + float(f0[0][0])\n"
            "     + float(f1.x)\n"
            "     + float(f2)\n"
            "   ;\n"
            "}\n"
         );
        ASSERT_TRUE(shader->isCompiledWithoutErrors());

        auto prog = Program::create(shader);
        ASSERT_TRUE(prog->verifyUniformBuffer("Buffer", ub));
    }
    {
        struct sTest_0_12
        {
            std140bvec2 f0;
            std140dmat4x3 f1;
            std140dmat3x4 f2;
        };

        auto ub = UniformBuffer::create();
        ub->addVerification({
            {&sTest_0_12::f0, "f0"}, //
            {&sTest_0_12::f1, "f1"}, //
            {&sTest_0_12::f2, "f2"}, //
        });

        auto shader = Shader::createFromSource(GL_VERTEX_SHADER,
            "layout(std140) uniform Buffer {\n"
            "  bvec2 f0;\n"
            "  dmat4x3 f1;\n"
            "  dmat3x4 f2;\n"
            "};\n"
            "out float x;\n"
            "void main() {\n"
            "   x = 0\n"
            "     + float(f0.x)\n"
            "     + float(f1[0][0])\n"
            "     + float(f2[0][0])\n"
            "   ;\n"
            "}\n"
         );
        ASSERT_TRUE(shader->isCompiledWithoutErrors());

        auto prog = Program::create(shader);
        ASSERT_TRUE(prog->verifyUniformBuffer("Buffer", ub));
    }
    {
        struct sTest_0_13
        {
            std140mat4 f0;
            std140vec2 f1;
            std140bool f2;
        };

        auto ub = UniformBuffer::create();
        ub->addVerification({
            {&sTest_0_13::f0, "f0"}, //
            {&sTest_0_13::f1, "f1"}, //
            {&sTest_0_13::f2, "f2"}, //
        });

        auto shader = Shader::createFromSource(GL_VERTEX_SHADER,
            "layout(std140) uniform Buffer {\n"
            "  mat4 f0;\n"
            "  vec2 f1;\n"
            "  bool f2;\n"
            "};\n"
            "out float x;\n"
            "void main() {\n"
            "   x = 0\n"
            "     + float(f0[0][0])\n"
            "     + float(f1.x)\n"
            "     + float(f2)\n"
            "   ;\n"
            "}\n"
         );
        ASSERT_TRUE(shader->isCompiledWithoutErrors());

        auto prog = Program::create(shader);
        ASSERT_TRUE(prog->verifyUniformBuffer("Buffer", ub));
    }
    {
        struct sTest_0_14
        {
            std140int f0;
            std140uint f1;
            std140ivec3 f2;
        };

        auto ub = UniformBuffer::create();
        ub->addVerification({
            {&sTest_0_14::f0, "f0"}, //
            {&sTest_0_14::f1, "f1"}, //
            {&sTest_0_14::f2, "f2"}, //
        });

        auto shader = Shader::createFromSource(GL_VERTEX_SHADER,
            "layout(std140) uniform Buffer {\n"
            "  int f0;\n"
            "  uint f1;\n"
            "  ivec3 f2;\n"
            "};\n"
            "out float x;\n"
            "void main() {\n"
            "   x = 0\n"
            "     + float(f0)\n"
            "     + float(f1)\n"
            "     + float(f2.x)\n"
            "   ;\n"
            "}\n"
         );
        ASSERT_TRUE(shader->isCompiledWithoutErrors());

        auto prog = Program::create(shader);
        ASSERT_TRUE(prog->verifyUniformBuffer("Buffer", ub));
    }
    {
        struct sTest_0_15
        {
            std140mat4 f0;
            std140dvec3 f1;
            std140double f2;
        };

        auto ub = UniformBuffer::create();
        ub->addVerification({
            {&sTest_0_15::f0, "f0"}, //
            {&sTest_0_15::f1, "f1"}, //
            {&sTest_0_15::f2, "f2"}, //
        });

        auto shader = Shader::createFromSource(GL_VERTEX_SHADER,
            "layout(std140) uniform Buffer {\n"
            "  mat4 f0;\n"
            "  dvec3 f1;\n"
            "  double f2;\n"
            "};\n"
            "out float x;\n"
            "void main() {\n"
            "   x = 0\n"
            "     + float(f0[0][0])\n"
            "     + float(f1.x)\n"
            "     + float(f2)\n"
            "   ;\n"
            "}\n"
         );
        ASSERT_TRUE(shader->isCompiledWithoutErrors());

        auto prog = Program::create(shader);
        ASSERT_TRUE(prog->verifyUniformBuffer("Buffer", ub));
    }
    {
        struct sTest_0_16
        {
            std140ivec2 f0;
            std140double f1;
            std140bool f2;
        };

        auto ub = UniformBuffer::create();
        ub->addVerification({
            {&sTest_0_16::f0, "f0"}, //
            {&sTest_0_16::f1, "f1"}, //
            {&sTest_0_16::f2, "f2"}, //
        });

        auto shader = Shader::createFromSource(GL_VERTEX_SHADER,
            "layout(std140) uniform Buffer {\n"
            "  ivec2 f0;\n"
            "  double f1;\n"
            "  bool f2;\n"
            "};\n"
            "out float x;\n"
            "void main() {\n"
            "   x = 0\n"
            "     + float(f0.x)\n"
            "     + float(f1)\n"
            "     + float(f2)\n"
            "   ;\n"
            "}\n"
         );
        ASSERT_TRUE(shader->isCompiledWithoutErrors());

        auto prog = Program::create(shader);
        ASSERT_TRUE(prog->verifyUniformBuffer("Buffer", ub));
    }
    {
        struct sTest_0_17
        {
            std140uvec2 f0;
            std140dmat2 f1;
            std140mat3x2 f2;
        };

        auto ub = UniformBuffer::create();
        ub->addVerification({
            {&sTest_0_17::f0, "f0"}, //
            {&sTest_0_17::f1, "f1"}, //
            {&sTest_0_17::f2, "f2"}, //
        });

        auto shader = Shader::createFromSource(GL_VERTEX_SHADER,
            "layout(std140) uniform Buffer {\n"
            "  uvec2 f0;\n"
            "  dmat2 f1;\n"
            "  mat3x2 f2;\n"
            "};\n"
            "out float x;\n"
            "void main() {\n"
            "   x = 0\n"
            "     + float(f0.x)\n"
            "     + float(f1[0][0])\n"
            "     + float(f2[0][0])\n"
            "   ;\n"
            "}\n"
         );
        ASSERT_TRUE(shader->isCompiledWithoutErrors());

        auto prog = Program::create(shader);
        ASSERT_TRUE(prog->verifyUniformBuffer("Buffer", ub));
    }
    {
        struct sTest_0_18
        {
            std140mat2x4 f0;
            std140float f1;
            std140mat2x3 f2;
        };

        auto ub = UniformBuffer::create();
        ub->addVerification({
            {&sTest_0_18::f0, "f0"}, //
            {&sTest_0_18::f1, "f1"}, //
            {&sTest_0_18::f2, "f2"}, //
        });

        auto shader = Shader::createFromSource(GL_VERTEX_SHADER,
            "layout(std140) uniform Buffer {\n"
            "  mat2x4 f0;\n"
            "  float f1;\n"
            "  mat2x3 f2;\n"
            "};\n"
            "out float x;\n"
            "void main() {\n"
            "   x = 0\n"
            "     + float(f0[0][0])\n"
            "     + float(f1)\n"
            "     + float(f2[0][0])\n"
            "   ;\n"
            "}\n"
         );
        ASSERT_TRUE(shader->isCompiledWithoutErrors());

        auto prog = Program::create(shader);
        ASSERT_TRUE(prog->verifyUniformBuffer("Buffer", ub));
    }
    {
        struct sTest_0_19
        {
            std140bool f0;
            std140dmat2x2 f1;
            std140uvec4 f2;
        };

        auto ub = UniformBuffer::create();
        ub->addVerification({
            {&sTest_0_19::f0, "f0"}, //
            {&sTest_0_19::f1, "f1"}, //
            {&sTest_0_19::f2, "f2"}, //
        });

        auto shader = Shader::createFromSource(GL_VERTEX_SHADER,
            "layout(std140) uniform Buffer {\n"
            "  bool f0;\n"
            "  dmat2x2 f1;\n"
            "  uvec4 f2;\n"
            "};\n"
            "out float x;\n"
            "void main() {\n"
            "   x = 0\n"
            "     + float(f0)\n"
            "     + float(f1[0][0])\n"
            "     + float(f2.x)\n"
            "   ;\n"
            "}\n"
         );
        ASSERT_TRUE(shader->isCompiledWithoutErrors());

        auto prog = Program::create(shader);
        ASSERT_TRUE(prog->verifyUniformBuffer("Buffer", ub));
    }
    {
        struct sTest_0_20
        {
            std140ivec3 f0;
            std140dmat3 f1;
            std140mat4 f2;
        };

        auto ub = UniformBuffer::create();
        ub->addVerification({
            {&sTest_0_20::f0, "f0"}, //
            {&sTest_0_20::f1, "f1"}, //
            {&sTest_0_20::f2, "f2"}, //
        });

        auto shader = Shader::createFromSource(GL_VERTEX_SHADER,
            "layout(std140) uniform Buffer {\n"
            "  ivec3 f0;\n"
            "  dmat3 f1;\n"
            "  mat4 f2;\n"
            "};\n"
            "out float x;\n"
            "void main() {\n"
            "   x = 0\n"
            "     + float(f0.x)\n"
            "     + float(f1[0][0])\n"
            "     + float(f2[0][0])\n"
            "   ;\n"
            "}\n"
         );
        ASSERT_TRUE(shader->isCompiledWithoutErrors());

        auto prog = Program::create(shader);
        ASSERT_TRUE(prog->verifyUniformBuffer("Buffer", ub));
    }
    {
        struct sTest_0_21
        {
            std140double f0;
            std140uvec2 f1;
            std140ivec2 f2;
        };

        auto ub = UniformBuffer::create();
        ub->addVerification({
            {&sTest_0_21::f0, "f0"}, //
            {&sTest_0_21::f1, "f1"}, //
            {&sTest_0_21::f2, "f2"}, //
        });

        auto shader = Shader::createFromSource(GL_VERTEX_SHADER,
            "layout(std140) uniform Buffer {\n"
            "  double f0;\n"
            "  uvec2 f1;\n"
            "  ivec2 f2;\n"
            "};\n"
            "out float x;\n"
            "void main() {\n"
            "   x = 0\n"
            "     + float(f0)\n"
            "     + float(f1.x)\n"
            "     + float(f2.x)\n"
            "   ;\n"
            "}\n"
         );
        ASSERT_TRUE(shader->isCompiledWithoutErrors());

        auto prog = Program::create(shader);
        ASSERT_TRUE(prog->verifyUniformBuffer("Buffer", ub));
    }
    {
        struct sTest_0_22
        {
            std140double f0;
            std140dmat3x4 f1;
            std140float f2;
        };

        auto ub = UniformBuffer::create();
        ub->addVerification({
            {&sTest_0_22::f0, "f0"}, //
            {&sTest_0_22::f1, "f1"}, //
            {&sTest_0_22::f2, "f2"}, //
        });

        auto shader = Shader::createFromSource(GL_VERTEX_SHADER,
            "layout(std140) uniform Buffer {\n"
            "  double f0;\n"
            "  dmat3x4 f1;\n"
            "  float f2;\n"
            "};\n"
            "out float x;\n"
            "void main() {\n"
            "   x = 0\n"
            "     + float(f0)\n"
            "     + float(f1[0][0])\n"
            "     + float(f2)\n"
            "   ;\n"
            "}\n"
         );
        ASSERT_TRUE(shader->isCompiledWithoutErrors());

        auto prog = Program::create(shader);
        ASSERT_TRUE(prog->verifyUniformBuffer("Buffer", ub));
    }
    {
        struct sTest_0_23
        {
            std140mat3x4 f0;
            std140mat3 f1;
            std140mat4x2 f2;
        };

        auto ub = UniformBuffer::create();
        ub->addVerification({
            {&sTest_0_23::f0, "f0"}, //
            {&sTest_0_23::f1, "f1"}, //
            {&sTest_0_23::f2, "f2"}, //
        });

        auto shader = Shader::createFromSource(GL_VERTEX_SHADER,
            "layout(std140) uniform Buffer {\n"
            "  mat3x4 f0;\n"
            "  mat3 f1;\n"
            "  mat4x2 f2;\n"
            "};\n"
            "out float x;\n"
            "void main() {\n"
            "   x = 0\n"
            "     + float(f0[0][0])\n"
            "     + float(f1[0][0])\n"
            "     + float(f2[0][0])\n"
            "   ;\n"
            "}\n"
         );
        ASSERT_TRUE(shader->isCompiledWithoutErrors());

        auto prog = Program::create(shader);
        ASSERT_TRUE(prog->verifyUniformBuffer("Buffer", ub));
    }
    {
        struct sTest_0_24
        {
            std140mat4x4 f0;
            std140ivec4 f1;
            std140bvec2 f2;
        };

        auto ub = UniformBuffer::create();
        ub->addVerification({
            {&sTest_0_24::f0, "f0"}, //
            {&sTest_0_24::f1, "f1"}, //
            {&sTest_0_24::f2, "f2"}, //
        });

        auto shader = Shader::createFromSource(GL_VERTEX_SHADER,
            "layout(std140) uniform Buffer {\n"
            "  mat4x4 f0;\n"
            "  ivec4 f1;\n"
            "  bvec2 f2;\n"
            "};\n"
            "out float x;\n"
            "void main() {\n"
            "   x = 0\n"
            "     + float(f0[0][0])\n"
            "     + float(f1.x)\n"
            "     + float(f2.x)\n"
            "   ;\n"
            "}\n"
         );
        ASSERT_TRUE(shader->isCompiledWithoutErrors());

        auto prog = Program::create(shader);
        ASSERT_TRUE(prog->verifyUniformBuffer("Buffer", ub));
    }
    {
        struct sTest_0_25
        {
            std140bool f0;
            std140dmat2 f1;
            std140mat3x4 f2;
        };

        auto ub = UniformBuffer::create();
        ub->addVerification({
            {&sTest_0_25::f0, "f0"}, //
            {&sTest_0_25::f1, "f1"}, //
            {&sTest_0_25::f2, "f2"}, //
        });

        auto shader = Shader::createFromSource(GL_VERTEX_SHADER,
            "layout(std140) uniform Buffer {\n"
            "  bool f0;\n"
            "  dmat2 f1;\n"
            "  mat3x4 f2;\n"
            "};\n"
            "out float x;\n"
            "void main() {\n"
            "   x = 0\n"
            "     + float(f0)\n"
            "     + float(f1[0][0])\n"
            "     + float(f2[0][0])\n"
            "   ;\n"
            "}\n"
         );
        ASSERT_TRUE(shader->isCompiledWithoutErrors());

        auto prog = Program::create(shader);
        ASSERT_TRUE(prog->verifyUniformBuffer("Buffer", ub));
    }
    {
        struct sTest_0_26
        {
            std140uint f0;
            std140double f1;
            std140bvec4 f2;
        };

        auto ub = UniformBuffer::create();
        ub->addVerification({
            {&sTest_0_26::f0, "f0"}, //
            {&sTest_0_26::f1, "f1"}, //
            {&sTest_0_26::f2, "f2"}, //
        });

        auto shader = Shader::createFromSource(GL_VERTEX_SHADER,
            "layout(std140) uniform Buffer {\n"
            "  uint f0;\n"
            "  double f1;\n"
            "  bvec4 f2;\n"
            "};\n"
            "out float x;\n"
            "void main() {\n"
            "   x = 0\n"
            "     + float(f0)\n"
            "     + float(f1)\n"
            "     + float(f2.x)\n"
            "   ;\n"
            "}\n"
         );
        ASSERT_TRUE(shader->isCompiledWithoutErrors());

        auto prog = Program::create(shader);
        ASSERT_TRUE(prog->verifyUniformBuffer("Buffer", ub));
    }
    {
        struct sTest_0_27
        {
            std140vec4 f0;
            std140ivec2 f1;
            std140dmat2x4 f2;
        };

        auto ub = UniformBuffer::create();
        ub->addVerification({
            {&sTest_0_27::f0, "f0"}, //
            {&sTest_0_27::f1, "f1"}, //
            {&sTest_0_27::f2, "f2"}, //
        });

        auto shader = Shader::createFromSource(GL_VERTEX_SHADER,
            "layout(std140) uniform Buffer {\n"
            "  vec4 f0;\n"
            "  ivec2 f1;\n"
            "  dmat2x4 f2;\n"
            "};\n"
            "out float x;\n"
            "void main() {\n"
            "   x = 0\n"
            "     + float(f0.x)\n"
            "     + float(f1.x)\n"
            "     + float(f2[0][0])\n"
            "   ;\n"
            "}\n"
         );
        ASSERT_TRUE(shader->isCompiledWithoutErrors());

        auto prog = Program::create(shader);
        ASSERT_TRUE(prog->verifyUniformBuffer("Buffer", ub));
    }
    {
        struct sTest_0_28
        {
            std140int f0;
            std140bool f1;
            std140uint f2;
        };

        auto ub = UniformBuffer::create();
        ub->addVerification({
            {&sTest_0_28::f0, "f0"}, //
            {&sTest_0_28::f1, "f1"}, //
            {&sTest_0_28::f2, "f2"}, //
        });

        auto shader = Shader::createFromSource(GL_VERTEX_SHADER,
            "layout(std140) uniform Buffer {\n"
            "  int f0;\n"
            "  bool f1;\n"
            "  uint f2;\n"
            "};\n"
            "out float x;\n"
            "void main() {\n"
            "   x = 0\n"
            "     + float(f0)\n"
            "     + float(f1)\n"
            "     + float(f2)\n"
            "   ;\n"
            "}\n"
         );
        ASSERT_TRUE(shader->isCompiledWithoutErrors());

        auto prog = Program::create(shader);
        ASSERT_TRUE(prog->verifyUniformBuffer("Buffer", ub));
    }
    {
        struct sTest_0_29
        {
            std140dmat2x4 f0;
            std140bvec4 f1;
            std140ivec4 f2;
        };

        auto ub = UniformBuffer::create();
        ub->addVerification({
            {&sTest_0_29::f0, "f0"}, //
            {&sTest_0_29::f1, "f1"}, //
            {&sTest_0_29::f2, "f2"}, //
        });

        auto shader = Shader::createFromSource(GL_VERTEX_SHADER,
            "layout(std140) uniform Buffer {\n"
            "  dmat2x4 f0;\n"
            "  bvec4 f1;\n"
            "  ivec4 f2;\n"
            "};\n"
            "out float x;\n"
            "void main() {\n"
            "   x = 0\n"
            "     + float(f0[0][0])\n"
            "     + float(f1.x)\n"
            "     + float(f2.x)\n"
            "   ;\n"
            "}\n"
         );
        ASSERT_TRUE(shader->isCompiledWithoutErrors());

        auto prog = Program::create(shader);
        ASSERT_TRUE(prog->verifyUniformBuffer("Buffer", ub));
    }
}

