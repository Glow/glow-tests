// This file is auto-generated and should not be modified directly.
#include "tests.hh"

#include <glow/std140.hh>
#include <glow/objects/UniformBuffer.hh>
#include <glow/objects/Shader.hh>
#include <glow/objects/Program.hh>


GLOW_TEST(Std140Gen, Test7)
{
    {
        struct sTest_7_0
        {
            std140vec3 f0;
            std140dmat4 f1;
            std140mat2 f2;
            std140ivec3 f3;
            std140int f4;
            std140vec4 f5;
            std140dmat3 f6;
            std140mat3 f7;
            std140float f8;
            std140dvec2 f9;
        };

        auto ub = UniformBuffer::create();
        ub->addVerification({
            {&sTest_7_0::f0, "f0"}, //
            {&sTest_7_0::f1, "f1"}, //
            {&sTest_7_0::f2, "f2"}, //
            {&sTest_7_0::f3, "f3"}, //
            {&sTest_7_0::f4, "f4"}, //
            {&sTest_7_0::f5, "f5"}, //
            {&sTest_7_0::f6, "f6"}, //
            {&sTest_7_0::f7, "f7"}, //
            {&sTest_7_0::f8, "f8"}, //
            {&sTest_7_0::f9, "f9"}, //
        });

        auto shader = Shader::createFromSource(GL_VERTEX_SHADER,
            "layout(std140) uniform Buffer {\n"
            "  vec3 f0;\n"
            "  dmat4 f1;\n"
            "  mat2 f2;\n"
            "  ivec3 f3;\n"
            "  int f4;\n"
            "  vec4 f5;\n"
            "  dmat3 f6;\n"
            "  mat3 f7;\n"
            "  float f8;\n"
            "  dvec2 f9;\n"
            "};\n"
            "out float x;\n"
            "void main() {\n"
            "   x = 0\n"
            "     + float(f0.x)\n"
            "     + float(f1[0][0])\n"
            "     + float(f2[0][0])\n"
            "     + float(f3.x)\n"
            "     + float(f4)\n"
            "     + float(f5.x)\n"
            "     + float(f6[0][0])\n"
            "     + float(f7[0][0])\n"
            "     + float(f8)\n"
            "     + float(f9.x)\n"
            "   ;\n"
            "}\n"
         );
        ASSERT_TRUE(shader->isCompiledWithoutErrors());

        auto prog = Program::create(shader);
        ASSERT_TRUE(prog->verifyUniformBuffer("Buffer", ub));
    }
    {
        struct sTest_7_1
        {
            std140mat2 f0;
            std140float f1;
            std140bvec2 f2;
            std140dmat4x4 f3;
            std140vec4 f4;
            std140uvec4 f5;
            std140uint f6;
            std140uvec2 f7;
            std140mat2x4 f8;
            std140vec2 f9;
        };

        auto ub = UniformBuffer::create();
        ub->addVerification({
            {&sTest_7_1::f0, "f0"}, //
            {&sTest_7_1::f1, "f1"}, //
            {&sTest_7_1::f2, "f2"}, //
            {&sTest_7_1::f3, "f3"}, //
            {&sTest_7_1::f4, "f4"}, //
            {&sTest_7_1::f5, "f5"}, //
            {&sTest_7_1::f6, "f6"}, //
            {&sTest_7_1::f7, "f7"}, //
            {&sTest_7_1::f8, "f8"}, //
            {&sTest_7_1::f9, "f9"}, //
        });

        auto shader = Shader::createFromSource(GL_VERTEX_SHADER,
            "layout(std140) uniform Buffer {\n"
            "  mat2 f0;\n"
            "  float f1;\n"
            "  bvec2 f2;\n"
            "  dmat4x4 f3;\n"
            "  vec4 f4;\n"
            "  uvec4 f5;\n"
            "  uint f6;\n"
            "  uvec2 f7;\n"
            "  mat2x4 f8;\n"
            "  vec2 f9;\n"
            "};\n"
            "out float x;\n"
            "void main() {\n"
            "   x = 0\n"
            "     + float(f0[0][0])\n"
            "     + float(f1)\n"
            "     + float(f2.x)\n"
            "     + float(f3[0][0])\n"
            "     + float(f4.x)\n"
            "     + float(f5.x)\n"
            "     + float(f6)\n"
            "     + float(f7.x)\n"
            "     + float(f8[0][0])\n"
            "     + float(f9.x)\n"
            "   ;\n"
            "}\n"
         );
        ASSERT_TRUE(shader->isCompiledWithoutErrors());

        auto prog = Program::create(shader);
        ASSERT_TRUE(prog->verifyUniformBuffer("Buffer", ub));
    }
    {
        struct sTest_7_2
        {
            std140uint f0;
            std140bool f1;
            std140float f2;
            std140uvec2 f3;
            std140double f4;
            std140float f5;
            std140bool f6;
            std140uvec4 f7;
            std140dmat3 f8;
            std140uint f9;
        };

        auto ub = UniformBuffer::create();
        ub->addVerification({
            {&sTest_7_2::f0, "f0"}, //
            {&sTest_7_2::f1, "f1"}, //
            {&sTest_7_2::f2, "f2"}, //
            {&sTest_7_2::f3, "f3"}, //
            {&sTest_7_2::f4, "f4"}, //
            {&sTest_7_2::f5, "f5"}, //
            {&sTest_7_2::f6, "f6"}, //
            {&sTest_7_2::f7, "f7"}, //
            {&sTest_7_2::f8, "f8"}, //
            {&sTest_7_2::f9, "f9"}, //
        });

        auto shader = Shader::createFromSource(GL_VERTEX_SHADER,
            "layout(std140) uniform Buffer {\n"
            "  uint f0;\n"
            "  bool f1;\n"
            "  float f2;\n"
            "  uvec2 f3;\n"
            "  double f4;\n"
            "  float f5;\n"
            "  bool f6;\n"
            "  uvec4 f7;\n"
            "  dmat3 f8;\n"
            "  uint f9;\n"
            "};\n"
            "out float x;\n"
            "void main() {\n"
            "   x = 0\n"
            "     + float(f0)\n"
            "     + float(f1)\n"
            "     + float(f2)\n"
            "     + float(f3.x)\n"
            "     + float(f4)\n"
            "     + float(f5)\n"
            "     + float(f6)\n"
            "     + float(f7.x)\n"
            "     + float(f8[0][0])\n"
            "     + float(f9)\n"
            "   ;\n"
            "}\n"
         );
        ASSERT_TRUE(shader->isCompiledWithoutErrors());

        auto prog = Program::create(shader);
        ASSERT_TRUE(prog->verifyUniformBuffer("Buffer", ub));
    }
    {
        struct sTest_7_3
        {
            std140bool f0;
            std140ivec4 f1;
            std140dmat4x3 f2;
            std140double f3;
            std140dmat4x3 f4;
            std140dmat4x2 f5;
            std140dmat2 f6;
            std140vec2 f7;
            std140uvec2 f8;
            std140mat2x2 f9;
        };

        auto ub = UniformBuffer::create();
        ub->addVerification({
            {&sTest_7_3::f0, "f0"}, //
            {&sTest_7_3::f1, "f1"}, //
            {&sTest_7_3::f2, "f2"}, //
            {&sTest_7_3::f3, "f3"}, //
            {&sTest_7_3::f4, "f4"}, //
            {&sTest_7_3::f5, "f5"}, //
            {&sTest_7_3::f6, "f6"}, //
            {&sTest_7_3::f7, "f7"}, //
            {&sTest_7_3::f8, "f8"}, //
            {&sTest_7_3::f9, "f9"}, //
        });

        auto shader = Shader::createFromSource(GL_VERTEX_SHADER,
            "layout(std140) uniform Buffer {\n"
            "  bool f0;\n"
            "  ivec4 f1;\n"
            "  dmat4x3 f2;\n"
            "  double f3;\n"
            "  dmat4x3 f4;\n"
            "  dmat4x2 f5;\n"
            "  dmat2 f6;\n"
            "  vec2 f7;\n"
            "  uvec2 f8;\n"
            "  mat2x2 f9;\n"
            "};\n"
            "out float x;\n"
            "void main() {\n"
            "   x = 0\n"
            "     + float(f0)\n"
            "     + float(f1.x)\n"
            "     + float(f2[0][0])\n"
            "     + float(f3)\n"
            "     + float(f4[0][0])\n"
            "     + float(f5[0][0])\n"
            "     + float(f6[0][0])\n"
            "     + float(f7.x)\n"
            "     + float(f8.x)\n"
            "     + float(f9[0][0])\n"
            "   ;\n"
            "}\n"
         );
        ASSERT_TRUE(shader->isCompiledWithoutErrors());

        auto prog = Program::create(shader);
        ASSERT_TRUE(prog->verifyUniformBuffer("Buffer", ub));
    }
    {
        struct sTest_7_4
        {
            std140dmat2x3 f0;
            std140mat4x3 f1;
            std140uint f2;
            std140dmat4 f3;
            std140double f4;
            std140ivec4 f5;
            std140uint f6;
            std140ivec3 f7;
            std140uint f8;
            std140int f9;
        };

        auto ub = UniformBuffer::create();
        ub->addVerification({
            {&sTest_7_4::f0, "f0"}, //
            {&sTest_7_4::f1, "f1"}, //
            {&sTest_7_4::f2, "f2"}, //
            {&sTest_7_4::f3, "f3"}, //
            {&sTest_7_4::f4, "f4"}, //
            {&sTest_7_4::f5, "f5"}, //
            {&sTest_7_4::f6, "f6"}, //
            {&sTest_7_4::f7, "f7"}, //
            {&sTest_7_4::f8, "f8"}, //
            {&sTest_7_4::f9, "f9"}, //
        });

        auto shader = Shader::createFromSource(GL_VERTEX_SHADER,
            "layout(std140) uniform Buffer {\n"
            "  dmat2x3 f0;\n"
            "  mat4x3 f1;\n"
            "  uint f2;\n"
            "  dmat4 f3;\n"
            "  double f4;\n"
            "  ivec4 f5;\n"
            "  uint f6;\n"
            "  ivec3 f7;\n"
            "  uint f8;\n"
            "  int f9;\n"
            "};\n"
            "out float x;\n"
            "void main() {\n"
            "   x = 0\n"
            "     + float(f0[0][0])\n"
            "     + float(f1[0][0])\n"
            "     + float(f2)\n"
            "     + float(f3[0][0])\n"
            "     + float(f4)\n"
            "     + float(f5.x)\n"
            "     + float(f6)\n"
            "     + float(f7.x)\n"
            "     + float(f8)\n"
            "     + float(f9)\n"
            "   ;\n"
            "}\n"
         );
        ASSERT_TRUE(shader->isCompiledWithoutErrors());

        auto prog = Program::create(shader);
        ASSERT_TRUE(prog->verifyUniformBuffer("Buffer", ub));
    }
    {
        struct sTest_7_5
        {
            std140dmat3 f0;
            std140dmat2x2 f1;
            std140ivec3 f2;
            std140vec4 f3;
            std140dmat3 f4;
            std140mat4x2 f5;
            std140int f6;
            std140bvec4 f7;
            std140bvec3 f8;
            std140bvec2 f9;
        };

        auto ub = UniformBuffer::create();
        ub->addVerification({
            {&sTest_7_5::f0, "f0"}, //
            {&sTest_7_5::f1, "f1"}, //
            {&sTest_7_5::f2, "f2"}, //
            {&sTest_7_5::f3, "f3"}, //
            {&sTest_7_5::f4, "f4"}, //
            {&sTest_7_5::f5, "f5"}, //
            {&sTest_7_5::f6, "f6"}, //
            {&sTest_7_5::f7, "f7"}, //
            {&sTest_7_5::f8, "f8"}, //
            {&sTest_7_5::f9, "f9"}, //
        });

        auto shader = Shader::createFromSource(GL_VERTEX_SHADER,
            "layout(std140) uniform Buffer {\n"
            "  dmat3 f0;\n"
            "  dmat2x2 f1;\n"
            "  ivec3 f2;\n"
            "  vec4 f3;\n"
            "  dmat3 f4;\n"
            "  mat4x2 f5;\n"
            "  int f6;\n"
            "  bvec4 f7;\n"
            "  bvec3 f8;\n"
            "  bvec2 f9;\n"
            "};\n"
            "out float x;\n"
            "void main() {\n"
            "   x = 0\n"
            "     + float(f0[0][0])\n"
            "     + float(f1[0][0])\n"
            "     + float(f2.x)\n"
            "     + float(f3.x)\n"
            "     + float(f4[0][0])\n"
            "     + float(f5[0][0])\n"
            "     + float(f6)\n"
            "     + float(f7.x)\n"
            "     + float(f8.x)\n"
            "     + float(f9.x)\n"
            "   ;\n"
            "}\n"
         );
        ASSERT_TRUE(shader->isCompiledWithoutErrors());

        auto prog = Program::create(shader);
        ASSERT_TRUE(prog->verifyUniformBuffer("Buffer", ub));
    }
    {
        struct sTest_7_6
        {
            std140bool f0;
            std140vec3 f1;
            std140mat3x2 f2;
            std140float f3;
            std140double f4;
            std140uvec4 f5;
            std140mat4x2 f6;
            std140mat2x3 f7;
            std140ivec3 f8;
            std140dmat4x4 f9;
        };

        auto ub = UniformBuffer::create();
        ub->addVerification({
            {&sTest_7_6::f0, "f0"}, //
            {&sTest_7_6::f1, "f1"}, //
            {&sTest_7_6::f2, "f2"}, //
            {&sTest_7_6::f3, "f3"}, //
            {&sTest_7_6::f4, "f4"}, //
            {&sTest_7_6::f5, "f5"}, //
            {&sTest_7_6::f6, "f6"}, //
            {&sTest_7_6::f7, "f7"}, //
            {&sTest_7_6::f8, "f8"}, //
            {&sTest_7_6::f9, "f9"}, //
        });

        auto shader = Shader::createFromSource(GL_VERTEX_SHADER,
            "layout(std140) uniform Buffer {\n"
            "  bool f0;\n"
            "  vec3 f1;\n"
            "  mat3x2 f2;\n"
            "  float f3;\n"
            "  double f4;\n"
            "  uvec4 f5;\n"
            "  mat4x2 f6;\n"
            "  mat2x3 f7;\n"
            "  ivec3 f8;\n"
            "  dmat4x4 f9;\n"
            "};\n"
            "out float x;\n"
            "void main() {\n"
            "   x = 0\n"
            "     + float(f0)\n"
            "     + float(f1.x)\n"
            "     + float(f2[0][0])\n"
            "     + float(f3)\n"
            "     + float(f4)\n"
            "     + float(f5.x)\n"
            "     + float(f6[0][0])\n"
            "     + float(f7[0][0])\n"
            "     + float(f8.x)\n"
            "     + float(f9[0][0])\n"
            "   ;\n"
            "}\n"
         );
        ASSERT_TRUE(shader->isCompiledWithoutErrors());

        auto prog = Program::create(shader);
        ASSERT_TRUE(prog->verifyUniformBuffer("Buffer", ub));
    }
    {
        struct sTest_7_7
        {
            std140mat2x2 f0;
            std140bool f1;
            std140mat3 f2;
            std140bvec4 f3;
            std140uint f4;
            std140ivec3 f5;
            std140dmat2 f6;
            std140mat3x4 f7;
            std140dmat3 f8;
            std140int f9;
        };

        auto ub = UniformBuffer::create();
        ub->addVerification({
            {&sTest_7_7::f0, "f0"}, //
            {&sTest_7_7::f1, "f1"}, //
            {&sTest_7_7::f2, "f2"}, //
            {&sTest_7_7::f3, "f3"}, //
            {&sTest_7_7::f4, "f4"}, //
            {&sTest_7_7::f5, "f5"}, //
            {&sTest_7_7::f6, "f6"}, //
            {&sTest_7_7::f7, "f7"}, //
            {&sTest_7_7::f8, "f8"}, //
            {&sTest_7_7::f9, "f9"}, //
        });

        auto shader = Shader::createFromSource(GL_VERTEX_SHADER,
            "layout(std140) uniform Buffer {\n"
            "  mat2x2 f0;\n"
            "  bool f1;\n"
            "  mat3 f2;\n"
            "  bvec4 f3;\n"
            "  uint f4;\n"
            "  ivec3 f5;\n"
            "  dmat2 f6;\n"
            "  mat3x4 f7;\n"
            "  dmat3 f8;\n"
            "  int f9;\n"
            "};\n"
            "out float x;\n"
            "void main() {\n"
            "   x = 0\n"
            "     + float(f0[0][0])\n"
            "     + float(f1)\n"
            "     + float(f2[0][0])\n"
            "     + float(f3.x)\n"
            "     + float(f4)\n"
            "     + float(f5.x)\n"
            "     + float(f6[0][0])\n"
            "     + float(f7[0][0])\n"
            "     + float(f8[0][0])\n"
            "     + float(f9)\n"
            "   ;\n"
            "}\n"
         );
        ASSERT_TRUE(shader->isCompiledWithoutErrors());

        auto prog = Program::create(shader);
        ASSERT_TRUE(prog->verifyUniformBuffer("Buffer", ub));
    }
    {
        struct sTest_7_8
        {
            std140uint f0;
            std140ivec2 f1;
            std140bvec4 f2;
            std140vec2 f3;
            std140float f4;
            std140vec4 f5;
            std140uint f6;
            std140dmat4 f7;
            std140uint f8;
            std140bvec3 f9;
        };

        auto ub = UniformBuffer::create();
        ub->addVerification({
            {&sTest_7_8::f0, "f0"}, //
            {&sTest_7_8::f1, "f1"}, //
            {&sTest_7_8::f2, "f2"}, //
            {&sTest_7_8::f3, "f3"}, //
            {&sTest_7_8::f4, "f4"}, //
            {&sTest_7_8::f5, "f5"}, //
            {&sTest_7_8::f6, "f6"}, //
            {&sTest_7_8::f7, "f7"}, //
            {&sTest_7_8::f8, "f8"}, //
            {&sTest_7_8::f9, "f9"}, //
        });

        auto shader = Shader::createFromSource(GL_VERTEX_SHADER,
            "layout(std140) uniform Buffer {\n"
            "  uint f0;\n"
            "  ivec2 f1;\n"
            "  bvec4 f2;\n"
            "  vec2 f3;\n"
            "  float f4;\n"
            "  vec4 f5;\n"
            "  uint f6;\n"
            "  dmat4 f7;\n"
            "  uint f8;\n"
            "  bvec3 f9;\n"
            "};\n"
            "out float x;\n"
            "void main() {\n"
            "   x = 0\n"
            "     + float(f0)\n"
            "     + float(f1.x)\n"
            "     + float(f2.x)\n"
            "     + float(f3.x)\n"
            "     + float(f4)\n"
            "     + float(f5.x)\n"
            "     + float(f6)\n"
            "     + float(f7[0][0])\n"
            "     + float(f8)\n"
            "     + float(f9.x)\n"
            "   ;\n"
            "}\n"
         );
        ASSERT_TRUE(shader->isCompiledWithoutErrors());

        auto prog = Program::create(shader);
        ASSERT_TRUE(prog->verifyUniformBuffer("Buffer", ub));
    }
    {
        struct sTest_7_9
        {
            std140uint f0;
            std140vec4 f1;
            std140bool f2;
            std140mat4 f3;
            std140int f4;
            std140mat4x3 f5;
            std140dmat4 f6;
            std140bvec3 f7;
            std140dvec3 f8;
            std140bvec3 f9;
        };

        auto ub = UniformBuffer::create();
        ub->addVerification({
            {&sTest_7_9::f0, "f0"}, //
            {&sTest_7_9::f1, "f1"}, //
            {&sTest_7_9::f2, "f2"}, //
            {&sTest_7_9::f3, "f3"}, //
            {&sTest_7_9::f4, "f4"}, //
            {&sTest_7_9::f5, "f5"}, //
            {&sTest_7_9::f6, "f6"}, //
            {&sTest_7_9::f7, "f7"}, //
            {&sTest_7_9::f8, "f8"}, //
            {&sTest_7_9::f9, "f9"}, //
        });

        auto shader = Shader::createFromSource(GL_VERTEX_SHADER,
            "layout(std140) uniform Buffer {\n"
            "  uint f0;\n"
            "  vec4 f1;\n"
            "  bool f2;\n"
            "  mat4 f3;\n"
            "  int f4;\n"
            "  mat4x3 f5;\n"
            "  dmat4 f6;\n"
            "  bvec3 f7;\n"
            "  dvec3 f8;\n"
            "  bvec3 f9;\n"
            "};\n"
            "out float x;\n"
            "void main() {\n"
            "   x = 0\n"
            "     + float(f0)\n"
            "     + float(f1.x)\n"
            "     + float(f2)\n"
            "     + float(f3[0][0])\n"
            "     + float(f4)\n"
            "     + float(f5[0][0])\n"
            "     + float(f6[0][0])\n"
            "     + float(f7.x)\n"
            "     + float(f8.x)\n"
            "     + float(f9.x)\n"
            "   ;\n"
            "}\n"
         );
        ASSERT_TRUE(shader->isCompiledWithoutErrors());

        auto prog = Program::create(shader);
        ASSERT_TRUE(prog->verifyUniformBuffer("Buffer", ub));
    }
    {
        struct sTest_7_10
        {
            std140dmat3 f0;
            std140mat4 f1;
            std140int f2;
            std140dmat4x3 f3;
            std140mat3x4 f4;
            std140mat2x2 f5;
            std140dmat3x2 f6;
            std140mat4 f7;
            std140dmat3x2 f8;
            std140uvec2 f9;
        };

        auto ub = UniformBuffer::create();
        ub->addVerification({
            {&sTest_7_10::f0, "f0"}, //
            {&sTest_7_10::f1, "f1"}, //
            {&sTest_7_10::f2, "f2"}, //
            {&sTest_7_10::f3, "f3"}, //
            {&sTest_7_10::f4, "f4"}, //
            {&sTest_7_10::f5, "f5"}, //
            {&sTest_7_10::f6, "f6"}, //
            {&sTest_7_10::f7, "f7"}, //
            {&sTest_7_10::f8, "f8"}, //
            {&sTest_7_10::f9, "f9"}, //
        });

        auto shader = Shader::createFromSource(GL_VERTEX_SHADER,
            "layout(std140) uniform Buffer {\n"
            "  dmat3 f0;\n"
            "  mat4 f1;\n"
            "  int f2;\n"
            "  dmat4x3 f3;\n"
            "  mat3x4 f4;\n"
            "  mat2x2 f5;\n"
            "  dmat3x2 f6;\n"
            "  mat4 f7;\n"
            "  dmat3x2 f8;\n"
            "  uvec2 f9;\n"
            "};\n"
            "out float x;\n"
            "void main() {\n"
            "   x = 0\n"
            "     + float(f0[0][0])\n"
            "     + float(f1[0][0])\n"
            "     + float(f2)\n"
            "     + float(f3[0][0])\n"
            "     + float(f4[0][0])\n"
            "     + float(f5[0][0])\n"
            "     + float(f6[0][0])\n"
            "     + float(f7[0][0])\n"
            "     + float(f8[0][0])\n"
            "     + float(f9.x)\n"
            "   ;\n"
            "}\n"
         );
        ASSERT_TRUE(shader->isCompiledWithoutErrors());

        auto prog = Program::create(shader);
        ASSERT_TRUE(prog->verifyUniformBuffer("Buffer", ub));
    }
    {
        struct sTest_7_11
        {
            std140uint f0;
            std140dmat4 f1;
            std140bvec4 f2;
            std140double f3;
            std140vec2 f4;
            std140float f5;
            std140uvec2 f6;
            std140mat3 f7;
            std140uint f8;
            std140dmat3x4 f9;
        };

        auto ub = UniformBuffer::create();
        ub->addVerification({
            {&sTest_7_11::f0, "f0"}, //
            {&sTest_7_11::f1, "f1"}, //
            {&sTest_7_11::f2, "f2"}, //
            {&sTest_7_11::f3, "f3"}, //
            {&sTest_7_11::f4, "f4"}, //
            {&sTest_7_11::f5, "f5"}, //
            {&sTest_7_11::f6, "f6"}, //
            {&sTest_7_11::f7, "f7"}, //
            {&sTest_7_11::f8, "f8"}, //
            {&sTest_7_11::f9, "f9"}, //
        });

        auto shader = Shader::createFromSource(GL_VERTEX_SHADER,
            "layout(std140) uniform Buffer {\n"
            "  uint f0;\n"
            "  dmat4 f1;\n"
            "  bvec4 f2;\n"
            "  double f3;\n"
            "  vec2 f4;\n"
            "  float f5;\n"
            "  uvec2 f6;\n"
            "  mat3 f7;\n"
            "  uint f8;\n"
            "  dmat3x4 f9;\n"
            "};\n"
            "out float x;\n"
            "void main() {\n"
            "   x = 0\n"
            "     + float(f0)\n"
            "     + float(f1[0][0])\n"
            "     + float(f2.x)\n"
            "     + float(f3)\n"
            "     + float(f4.x)\n"
            "     + float(f5)\n"
            "     + float(f6.x)\n"
            "     + float(f7[0][0])\n"
            "     + float(f8)\n"
            "     + float(f9[0][0])\n"
            "   ;\n"
            "}\n"
         );
        ASSERT_TRUE(shader->isCompiledWithoutErrors());

        auto prog = Program::create(shader);
        ASSERT_TRUE(prog->verifyUniformBuffer("Buffer", ub));
    }
    {
        struct sTest_7_12
        {
            std140bool f0;
            std140bvec2 f1;
            std140dvec3 f2;
            std140mat3x2 f3;
            std140bvec4 f4;
            std140mat3 f5;
            std140mat2 f6;
            std140ivec4 f7;
            std140mat3 f8;
            std140uvec2 f9;
        };

        auto ub = UniformBuffer::create();
        ub->addVerification({
            {&sTest_7_12::f0, "f0"}, //
            {&sTest_7_12::f1, "f1"}, //
            {&sTest_7_12::f2, "f2"}, //
            {&sTest_7_12::f3, "f3"}, //
            {&sTest_7_12::f4, "f4"}, //
            {&sTest_7_12::f5, "f5"}, //
            {&sTest_7_12::f6, "f6"}, //
            {&sTest_7_12::f7, "f7"}, //
            {&sTest_7_12::f8, "f8"}, //
            {&sTest_7_12::f9, "f9"}, //
        });

        auto shader = Shader::createFromSource(GL_VERTEX_SHADER,
            "layout(std140) uniform Buffer {\n"
            "  bool f0;\n"
            "  bvec2 f1;\n"
            "  dvec3 f2;\n"
            "  mat3x2 f3;\n"
            "  bvec4 f4;\n"
            "  mat3 f5;\n"
            "  mat2 f6;\n"
            "  ivec4 f7;\n"
            "  mat3 f8;\n"
            "  uvec2 f9;\n"
            "};\n"
            "out float x;\n"
            "void main() {\n"
            "   x = 0\n"
            "     + float(f0)\n"
            "     + float(f1.x)\n"
            "     + float(f2.x)\n"
            "     + float(f3[0][0])\n"
            "     + float(f4.x)\n"
            "     + float(f5[0][0])\n"
            "     + float(f6[0][0])\n"
            "     + float(f7.x)\n"
            "     + float(f8[0][0])\n"
            "     + float(f9.x)\n"
            "   ;\n"
            "}\n"
         );
        ASSERT_TRUE(shader->isCompiledWithoutErrors());

        auto prog = Program::create(shader);
        ASSERT_TRUE(prog->verifyUniformBuffer("Buffer", ub));
    }
    {
        struct sTest_7_13
        {
            std140vec3 f0;
            std140dmat3x4 f1;
            std140bool f2;
            std140bool f3;
            std140uvec3 f4;
            std140int f5;
            std140uvec2 f6;
            std140ivec2 f7;
            std140bvec4 f8;
            std140dmat3 f9;
        };

        auto ub = UniformBuffer::create();
        ub->addVerification({
            {&sTest_7_13::f0, "f0"}, //
            {&sTest_7_13::f1, "f1"}, //
            {&sTest_7_13::f2, "f2"}, //
            {&sTest_7_13::f3, "f3"}, //
            {&sTest_7_13::f4, "f4"}, //
            {&sTest_7_13::f5, "f5"}, //
            {&sTest_7_13::f6, "f6"}, //
            {&sTest_7_13::f7, "f7"}, //
            {&sTest_7_13::f8, "f8"}, //
            {&sTest_7_13::f9, "f9"}, //
        });

        auto shader = Shader::createFromSource(GL_VERTEX_SHADER,
            "layout(std140) uniform Buffer {\n"
            "  vec3 f0;\n"
            "  dmat3x4 f1;\n"
            "  bool f2;\n"
            "  bool f3;\n"
            "  uvec3 f4;\n"
            "  int f5;\n"
            "  uvec2 f6;\n"
            "  ivec2 f7;\n"
            "  bvec4 f8;\n"
            "  dmat3 f9;\n"
            "};\n"
            "out float x;\n"
            "void main() {\n"
            "   x = 0\n"
            "     + float(f0.x)\n"
            "     + float(f1[0][0])\n"
            "     + float(f2)\n"
            "     + float(f3)\n"
            "     + float(f4.x)\n"
            "     + float(f5)\n"
            "     + float(f6.x)\n"
            "     + float(f7.x)\n"
            "     + float(f8.x)\n"
            "     + float(f9[0][0])\n"
            "   ;\n"
            "}\n"
         );
        ASSERT_TRUE(shader->isCompiledWithoutErrors());

        auto prog = Program::create(shader);
        ASSERT_TRUE(prog->verifyUniformBuffer("Buffer", ub));
    }
    {
        struct sTest_7_14
        {
            std140dmat4x4 f0;
            std140uint f1;
            std140ivec4 f2;
            std140double f3;
            std140float f4;
            std140dvec2 f5;
            std140int f6;
            std140double f7;
            std140mat4 f8;
            std140bvec3 f9;
        };

        auto ub = UniformBuffer::create();
        ub->addVerification({
            {&sTest_7_14::f0, "f0"}, //
            {&sTest_7_14::f1, "f1"}, //
            {&sTest_7_14::f2, "f2"}, //
            {&sTest_7_14::f3, "f3"}, //
            {&sTest_7_14::f4, "f4"}, //
            {&sTest_7_14::f5, "f5"}, //
            {&sTest_7_14::f6, "f6"}, //
            {&sTest_7_14::f7, "f7"}, //
            {&sTest_7_14::f8, "f8"}, //
            {&sTest_7_14::f9, "f9"}, //
        });

        auto shader = Shader::createFromSource(GL_VERTEX_SHADER,
            "layout(std140) uniform Buffer {\n"
            "  dmat4x4 f0;\n"
            "  uint f1;\n"
            "  ivec4 f2;\n"
            "  double f3;\n"
            "  float f4;\n"
            "  dvec2 f5;\n"
            "  int f6;\n"
            "  double f7;\n"
            "  mat4 f8;\n"
            "  bvec3 f9;\n"
            "};\n"
            "out float x;\n"
            "void main() {\n"
            "   x = 0\n"
            "     + float(f0[0][0])\n"
            "     + float(f1)\n"
            "     + float(f2.x)\n"
            "     + float(f3)\n"
            "     + float(f4)\n"
            "     + float(f5.x)\n"
            "     + float(f6)\n"
            "     + float(f7)\n"
            "     + float(f8[0][0])\n"
            "     + float(f9.x)\n"
            "   ;\n"
            "}\n"
         );
        ASSERT_TRUE(shader->isCompiledWithoutErrors());

        auto prog = Program::create(shader);
        ASSERT_TRUE(prog->verifyUniformBuffer("Buffer", ub));
    }
    {
        struct sTest_7_15
        {
            std140vec4 f0;
            std140int f1;
            std140ivec4 f2;
            std140mat3 f3;
            std140mat3 f4;
            std140bvec3 f5;
            std140dmat3 f6;
            std140bool f7;
            std140uvec2 f8;
            std140dmat4 f9;
        };

        auto ub = UniformBuffer::create();
        ub->addVerification({
            {&sTest_7_15::f0, "f0"}, //
            {&sTest_7_15::f1, "f1"}, //
            {&sTest_7_15::f2, "f2"}, //
            {&sTest_7_15::f3, "f3"}, //
            {&sTest_7_15::f4, "f4"}, //
            {&sTest_7_15::f5, "f5"}, //
            {&sTest_7_15::f6, "f6"}, //
            {&sTest_7_15::f7, "f7"}, //
            {&sTest_7_15::f8, "f8"}, //
            {&sTest_7_15::f9, "f9"}, //
        });

        auto shader = Shader::createFromSource(GL_VERTEX_SHADER,
            "layout(std140) uniform Buffer {\n"
            "  vec4 f0;\n"
            "  int f1;\n"
            "  ivec4 f2;\n"
            "  mat3 f3;\n"
            "  mat3 f4;\n"
            "  bvec3 f5;\n"
            "  dmat3 f6;\n"
            "  bool f7;\n"
            "  uvec2 f8;\n"
            "  dmat4 f9;\n"
            "};\n"
            "out float x;\n"
            "void main() {\n"
            "   x = 0\n"
            "     + float(f0.x)\n"
            "     + float(f1)\n"
            "     + float(f2.x)\n"
            "     + float(f3[0][0])\n"
            "     + float(f4[0][0])\n"
            "     + float(f5.x)\n"
            "     + float(f6[0][0])\n"
            "     + float(f7)\n"
            "     + float(f8.x)\n"
            "     + float(f9[0][0])\n"
            "   ;\n"
            "}\n"
         );
        ASSERT_TRUE(shader->isCompiledWithoutErrors());

        auto prog = Program::create(shader);
        ASSERT_TRUE(prog->verifyUniformBuffer("Buffer", ub));
    }
    {
        struct sTest_7_16
        {
            std140uint f0;
            std140bool f1;
            std140bvec4 f2;
            std140ivec3 f3;
            std140bool f4;
            std140bool f5;
            std140uint f6;
            std140uvec2 f7;
            std140int f8;
            std140dmat4x4 f9;
        };

        auto ub = UniformBuffer::create();
        ub->addVerification({
            {&sTest_7_16::f0, "f0"}, //
            {&sTest_7_16::f1, "f1"}, //
            {&sTest_7_16::f2, "f2"}, //
            {&sTest_7_16::f3, "f3"}, //
            {&sTest_7_16::f4, "f4"}, //
            {&sTest_7_16::f5, "f5"}, //
            {&sTest_7_16::f6, "f6"}, //
            {&sTest_7_16::f7, "f7"}, //
            {&sTest_7_16::f8, "f8"}, //
            {&sTest_7_16::f9, "f9"}, //
        });

        auto shader = Shader::createFromSource(GL_VERTEX_SHADER,
            "layout(std140) uniform Buffer {\n"
            "  uint f0;\n"
            "  bool f1;\n"
            "  bvec4 f2;\n"
            "  ivec3 f3;\n"
            "  bool f4;\n"
            "  bool f5;\n"
            "  uint f6;\n"
            "  uvec2 f7;\n"
            "  int f8;\n"
            "  dmat4x4 f9;\n"
            "};\n"
            "out float x;\n"
            "void main() {\n"
            "   x = 0\n"
            "     + float(f0)\n"
            "     + float(f1)\n"
            "     + float(f2.x)\n"
            "     + float(f3.x)\n"
            "     + float(f4)\n"
            "     + float(f5)\n"
            "     + float(f6)\n"
            "     + float(f7.x)\n"
            "     + float(f8)\n"
            "     + float(f9[0][0])\n"
            "   ;\n"
            "}\n"
         );
        ASSERT_TRUE(shader->isCompiledWithoutErrors());

        auto prog = Program::create(shader);
        ASSERT_TRUE(prog->verifyUniformBuffer("Buffer", ub));
    }
    {
        struct sTest_7_17
        {
            std140dmat3x4 f0;
            std140vec2 f1;
            std140uvec2 f2;
            std140double f3;
            std140int f4;
            std140int f5;
            std140bool f6;
            std140dvec2 f7;
            std140dmat3 f8;
            std140float f9;
        };

        auto ub = UniformBuffer::create();
        ub->addVerification({
            {&sTest_7_17::f0, "f0"}, //
            {&sTest_7_17::f1, "f1"}, //
            {&sTest_7_17::f2, "f2"}, //
            {&sTest_7_17::f3, "f3"}, //
            {&sTest_7_17::f4, "f4"}, //
            {&sTest_7_17::f5, "f5"}, //
            {&sTest_7_17::f6, "f6"}, //
            {&sTest_7_17::f7, "f7"}, //
            {&sTest_7_17::f8, "f8"}, //
            {&sTest_7_17::f9, "f9"}, //
        });

        auto shader = Shader::createFromSource(GL_VERTEX_SHADER,
            "layout(std140) uniform Buffer {\n"
            "  dmat3x4 f0;\n"
            "  vec2 f1;\n"
            "  uvec2 f2;\n"
            "  double f3;\n"
            "  int f4;\n"
            "  int f5;\n"
            "  bool f6;\n"
            "  dvec2 f7;\n"
            "  dmat3 f8;\n"
            "  float f9;\n"
            "};\n"
            "out float x;\n"
            "void main() {\n"
            "   x = 0\n"
            "     + float(f0[0][0])\n"
            "     + float(f1.x)\n"
            "     + float(f2.x)\n"
            "     + float(f3)\n"
            "     + float(f4)\n"
            "     + float(f5)\n"
            "     + float(f6)\n"
            "     + float(f7.x)\n"
            "     + float(f8[0][0])\n"
            "     + float(f9)\n"
            "   ;\n"
            "}\n"
         );
        ASSERT_TRUE(shader->isCompiledWithoutErrors());

        auto prog = Program::create(shader);
        ASSERT_TRUE(prog->verifyUniformBuffer("Buffer", ub));
    }
    {
        struct sTest_7_18
        {
            std140dmat2 f0;
            std140dmat3x3 f1;
            std140dmat2 f2;
            std140mat3x2 f3;
            std140uvec4 f4;
            std140dmat3x3 f5;
            std140dvec2 f6;
            std140int f7;
            std140mat3x3 f8;
            std140dvec3 f9;
        };

        auto ub = UniformBuffer::create();
        ub->addVerification({
            {&sTest_7_18::f0, "f0"}, //
            {&sTest_7_18::f1, "f1"}, //
            {&sTest_7_18::f2, "f2"}, //
            {&sTest_7_18::f3, "f3"}, //
            {&sTest_7_18::f4, "f4"}, //
            {&sTest_7_18::f5, "f5"}, //
            {&sTest_7_18::f6, "f6"}, //
            {&sTest_7_18::f7, "f7"}, //
            {&sTest_7_18::f8, "f8"}, //
            {&sTest_7_18::f9, "f9"}, //
        });

        auto shader = Shader::createFromSource(GL_VERTEX_SHADER,
            "layout(std140) uniform Buffer {\n"
            "  dmat2 f0;\n"
            "  dmat3x3 f1;\n"
            "  dmat2 f2;\n"
            "  mat3x2 f3;\n"
            "  uvec4 f4;\n"
            "  dmat3x3 f5;\n"
            "  dvec2 f6;\n"
            "  int f7;\n"
            "  mat3x3 f8;\n"
            "  dvec3 f9;\n"
            "};\n"
            "out float x;\n"
            "void main() {\n"
            "   x = 0\n"
            "     + float(f0[0][0])\n"
            "     + float(f1[0][0])\n"
            "     + float(f2[0][0])\n"
            "     + float(f3[0][0])\n"
            "     + float(f4.x)\n"
            "     + float(f5[0][0])\n"
            "     + float(f6.x)\n"
            "     + float(f7)\n"
            "     + float(f8[0][0])\n"
            "     + float(f9.x)\n"
            "   ;\n"
            "}\n"
         );
        ASSERT_TRUE(shader->isCompiledWithoutErrors());

        auto prog = Program::create(shader);
        ASSERT_TRUE(prog->verifyUniformBuffer("Buffer", ub));
    }
    {
        struct sTest_7_19
        {
            std140uvec2 f0;
            std140dmat2 f1;
            std140dmat2 f2;
            std140bool f3;
            std140mat4 f4;
            std140dvec2 f5;
            std140uint f6;
            std140vec2 f7;
            std140double f8;
            std140bool f9;
        };

        auto ub = UniformBuffer::create();
        ub->addVerification({
            {&sTest_7_19::f0, "f0"}, //
            {&sTest_7_19::f1, "f1"}, //
            {&sTest_7_19::f2, "f2"}, //
            {&sTest_7_19::f3, "f3"}, //
            {&sTest_7_19::f4, "f4"}, //
            {&sTest_7_19::f5, "f5"}, //
            {&sTest_7_19::f6, "f6"}, //
            {&sTest_7_19::f7, "f7"}, //
            {&sTest_7_19::f8, "f8"}, //
            {&sTest_7_19::f9, "f9"}, //
        });

        auto shader = Shader::createFromSource(GL_VERTEX_SHADER,
            "layout(std140) uniform Buffer {\n"
            "  uvec2 f0;\n"
            "  dmat2 f1;\n"
            "  dmat2 f2;\n"
            "  bool f3;\n"
            "  mat4 f4;\n"
            "  dvec2 f5;\n"
            "  uint f6;\n"
            "  vec2 f7;\n"
            "  double f8;\n"
            "  bool f9;\n"
            "};\n"
            "out float x;\n"
            "void main() {\n"
            "   x = 0\n"
            "     + float(f0.x)\n"
            "     + float(f1[0][0])\n"
            "     + float(f2[0][0])\n"
            "     + float(f3)\n"
            "     + float(f4[0][0])\n"
            "     + float(f5.x)\n"
            "     + float(f6)\n"
            "     + float(f7.x)\n"
            "     + float(f8)\n"
            "     + float(f9)\n"
            "   ;\n"
            "}\n"
         );
        ASSERT_TRUE(shader->isCompiledWithoutErrors());

        auto prog = Program::create(shader);
        ASSERT_TRUE(prog->verifyUniformBuffer("Buffer", ub));
    }
    {
        struct sTest_7_20
        {
            std140double f0;
            std140int f1;
            std140float f2;
            std140mat4x4 f3;
            std140dmat4 f4;
            std140mat2 f5;
            std140mat4x4 f6;
            std140mat3 f7;
            std140dvec2 f8;
            std140mat4 f9;
        };

        auto ub = UniformBuffer::create();
        ub->addVerification({
            {&sTest_7_20::f0, "f0"}, //
            {&sTest_7_20::f1, "f1"}, //
            {&sTest_7_20::f2, "f2"}, //
            {&sTest_7_20::f3, "f3"}, //
            {&sTest_7_20::f4, "f4"}, //
            {&sTest_7_20::f5, "f5"}, //
            {&sTest_7_20::f6, "f6"}, //
            {&sTest_7_20::f7, "f7"}, //
            {&sTest_7_20::f8, "f8"}, //
            {&sTest_7_20::f9, "f9"}, //
        });

        auto shader = Shader::createFromSource(GL_VERTEX_SHADER,
            "layout(std140) uniform Buffer {\n"
            "  double f0;\n"
            "  int f1;\n"
            "  float f2;\n"
            "  mat4x4 f3;\n"
            "  dmat4 f4;\n"
            "  mat2 f5;\n"
            "  mat4x4 f6;\n"
            "  mat3 f7;\n"
            "  dvec2 f8;\n"
            "  mat4 f9;\n"
            "};\n"
            "out float x;\n"
            "void main() {\n"
            "   x = 0\n"
            "     + float(f0)\n"
            "     + float(f1)\n"
            "     + float(f2)\n"
            "     + float(f3[0][0])\n"
            "     + float(f4[0][0])\n"
            "     + float(f5[0][0])\n"
            "     + float(f6[0][0])\n"
            "     + float(f7[0][0])\n"
            "     + float(f8.x)\n"
            "     + float(f9[0][0])\n"
            "   ;\n"
            "}\n"
         );
        ASSERT_TRUE(shader->isCompiledWithoutErrors());

        auto prog = Program::create(shader);
        ASSERT_TRUE(prog->verifyUniformBuffer("Buffer", ub));
    }
    {
        struct sTest_7_21
        {
            std140dmat2x2 f0;
            std140bool f1;
            std140dmat2x2 f2;
            std140dmat4 f3;
            std140bool f4;
            std140mat2x4 f5;
            std140mat3x3 f6;
            std140bool f7;
            std140ivec4 f8;
            std140dmat2x4 f9;
        };

        auto ub = UniformBuffer::create();
        ub->addVerification({
            {&sTest_7_21::f0, "f0"}, //
            {&sTest_7_21::f1, "f1"}, //
            {&sTest_7_21::f2, "f2"}, //
            {&sTest_7_21::f3, "f3"}, //
            {&sTest_7_21::f4, "f4"}, //
            {&sTest_7_21::f5, "f5"}, //
            {&sTest_7_21::f6, "f6"}, //
            {&sTest_7_21::f7, "f7"}, //
            {&sTest_7_21::f8, "f8"}, //
            {&sTest_7_21::f9, "f9"}, //
        });

        auto shader = Shader::createFromSource(GL_VERTEX_SHADER,
            "layout(std140) uniform Buffer {\n"
            "  dmat2x2 f0;\n"
            "  bool f1;\n"
            "  dmat2x2 f2;\n"
            "  dmat4 f3;\n"
            "  bool f4;\n"
            "  mat2x4 f5;\n"
            "  mat3x3 f6;\n"
            "  bool f7;\n"
            "  ivec4 f8;\n"
            "  dmat2x4 f9;\n"
            "};\n"
            "out float x;\n"
            "void main() {\n"
            "   x = 0\n"
            "     + float(f0[0][0])\n"
            "     + float(f1)\n"
            "     + float(f2[0][0])\n"
            "     + float(f3[0][0])\n"
            "     + float(f4)\n"
            "     + float(f5[0][0])\n"
            "     + float(f6[0][0])\n"
            "     + float(f7)\n"
            "     + float(f8.x)\n"
            "     + float(f9[0][0])\n"
            "   ;\n"
            "}\n"
         );
        ASSERT_TRUE(shader->isCompiledWithoutErrors());

        auto prog = Program::create(shader);
        ASSERT_TRUE(prog->verifyUniformBuffer("Buffer", ub));
    }
    {
        struct sTest_7_22
        {
            std140mat2x4 f0;
            std140uvec4 f1;
            std140ivec4 f2;
            std140dmat3 f3;
            std140uvec2 f4;
            std140ivec4 f5;
            std140double f6;
            std140float f7;
            std140mat3 f8;
            std140bool f9;
        };

        auto ub = UniformBuffer::create();
        ub->addVerification({
            {&sTest_7_22::f0, "f0"}, //
            {&sTest_7_22::f1, "f1"}, //
            {&sTest_7_22::f2, "f2"}, //
            {&sTest_7_22::f3, "f3"}, //
            {&sTest_7_22::f4, "f4"}, //
            {&sTest_7_22::f5, "f5"}, //
            {&sTest_7_22::f6, "f6"}, //
            {&sTest_7_22::f7, "f7"}, //
            {&sTest_7_22::f8, "f8"}, //
            {&sTest_7_22::f9, "f9"}, //
        });

        auto shader = Shader::createFromSource(GL_VERTEX_SHADER,
            "layout(std140) uniform Buffer {\n"
            "  mat2x4 f0;\n"
            "  uvec4 f1;\n"
            "  ivec4 f2;\n"
            "  dmat3 f3;\n"
            "  uvec2 f4;\n"
            "  ivec4 f5;\n"
            "  double f6;\n"
            "  float f7;\n"
            "  mat3 f8;\n"
            "  bool f9;\n"
            "};\n"
            "out float x;\n"
            "void main() {\n"
            "   x = 0\n"
            "     + float(f0[0][0])\n"
            "     + float(f1.x)\n"
            "     + float(f2.x)\n"
            "     + float(f3[0][0])\n"
            "     + float(f4.x)\n"
            "     + float(f5.x)\n"
            "     + float(f6)\n"
            "     + float(f7)\n"
            "     + float(f8[0][0])\n"
            "     + float(f9)\n"
            "   ;\n"
            "}\n"
         );
        ASSERT_TRUE(shader->isCompiledWithoutErrors());

        auto prog = Program::create(shader);
        ASSERT_TRUE(prog->verifyUniformBuffer("Buffer", ub));
    }
    {
        struct sTest_7_23
        {
            std140bool f0;
            std140double f1;
            std140dmat2 f2;
            std140ivec2 f3;
            std140double f4;
            std140ivec3 f5;
            std140dmat4x2 f6;
            std140uvec2 f7;
            std140mat3x2 f8;
            std140float f9;
        };

        auto ub = UniformBuffer::create();
        ub->addVerification({
            {&sTest_7_23::f0, "f0"}, //
            {&sTest_7_23::f1, "f1"}, //
            {&sTest_7_23::f2, "f2"}, //
            {&sTest_7_23::f3, "f3"}, //
            {&sTest_7_23::f4, "f4"}, //
            {&sTest_7_23::f5, "f5"}, //
            {&sTest_7_23::f6, "f6"}, //
            {&sTest_7_23::f7, "f7"}, //
            {&sTest_7_23::f8, "f8"}, //
            {&sTest_7_23::f9, "f9"}, //
        });

        auto shader = Shader::createFromSource(GL_VERTEX_SHADER,
            "layout(std140) uniform Buffer {\n"
            "  bool f0;\n"
            "  double f1;\n"
            "  dmat2 f2;\n"
            "  ivec2 f3;\n"
            "  double f4;\n"
            "  ivec3 f5;\n"
            "  dmat4x2 f6;\n"
            "  uvec2 f7;\n"
            "  mat3x2 f8;\n"
            "  float f9;\n"
            "};\n"
            "out float x;\n"
            "void main() {\n"
            "   x = 0\n"
            "     + float(f0)\n"
            "     + float(f1)\n"
            "     + float(f2[0][0])\n"
            "     + float(f3.x)\n"
            "     + float(f4)\n"
            "     + float(f5.x)\n"
            "     + float(f6[0][0])\n"
            "     + float(f7.x)\n"
            "     + float(f8[0][0])\n"
            "     + float(f9)\n"
            "   ;\n"
            "}\n"
         );
        ASSERT_TRUE(shader->isCompiledWithoutErrors());

        auto prog = Program::create(shader);
        ASSERT_TRUE(prog->verifyUniformBuffer("Buffer", ub));
    }
    {
        struct sTest_7_24
        {
            std140int f0;
            std140uint f1;
            std140bool f2;
            std140int f3;
            std140dvec4 f4;
            std140uint f5;
            std140mat2x3 f6;
            std140bvec4 f7;
            std140double f8;
            std140dvec3 f9;
        };

        auto ub = UniformBuffer::create();
        ub->addVerification({
            {&sTest_7_24::f0, "f0"}, //
            {&sTest_7_24::f1, "f1"}, //
            {&sTest_7_24::f2, "f2"}, //
            {&sTest_7_24::f3, "f3"}, //
            {&sTest_7_24::f4, "f4"}, //
            {&sTest_7_24::f5, "f5"}, //
            {&sTest_7_24::f6, "f6"}, //
            {&sTest_7_24::f7, "f7"}, //
            {&sTest_7_24::f8, "f8"}, //
            {&sTest_7_24::f9, "f9"}, //
        });

        auto shader = Shader::createFromSource(GL_VERTEX_SHADER,
            "layout(std140) uniform Buffer {\n"
            "  int f0;\n"
            "  uint f1;\n"
            "  bool f2;\n"
            "  int f3;\n"
            "  dvec4 f4;\n"
            "  uint f5;\n"
            "  mat2x3 f6;\n"
            "  bvec4 f7;\n"
            "  double f8;\n"
            "  dvec3 f9;\n"
            "};\n"
            "out float x;\n"
            "void main() {\n"
            "   x = 0\n"
            "     + float(f0)\n"
            "     + float(f1)\n"
            "     + float(f2)\n"
            "     + float(f3)\n"
            "     + float(f4.x)\n"
            "     + float(f5)\n"
            "     + float(f6[0][0])\n"
            "     + float(f7.x)\n"
            "     + float(f8)\n"
            "     + float(f9.x)\n"
            "   ;\n"
            "}\n"
         );
        ASSERT_TRUE(shader->isCompiledWithoutErrors());

        auto prog = Program::create(shader);
        ASSERT_TRUE(prog->verifyUniformBuffer("Buffer", ub));
    }
    {
        struct sTest_7_25
        {
            std140double f0;
            std140uint f1;
            std140float f2;
            std140double f3;
            std140mat3 f4;
            std140int f5;
            std140mat3 f6;
            std140ivec2 f7;
            std140mat2 f8;
            std140dvec3 f9;
        };

        auto ub = UniformBuffer::create();
        ub->addVerification({
            {&sTest_7_25::f0, "f0"}, //
            {&sTest_7_25::f1, "f1"}, //
            {&sTest_7_25::f2, "f2"}, //
            {&sTest_7_25::f3, "f3"}, //
            {&sTest_7_25::f4, "f4"}, //
            {&sTest_7_25::f5, "f5"}, //
            {&sTest_7_25::f6, "f6"}, //
            {&sTest_7_25::f7, "f7"}, //
            {&sTest_7_25::f8, "f8"}, //
            {&sTest_7_25::f9, "f9"}, //
        });

        auto shader = Shader::createFromSource(GL_VERTEX_SHADER,
            "layout(std140) uniform Buffer {\n"
            "  double f0;\n"
            "  uint f1;\n"
            "  float f2;\n"
            "  double f3;\n"
            "  mat3 f4;\n"
            "  int f5;\n"
            "  mat3 f6;\n"
            "  ivec2 f7;\n"
            "  mat2 f8;\n"
            "  dvec3 f9;\n"
            "};\n"
            "out float x;\n"
            "void main() {\n"
            "   x = 0\n"
            "     + float(f0)\n"
            "     + float(f1)\n"
            "     + float(f2)\n"
            "     + float(f3)\n"
            "     + float(f4[0][0])\n"
            "     + float(f5)\n"
            "     + float(f6[0][0])\n"
            "     + float(f7.x)\n"
            "     + float(f8[0][0])\n"
            "     + float(f9.x)\n"
            "   ;\n"
            "}\n"
         );
        ASSERT_TRUE(shader->isCompiledWithoutErrors());

        auto prog = Program::create(shader);
        ASSERT_TRUE(prog->verifyUniformBuffer("Buffer", ub));
    }
    {
        struct sTest_7_26
        {
            std140ivec2 f0;
            std140bvec3 f1;
            std140uint f2;
            std140bvec2 f3;
            std140float f4;
            std140mat3x4 f5;
            std140dvec3 f6;
            std140uint f7;
            std140bvec3 f8;
            std140vec4 f9;
        };

        auto ub = UniformBuffer::create();
        ub->addVerification({
            {&sTest_7_26::f0, "f0"}, //
            {&sTest_7_26::f1, "f1"}, //
            {&sTest_7_26::f2, "f2"}, //
            {&sTest_7_26::f3, "f3"}, //
            {&sTest_7_26::f4, "f4"}, //
            {&sTest_7_26::f5, "f5"}, //
            {&sTest_7_26::f6, "f6"}, //
            {&sTest_7_26::f7, "f7"}, //
            {&sTest_7_26::f8, "f8"}, //
            {&sTest_7_26::f9, "f9"}, //
        });

        auto shader = Shader::createFromSource(GL_VERTEX_SHADER,
            "layout(std140) uniform Buffer {\n"
            "  ivec2 f0;\n"
            "  bvec3 f1;\n"
            "  uint f2;\n"
            "  bvec2 f3;\n"
            "  float f4;\n"
            "  mat3x4 f5;\n"
            "  dvec3 f6;\n"
            "  uint f7;\n"
            "  bvec3 f8;\n"
            "  vec4 f9;\n"
            "};\n"
            "out float x;\n"
            "void main() {\n"
            "   x = 0\n"
            "     + float(f0.x)\n"
            "     + float(f1.x)\n"
            "     + float(f2)\n"
            "     + float(f3.x)\n"
            "     + float(f4)\n"
            "     + float(f5[0][0])\n"
            "     + float(f6.x)\n"
            "     + float(f7)\n"
            "     + float(f8.x)\n"
            "     + float(f9.x)\n"
            "   ;\n"
            "}\n"
         );
        ASSERT_TRUE(shader->isCompiledWithoutErrors());

        auto prog = Program::create(shader);
        ASSERT_TRUE(prog->verifyUniformBuffer("Buffer", ub));
    }
    {
        struct sTest_7_27
        {
            std140dmat2 f0;
            std140int f1;
            std140vec4 f2;
            std140dvec2 f3;
            std140mat3 f4;
            std140vec3 f5;
            std140uvec4 f6;
            std140bool f7;
            std140dmat3 f8;
            std140double f9;
        };

        auto ub = UniformBuffer::create();
        ub->addVerification({
            {&sTest_7_27::f0, "f0"}, //
            {&sTest_7_27::f1, "f1"}, //
            {&sTest_7_27::f2, "f2"}, //
            {&sTest_7_27::f3, "f3"}, //
            {&sTest_7_27::f4, "f4"}, //
            {&sTest_7_27::f5, "f5"}, //
            {&sTest_7_27::f6, "f6"}, //
            {&sTest_7_27::f7, "f7"}, //
            {&sTest_7_27::f8, "f8"}, //
            {&sTest_7_27::f9, "f9"}, //
        });

        auto shader = Shader::createFromSource(GL_VERTEX_SHADER,
            "layout(std140) uniform Buffer {\n"
            "  dmat2 f0;\n"
            "  int f1;\n"
            "  vec4 f2;\n"
            "  dvec2 f3;\n"
            "  mat3 f4;\n"
            "  vec3 f5;\n"
            "  uvec4 f6;\n"
            "  bool f7;\n"
            "  dmat3 f8;\n"
            "  double f9;\n"
            "};\n"
            "out float x;\n"
            "void main() {\n"
            "   x = 0\n"
            "     + float(f0[0][0])\n"
            "     + float(f1)\n"
            "     + float(f2.x)\n"
            "     + float(f3.x)\n"
            "     + float(f4[0][0])\n"
            "     + float(f5.x)\n"
            "     + float(f6.x)\n"
            "     + float(f7)\n"
            "     + float(f8[0][0])\n"
            "     + float(f9)\n"
            "   ;\n"
            "}\n"
         );
        ASSERT_TRUE(shader->isCompiledWithoutErrors());

        auto prog = Program::create(shader);
        ASSERT_TRUE(prog->verifyUniformBuffer("Buffer", ub));
    }
    {
        struct sTest_7_28
        {
            std140bool f0;
            std140vec2 f1;
            std140vec2 f2;
            std140uvec2 f3;
            std140bvec4 f4;
            std140dvec2 f5;
            std140mat2x4 f6;
            std140bvec2 f7;
            std140bvec2 f8;
            std140dmat3x2 f9;
        };

        auto ub = UniformBuffer::create();
        ub->addVerification({
            {&sTest_7_28::f0, "f0"}, //
            {&sTest_7_28::f1, "f1"}, //
            {&sTest_7_28::f2, "f2"}, //
            {&sTest_7_28::f3, "f3"}, //
            {&sTest_7_28::f4, "f4"}, //
            {&sTest_7_28::f5, "f5"}, //
            {&sTest_7_28::f6, "f6"}, //
            {&sTest_7_28::f7, "f7"}, //
            {&sTest_7_28::f8, "f8"}, //
            {&sTest_7_28::f9, "f9"}, //
        });

        auto shader = Shader::createFromSource(GL_VERTEX_SHADER,
            "layout(std140) uniform Buffer {\n"
            "  bool f0;\n"
            "  vec2 f1;\n"
            "  vec2 f2;\n"
            "  uvec2 f3;\n"
            "  bvec4 f4;\n"
            "  dvec2 f5;\n"
            "  mat2x4 f6;\n"
            "  bvec2 f7;\n"
            "  bvec2 f8;\n"
            "  dmat3x2 f9;\n"
            "};\n"
            "out float x;\n"
            "void main() {\n"
            "   x = 0\n"
            "     + float(f0)\n"
            "     + float(f1.x)\n"
            "     + float(f2.x)\n"
            "     + float(f3.x)\n"
            "     + float(f4.x)\n"
            "     + float(f5.x)\n"
            "     + float(f6[0][0])\n"
            "     + float(f7.x)\n"
            "     + float(f8.x)\n"
            "     + float(f9[0][0])\n"
            "   ;\n"
            "}\n"
         );
        ASSERT_TRUE(shader->isCompiledWithoutErrors());

        auto prog = Program::create(shader);
        ASSERT_TRUE(prog->verifyUniformBuffer("Buffer", ub));
    }
    {
        struct sTest_7_29
        {
            std140vec4 f0;
            std140dvec3 f1;
            std140bvec4 f2;
            std140uvec3 f3;
            std140dvec4 f4;
            std140dvec3 f5;
            std140dmat3x3 f6;
            std140mat3 f7;
            std140bool f8;
            std140uint f9;
        };

        auto ub = UniformBuffer::create();
        ub->addVerification({
            {&sTest_7_29::f0, "f0"}, //
            {&sTest_7_29::f1, "f1"}, //
            {&sTest_7_29::f2, "f2"}, //
            {&sTest_7_29::f3, "f3"}, //
            {&sTest_7_29::f4, "f4"}, //
            {&sTest_7_29::f5, "f5"}, //
            {&sTest_7_29::f6, "f6"}, //
            {&sTest_7_29::f7, "f7"}, //
            {&sTest_7_29::f8, "f8"}, //
            {&sTest_7_29::f9, "f9"}, //
        });

        auto shader = Shader::createFromSource(GL_VERTEX_SHADER,
            "layout(std140) uniform Buffer {\n"
            "  vec4 f0;\n"
            "  dvec3 f1;\n"
            "  bvec4 f2;\n"
            "  uvec3 f3;\n"
            "  dvec4 f4;\n"
            "  dvec3 f5;\n"
            "  dmat3x3 f6;\n"
            "  mat3 f7;\n"
            "  bool f8;\n"
            "  uint f9;\n"
            "};\n"
            "out float x;\n"
            "void main() {\n"
            "   x = 0\n"
            "     + float(f0.x)\n"
            "     + float(f1.x)\n"
            "     + float(f2.x)\n"
            "     + float(f3.x)\n"
            "     + float(f4.x)\n"
            "     + float(f5.x)\n"
            "     + float(f6[0][0])\n"
            "     + float(f7[0][0])\n"
            "     + float(f8)\n"
            "     + float(f9)\n"
            "   ;\n"
            "}\n"
         );
        ASSERT_TRUE(shader->isCompiledWithoutErrors());

        auto prog = Program::create(shader);
        ASSERT_TRUE(prog->verifyUniformBuffer("Buffer", ub));
    }
}

