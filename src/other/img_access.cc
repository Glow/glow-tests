#include "../tests.hh"

namespace
{
struct MyImage
{
    std::vector<int> colors;
    int w, h;

    int& at1(int x, int y)
    {
        if (w <= 0 || h <= 0 || colors.empty())
        {
            std::cerr << "Invalid img" << std::endl;
            return w;
        }

        while (x < 0)
            x += w;
        while (x >= w)
            x -= w;
        while (y < 0)
            y += h;
        while (y >= h)
            y -= h;

        return colors[y * w + x];
    }

    int& at2(int x, int y)
    {
        x = (x + w) % w;
        y = (x + h) % h;
        return colors[y * w + x];
    }
};
}

TEST(ImageAccess, Benchmark1)
{
    MyImage img;
    img.w = 128;
    img.h = 128;
    img.colors.resize(img.w * img.h);

    for (int i = 0; i < 100; ++i)
    {
        for (auto y = 0; y < img.h; ++y)
            for (auto x = 0; x < img.w; ++x)
            {
                img.at1(x, y) = x * y;
            }
        for (auto y = 0; y < img.h; ++y)
            for (auto x = 0; x < img.w; ++x)
            {
                img.at1(x, y) -= 17;
            }
        for (auto y = 0; y < img.h; ++y)
            for (auto x = 0; x < img.w; ++x)
            {
                img.at1(x - y, x + y) = x * y;
            }
    }
}

TEST(ImageAccess, Benchmark2)
{
    MyImage img;
    img.w = 128;
    img.h = 128;
    img.colors.resize(img.w * img.h);

    for (int i = 0; i < 100; ++i)
    {
        for (auto y = 0; y < img.h; ++y)
            for (auto x = 0; x < img.w; ++x)
            {
                img.at2(x, y) = x * y;
            }
        for (auto y = 0; y < img.h; ++y)
            for (auto x = 0; x < img.w; ++x)
            {
                img.at2(x, y) -= 17;
            }
        for (auto y = 0; y < img.h; ++y)
            for (auto x = 0; x < img.w; ++x)
            {
                img.at2(x - y, x + y) = x * y;
            }
    }
}
