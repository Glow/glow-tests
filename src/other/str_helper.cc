#include "tests.hh"

#include "glow/common/str_utils.hh"

using namespace glow;

TEST(Other, StringHelper)
{
    ASSERT_TRUE(util::endswith("blaa", "laa"));
    ASSERT_FALSE(util::endswith("laa", "blaa"));

    ASSERT_EQ(util::fileEndingOf("/path/to\\myfile.foo.png"), ".png");
    ASSERT_EQ(util::fileEndingOf("/path\\to/myfile"), "");
    ASSERT_EQ(util::fileEndingOf("/path\\to.png/myfile"), "");
    ASSERT_EQ(util::fileEndingOf("/path\\to/myfile.png"), ".png");
}
