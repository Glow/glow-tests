#include "tests.hh"

#include <glm/glm.hpp>
#include <glm/ext.hpp>

#include <glow/common/str_utils.hh>
#include <glow/objects/Program.hh>
#include <glow/objects/Texture2D.hh>
#include <glow/objects/Framebuffer.hh>

#include <glow-extras/geometry/Cube.hh>

GLOW_TEST(Scene, Cube)
{
    auto tex = Texture2D::create(512, 512, GL_RGB);
    auto fb = Framebuffer::create({{"fColor", tex}});
    fb->bind().attachDepth();
    auto cube = geometry::Cube<>().generate();
    auto shader = Program::createFromFile(util::pathOf(__FILE__) + "/cube");

    { // draw
        auto _fb = fb->bind();
        auto _shader = shader->use();

        glEnable(GL_DEPTH_TEST);
        glClearColor(0, 0, 0, 1);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        _shader.setUniform("uModelMatrix", glm::mat4());
        _shader.setUniform("uViewMatrix", glm::lookAt(glm::vec3(1, 4, 2), glm::vec3(0, 0, 0), glm::vec3(0, 1, 0)));
        _shader.setUniform("uProjectionMatrix", glm::perspective(glm::radians(75.f), 1.f, .1f, 100.f));

        cube->bind().draw();
    }

    // tex->bind().writeToFile("/tmp/cube.png");
}
