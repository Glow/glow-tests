#include "tests.hh"

#include <fstream>

#include <glm/glm.hpp>

#include <glow/std140.hh>

#include <fmt/format.hh>

#include <glow/common/log.hh>
#include <glow/common/scoped_gl.hh>
#include <glow/common/str_utils.hh>
#include <glow/common/stream_readall.hh>

#include <glow/objects/TimerQuery.hh>
#include <glow/objects/Program.hh>
#include <glow/objects/Shader.hh>
#include <glow/objects/Framebuffer.hh>
#include <glow/objects/ShaderStorageBuffer.hh>
#include <glow/objects/TextureRectangle.hh>
#include <glow/data/TextureData.hh>

#include <glow-extras/geometry/Quad.hh>

namespace
{
using namespace glow;

struct Particle140
{
    std140vec3 pos;
    std140mat3 frame;
};

const bool extensiveTest = false;
}

GLOW_TEST(RandomVRAM, ComputeSSBO_LocalSize)
{
    std::fstream fileRandom(util::pathOf(__FILE__) + "/vram-random-compute-ssbo.csh");
    std::fstream fileSerial(util::pathOf(__FILE__) + "/vram-serial-compute-ssbo.csh");
    auto srcRandom = util::readall(fileRandom);
    auto srcSerial = util::readall(fileSerial);

    const int reps = 10;
    const int maxLocalSize = extensiveTest ? 64 : 32;
    const int localSizeInc = extensiveTest ? 2 : 4;
    const int maxVramMB = extensiveTest ? 256 : 32;
    const int vramInc = 16;

    std::vector<int> localSizes;
    if (localSizeInc > 1)
        localSizes.push_back(1);
    for (auto ls = localSizeInc; ls <= maxLocalSize; ls += localSizeInc)
        localSizes.push_back(ls);

    std::vector<std::vector<std::string>> table;

    { // first col
        std::vector<std::string> col;
        col.push_back("Local Size");
        for (auto localSize : localSizes)
            col.push_back(fmt::format("{}", localSize));
        table.push_back(col);
    }

    std::vector<std::vector<std::string>> randomCols;
    std::vector<std::vector<std::string>> serialCols;

    for (auto vramMB = vramInc; vramMB <= maxVramMB; vramMB += vramInc)
    {
        if (extensiveTest)
            debug() << "VRAM " << vramMB << " MB";
        uint32_t vramSize = vramMB * 1024 * 1024;

        std::vector<std::string> colRandom;
        colRandom.push_back(fmt::format("{} MB R", vramMB));
        std::vector<std::string> colSerial;
        colSerial.push_back(fmt::format("{} MB S", vramMB));

        auto ssboIn = ShaderStorageBuffer::create();
        auto ssboOut = ShaderStorageBuffer::create();
        uint32_t particleCnt = vramSize / sizeof(Particle140);
        ssboIn->bind().reserve(vramSize);
        ssboOut->bind().reserve(vramSize);

        TimerQuery q;

        for (auto localSize : localSizes)
        {
            if (extensiveTest)
                debug() << "  Local Size " << localSize;
            auto prefix = fmt::format("#version 450 core\n"
                                      "layout(local_size_x = {}, local_size_y = 1, local_size_z = 1) in;\n",
                                      localSize);
            auto shaderRandom = Program::create({Shader::createFromSource(GL_COMPUTE_SHADER, prefix + srcRandom)});
            auto shaderSerial = Program::create({Shader::createFromSource(GL_COMPUTE_SHADER, prefix + srcSerial)});

            shaderRandom->setShaderStorageBuffer("bIn", ssboIn);
            shaderRandom->setShaderStorageBuffer("bOut", ssboOut);

            shaderSerial->setShaderStorageBuffer("bIn", ssboIn);
            shaderSerial->setShaderStorageBuffer("bOut", ssboOut);

            double timeRandom, timeSerial;

            {
                auto s = shaderRandom->use();
                s.setUniform("uParticleCount", particleCnt);
                s.compute(); // once

                q.begin();
                for (int i = 0; i < reps; ++i)
                    s.compute(particleCnt / localSize);
                q.end();
                timeRandom = TimerQuery::toSeconds(q.getResult64()) / reps;
                colRandom.push_back(fmt::format("{}", timeRandom * 1000.));
            }
            {
                auto s = shaderSerial->use();
                s.setUniform("uParticleCount", particleCnt);
                s.compute(); // once

                q.begin();
                for (int i = 0; i < reps; ++i)
                    s.compute(particleCnt / localSize);
                q.end();
                timeSerial = TimerQuery::toSeconds(q.getResult64()) / reps;
                colSerial.push_back(fmt::format("{}", timeSerial * 1000.));
            }

            /*debug() << "Group Size " << localSize << ": \tDuration: " << (int)(timeRandom * 10000.) / 10. << " vs "
                     << (int)(timeSerial * 10000.) / 10. << " ms";*/
        }

        serialCols.push_back(colSerial);
        randomCols.push_back(colRandom);
    }

    for (auto const& v : serialCols)
        table.push_back(v);
    for (auto const& v : randomCols)
        table.push_back(v);

    // write table
    if (extensiveTest)
    {
        auto const filename = "/tmp/ComputeSSBO_LocalSize.csv";
        {
            std::ofstream file(filename);
            for (auto y = 0u; y < table[0].size(); ++y)
            {
                for (auto x = 0u; x < table.size(); ++x)
                {
                    if (x > 0)
                        file << ";";
                    file << table[x][y];
                }
                file << "\n";
            }
        }
        debug() << "Wrote to " << filename;
    }
}

GLOW_TEST(RandomVRAM, ComputeSSBO_VsSerial)
{
    std::fstream fileRandom(util::pathOf(__FILE__) + "/vram-random-compute-ssbo.csh");
    std::fstream fileSerial(util::pathOf(__FILE__) + "/vram-serial-compute-ssbo.csh");
    auto srcRandom = util::readall(fileRandom);
    auto srcSerial = util::readall(fileSerial);

    const int reps = 10;
    const int localSize = 4;
    const int maxVramMB = extensiveTest ? 512 : 64;
    const int vramInc = 16;

    std::vector<std::vector<std::string>> table;

    { // first col
        std::vector<std::string> col;
        col.push_back("Buffer Size");
        for (auto vramMB = vramInc; vramMB <= maxVramMB; vramMB += vramInc)
            col.push_back(fmt::format("{}", vramMB));
        table.push_back(col);
    }


    std::vector<std::string> colRandom;
    colRandom.push_back("Random");
    std::vector<std::string> colSerial;
    colSerial.push_back("Serial");

    for (auto vramMB = vramInc; vramMB <= maxVramMB; vramMB += vramInc)
    {
        if (extensiveTest)
            debug() << "VRAM " << vramMB << " MB";
        uint32_t vramSize = vramMB * 1024 * 1024;

        auto ssboIn = ShaderStorageBuffer::create();
        auto ssboOut = ShaderStorageBuffer::create();
        uint32_t particleCnt = vramSize / sizeof(Particle140);
        ssboIn->bind().reserve(vramSize);
        ssboOut->bind().reserve(vramSize);

        TimerQuery q;

        {
            if (extensiveTest)
                debug() << "  Local Size " << localSize;
            auto prefix = fmt::format("#version 450 core\n"
                                      "layout(local_size_x = {}, local_size_y = 1, local_size_z = 1) in;\n",
                                      localSize);
            auto shaderRandom = Program::create({Shader::createFromSource(GL_COMPUTE_SHADER, prefix + srcRandom)});
            auto shaderSerial = Program::create({Shader::createFromSource(GL_COMPUTE_SHADER, prefix + srcSerial)});

            shaderRandom->setShaderStorageBuffer("bIn", ssboIn);
            shaderRandom->setShaderStorageBuffer("bOut", ssboOut);

            shaderSerial->setShaderStorageBuffer("bIn", ssboIn);
            shaderSerial->setShaderStorageBuffer("bOut", ssboOut);

            double timeRandom, timeSerial;

            {
                auto s = shaderRandom->use();
                s.setUniform("uParticleCount", particleCnt);
                s.compute(); // once

                q.begin();
                for (int i = 0; i < reps; ++i)
                    s.compute(particleCnt / localSize);
                q.end();
                timeRandom = TimerQuery::toSeconds(q.getResult64()) / reps;
                colRandom.push_back(fmt::format("{}", timeRandom * 1000.));
            }
            {
                auto s = shaderSerial->use();
                s.setUniform("uParticleCount", particleCnt);
                s.compute(); // once

                q.begin();
                for (int i = 0; i < reps; ++i)
                    s.compute(particleCnt / localSize);
                q.end();
                timeSerial = TimerQuery::toSeconds(q.getResult64()) / reps;
                colSerial.push_back(fmt::format("{}", timeSerial * 1000.));
            }

            /*debug() << "Group Size " << localSize << ": \tDuration: " << (int)(timeRandom * 10000.) / 10. << " vs "
                     << (int)(timeSerial * 10000.) / 10. << " ms";*/
        }
    }

    table.push_back(colSerial);
    table.push_back(colRandom);

    // write table
    if (extensiveTest)
    {
        auto const filename = "/tmp/ComputeSSBO_VsSerial.csv";
        {
            std::ofstream file(filename);
            for (auto y = 0u; y < table[0].size(); ++y)
            {
                for (auto x = 0u; x < table.size(); ++x)
                {
                    if (x > 0)
                        file << ";";
                    file << table[x][y];
                }
                file << "\n";
            }
        }
        debug() << "Wrote to " << filename;
    }
}

GLOW_TEST(RandomVRAM, Texture)
{
    auto shaderRandom = Program::createFromFiles(
        {util::pathOf(__FILE__) + "/vram-random-texture.fsh", util::pathOf(__FILE__) + "/vram-texture.vsh"});
    auto shaderSerial = Program::createFromFiles(
        {util::pathOf(__FILE__) + "/vram-serial-texture.fsh", util::pathOf(__FILE__) + "/vram-texture.vsh"});
    auto shaderFill = Program::createFromFiles(
        {util::pathOf(__FILE__) + "/vram-texture-fill.fsh", util::pathOf(__FILE__) + "/vram-texture.vsh"});

    const int reps = 10;
    const int maxTexSize = extensiveTest ? 4096 + 128 * 8 : 512;
    const int texInc = 128;
    const int texStart = 128 * 2;

    std::vector<std::vector<std::string>> table;

    { // first col
        std::vector<std::string> col;
        col.push_back("Buffer Size");
        for (auto texSize = texStart; texSize <= maxTexSize; texSize += texInc)
            col.push_back(fmt::format("{}", texSize * texSize * sizeof(glm::vec4) / 1024 / 1024));
        table.push_back(col);
    }


    std::vector<std::string> colRandom;
    colRandom.push_back("Random");
    std::vector<std::string> colSerial;
    colSerial.push_back("Serial");

    auto quad = glow::geometry::Quad<>().generate();
    auto vao = quad->bind();

    glow::restoreDefaultOpenGLState();

    for (auto texSize = texStart; texSize <= maxTexSize; texSize += texInc)
    {
        if (extensiveTest)
            debug() << "VRAM " << texSize << " x " << texSize << " = "
                    << texSize * texSize * sizeof(glm::vec4) / 1024 / 1024 << " MB";

        scoped::viewport vp(0, 0, texSize, texSize);
        auto texIn = TextureRectangle::create(texSize, texSize, GL_RGBA32F);
        auto texOut = TextureRectangle::create(texSize, texSize, GL_RGBA32F);

        {
            auto tf = Framebuffer::create({{"fOut", texIn}});
            auto f = tf->bind();
            auto s = shaderFill->use();
            s.setTexture("uTexIn", texOut);
            vao.draw();
        }

        auto fbo = Framebuffer::create({{"fOut", texOut}});

        {
            auto s = shaderFill->use();
            s.setTexture("uTexIn", texIn);
            vao.draw();
        }

        auto bfbo = fbo->bind();
        TimerQuery q;

        double timeRandom, timeSerial;

        {
            auto s = shaderRandom->use();
            s.setTexture("uTexIn", texIn);
            vao.draw(); // once
            vao.draw(); // twice

            q.begin();
            for (int i = 0; i < reps; ++i)
                vao.draw();
            q.end();
            timeRandom = TimerQuery::toSeconds(q.getResult64()) / reps;
            colRandom.push_back(fmt::format("{}", timeRandom * 1000.));
        }
        {
            auto s = shaderSerial->use();
            s.setTexture("uTexIn", texIn);
            vao.draw(); // once
            vao.draw(); // twice

            q.begin();
            for (int i = 0; i < reps; ++i)
                vao.draw();
            q.end();
            timeSerial = TimerQuery::toSeconds(q.getResult64()) / reps;
            colSerial.push_back(fmt::format("{}", timeSerial * 1000.));
        }
    }

    table.push_back(colSerial);
    table.push_back(colRandom);

    // write table
    if (extensiveTest)
    {
        auto const filename = "/tmp/ComputeSSBO_Texture.csv";
        {
            std::ofstream file(filename);
            for (auto y = 0u; y < table[0].size(); ++y)
            {
                for (auto x = 0u; x < table.size(); ++x)
                {
                    if (x > 0)
                        file << ";";
                    file << table[x][y];
                }
                file << "\n";
            }
        }
        debug() << "Wrote to " << filename;
    }
}

GLOW_TEST(RandomVRAM, TextureImage)
{
    auto shaderRandom = Program::createFromFiles(
        {util::pathOf(__FILE__) + "/vram-random-image.fsh", util::pathOf(__FILE__) + "/vram-texture.vsh"});
    auto shaderSerial = Program::createFromFiles(
        {util::pathOf(__FILE__) + "/vram-serial-image.fsh", util::pathOf(__FILE__) + "/vram-texture.vsh"});
    auto shaderFill = Program::createFromFiles(
        {util::pathOf(__FILE__) + "/vram-texture-fill.fsh", util::pathOf(__FILE__) + "/vram-texture.vsh"});

    const int reps = 10;
    const int maxTexSize = extensiveTest ? 4096 + 128 * 8 : 512;
    const int texInc = 128;
    const int texStart = 128 * 2;

    std::vector<std::vector<std::string>> table;

    { // first col
        std::vector<std::string> col;
        col.push_back("Buffer Size");
        for (auto texSize = texStart; texSize <= maxTexSize; texSize += texInc)
            col.push_back(fmt::format("{}", texSize * texSize * sizeof(glm::vec4) / 1024 / 1024));
        table.push_back(col);
    }


    std::vector<std::string> colRandom;
    colRandom.push_back("Random");
    std::vector<std::string> colSerial;
    colSerial.push_back("Serial");

    auto quad = glow::geometry::Quad<>().generate();
    auto vao = quad->bind();

    glow::restoreDefaultOpenGLState();

    for (auto texSize = texStart; texSize <= maxTexSize; texSize += texInc)
    {
        if (extensiveTest)
            debug() << "VRAM " << texSize << " x " << texSize << " = "
                    << texSize * texSize * sizeof(glm::vec4) / 1024 / 1024 << " MB";

        scoped::viewport vp(0, 0, texSize, texSize);
        auto texIn = TextureRectangle::createStorageImmutable(texSize, texSize, GL_RGBA32F);
        auto texOut = TextureRectangle::createStorageImmutable(texSize, texSize, GL_RGBA32F);

        {
            auto tf = Framebuffer::create({{"fOut", texIn}});
            auto f = tf->bind();
            auto s = shaderFill->use();
            s.setTexture("uTexIn", texOut);
            vao.draw();
        }

        auto fbo = Framebuffer::create({{"fOut", texOut}});

        {
            auto s = shaderFill->use();
            s.setTexture("uTexIn", texIn);
            vao.draw();
        }

        auto bfbo = fbo->bind();
        TimerQuery q;

        double timeRandom, timeSerial;

        {
            auto s = shaderRandom->use();
            s.setImage(0, texIn);
            vao.draw(); // once
            vao.draw(); // twice

            q.begin();
            for (int i = 0; i < reps; ++i)
                vao.draw();
            q.end();
            timeRandom = TimerQuery::toSeconds(q.getResult64()) / reps;
            colRandom.push_back(fmt::format("{}", timeRandom * 1000.));
        }
        {
            auto s = shaderSerial->use();
            s.setImage(0, texIn);
            vao.draw(); // once
            vao.draw(); // twice

            q.begin();
            for (int i = 0; i < reps; ++i)
                vao.draw();
            q.end();
            timeSerial = TimerQuery::toSeconds(q.getResult64()) / reps;
            colSerial.push_back(fmt::format("{}", timeSerial * 1000.));
        }
    }

    table.push_back(colSerial);
    table.push_back(colRandom);

    // write table
    if (extensiveTest)
    {
        auto const filename = "/tmp/ComputeSSBO_Image.csv";
        {
            std::ofstream file(filename);
            for (auto y = 0u; y < table[0].size(); ++y)
            {
                for (auto x = 0u; x < table.size(); ++x)
                {
                    if (x > 0)
                        file << ";";
                    file << table[x][y];
                }
                file << "\n";
            }
        }
        debug() << "Wrote to " << filename;
    }
}
