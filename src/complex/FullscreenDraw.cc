#include "tests.hh"

#include <glm/glm.hpp>

#include <glow/objects/VertexArray.hh>
#include <glow/objects/Shader.hh>
#include <glow/objects/Program.hh>
#include <glow/objects/ArrayBuffer.hh>
#include <glow/objects/ElementArrayBuffer.hh>
#include <glow/objects/Texture2D.hh>
#include <glow/objects/Framebuffer.hh>

GLOW_TEST(FullscreenDraw, Constant)
{
    auto vertexShader = Shader::createFromSource(GL_VERTEX_SHADER, "#version 440 core\n"
                                                             "in vec2 aPosition;\n"
                                                             "out vec2 vPosition;\n"
                                                             "void main() {\n"
                                                             "  vPosition = aPosition * .5 + .5;\n"
                                                             "  gl_Position = vec4(aPosition, 0.0, 1.0);\n"
                                                             "}\n");

    auto fragmentShader = Shader::createFromSource(GL_FRAGMENT_SHADER, "#version 440 core\n"
                                                                 "in vec2 vPosition;\n"
                                                                 "out vec3 fColor;\n"
                                                                 "void main() {\n"
                                                                 "  fColor = vec3(-1, 0, 1);\n"
                                                                 "}\n");

    auto shader = Program::create({vertexShader, fragmentShader});

    auto ab = ArrayBuffer::create();
    ab->defineAttribute<glm::vec2>("aPosition");
    ab->bind().setData(std::vector<glm::vec2>{
        {-1, -1}, // lower-left
        {+1, -1}, // lower-right
        {+1, +1}, // upper-right
        {-1, +1}, // upper-left
    });

    auto eab = ElementArrayBuffer::create(std::vector<uint8_t>{
        0, 1, 2, // face 1
        0, 2, 3, // face 2
    });

    auto quad = VertexArray::create(ab, eab);

    auto tex = Texture2D::create(4, 4, GL_RGB32F);
    auto fbo = Framebuffer::create({{"fColor", tex}});

    {
        auto bfbo = fbo->bind();
        auto ushader = shader->use();

        quad->bind().draw();
    }

    auto data = tex->bind().getData<glm::vec3>();

    ASSERT_EQ(data.size(), 4u * 4u);
    for (auto const& c : data)
        ASSERT_EQ(c, glm::vec3(-1, 0, 1));
}
