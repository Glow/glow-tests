#include "../tests.hh"

#include <glow/common/scoped_gl.hh>
#include <glow/common/gl_helper.hh>

GLOW_TEST(Glow, ScopedGL)
{
    restoreDefaultOpenGLState();

    // enable-disable
    ASSERT_FALSE(glIsEnabled(GL_BLEND));
    ASSERT_FALSE(glIsEnabled(GL_DEPTH_TEST));

    {
        scoped::enable _blend(GL_BLEND);
        scoped::enable _depth(GL_DEPTH_TEST);

        ASSERT_TRUE(glIsEnabled(GL_BLEND));
        ASSERT_TRUE(glIsEnabled(GL_DEPTH_TEST));

        {
            scoped::disable _blend(GL_BLEND);

            ASSERT_FALSE(glIsEnabled(GL_BLEND));
            ASSERT_TRUE(glIsEnabled(GL_DEPTH_TEST));
        }

        ASSERT_TRUE(glIsEnabled(GL_BLEND));
        ASSERT_TRUE(glIsEnabled(GL_DEPTH_TEST));
    }

    ASSERT_FALSE(glIsEnabled(GL_BLEND));
    ASSERT_FALSE(glIsEnabled(GL_DEPTH_TEST));

    // functions
    ASSERT_EQ(glGetEnum(GL_CULL_FACE_MODE), (GLenum)GL_BACK);
    {
        scoped::cullFace _(GL_FRONT);
        ASSERT_EQ(glGetEnum(GL_CULL_FACE_MODE), (GLenum)GL_FRONT);
        {
            scoped::cullFace _(GL_FRONT_AND_BACK);
            ASSERT_EQ(glGetEnum(GL_CULL_FACE_MODE), (GLenum)GL_FRONT_AND_BACK);
        }
        ASSERT_EQ(glGetEnum(GL_CULL_FACE_MODE), (GLenum)GL_FRONT);
    }
    ASSERT_EQ(glGetEnum(GL_CULL_FACE_MODE), (GLenum)GL_BACK);

    // pixel store
    ASSERT_EQ(glGetInt(GL_PACK_ALIGNMENT), 4);
    {
        scoped::packAlignment _(1);
        ASSERT_EQ(glGetInt(GL_PACK_ALIGNMENT), 1);
        {
            scoped::packAlignment _(2);
            ASSERT_EQ(glGetInt(GL_PACK_ALIGNMENT), 2);
        }
        ASSERT_EQ(glGetInt(GL_PACK_ALIGNMENT), 1);
    }
    ASSERT_EQ(glGetInt(GL_PACK_ALIGNMENT), 4);
}
