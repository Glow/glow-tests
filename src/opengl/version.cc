#include "tests.hh"

GLOW_TEST(Glow, Version)
{
    ASSERT_GE(GLVersion.major, 4);
    ASSERT_GE(GLVersion.minor, 3);
}
