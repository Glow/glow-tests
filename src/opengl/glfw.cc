#include "tests.hh"

#include <glow/gl.hh>
#include <glow/glow.hh>

TEST(Glfw, Init)
{
    ASSERT_TRUE(glfwInit());

    glfwWindowHint(GLFW_VISIBLE, GLFW_FALSE);
    auto window = glfwCreateWindow(512, 512, "GlfwTest", nullptr, nullptr);

    ASSERT_NE(window, nullptr);

    glfwMakeContextCurrent(window);

    ASSERT_TRUE(glow::initGLOW());

    glfwSwapBuffers(window);

    glfwDestroyWindow(window);
    glfwTerminate();
}

TEST(Glfw, InitDebug)
{
    ASSERT_TRUE(glfwInit());

    glfwWindowHint(GLFW_VISIBLE, GLFW_FALSE);
    glfwWindowHint(GLFW_OPENGL_DEBUG_CONTEXT, GLFW_TRUE);
    auto window = glfwCreateWindow(512, 512, "GlfwTest", nullptr, nullptr);

    ASSERT_NE(window, nullptr);

    glfwMakeContextCurrent(window);

    ASSERT_TRUE(glow::initGLOW());

    glfwSwapBuffers(window);

    glfwDestroyWindow(window);
    glfwTerminate();
}

TEST(Glfw, InitForwardCompat)
{
    ASSERT_TRUE(glfwInit());

    glfwWindowHint(GLFW_VISIBLE, GLFW_FALSE);
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GLFW_TRUE);
    glfwWindowHint(GLFW_OPENGL_CORE_PROFILE, GLFW_TRUE);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 4);
    auto window = glfwCreateWindow(512, 512, "GlfwTest", nullptr, nullptr);

    ASSERT_NE(window, nullptr);

    glfwMakeContextCurrent(window);

    ASSERT_TRUE(glow::initGLOW());

    glfwSwapBuffers(window);

    glfwDestroyWindow(window);
    glfwTerminate();
}

TEST(Glfw, InitProfilesAndVersions)
{
    ASSERT_TRUE(glfwInit());

    glfwWindowHint(GLFW_VISIBLE, GLFW_FALSE);
    for (auto profile : {GLFW_OPENGL_CORE_PROFILE, GLFW_OPENGL_COMPAT_PROFILE})
    {
        glfwWindowHint(GLFW_OPENGL_PROFILE, profile);
        for (auto version : std::vector<std::pair<int, int>>{
                 {3, 3}, {4, 0}, {4, 1}, {4, 2}, {4, 3}, {4, 4}, {4, 5},
             })
        {
            glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, version.first);
            glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, version.second);
            auto window = glfwCreateWindow(512, 512, "GlfwTest", nullptr, nullptr);

            ASSERT_NE(window, nullptr);

            glfwMakeContextCurrent(window);

            ASSERT_TRUE(glow::initGLOW());

            // version check
            ASSERT_TRUE(GLVersion.major > version.first || GLVersion.minor >= version.second);

            glfwSwapBuffers(window);

            glfwDestroyWindow(window);
        }
    }
    glfwTerminate();
}
