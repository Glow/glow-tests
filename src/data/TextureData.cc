#include "tests.hh"

#include <glow/common/str_utils.hh>
#include <glow/data/SurfaceData.hh>
#include <glow/data/TextureData.hh>

using namespace glow;

GLOW_TEST(TextureData, STB)
{
    for (std::string const& file : {"test-rgb.bmp",  //
                                    "test-rgb.jpg",  //
                                    "test-rgb.jpeg", //
                                    "test-rgb.ppm",  //
                                    "test-rgb.tga",  //
                                    "test-grey.bmp", //
                                    "test-grey.jpg", //
                                    "test-grey.tga"})
    {
        auto tex = TextureData::createFromFile(util::pathOf(__FILE__) + "/textures/" + file, ColorSpace::Linear);
        ASSERT_NE(tex, nullptr);
        ASSERT_EQ(tex->getWidth(), 3);
        ASSERT_EQ(tex->getHeight(), 2);
        ASSERT_EQ(tex->getSurfaces().size(), 1u);
        auto surf = tex->getSurfaces()[0];
        ASSERT_EQ(surf->getMipmapLevel(), 0);
        ASSERT_EQ(surf->getWidth(), 3);
        ASSERT_EQ(surf->getHeight(), 2);
        ASSERT_EQ(surf->getType(), (GLenum)GL_UNSIGNED_BYTE);
        ASSERT_EQ(surf->getFormat(), (GLenum)GL_RGB);

        auto surfA = surf->convertTo(GL_RGBA);
        ASSERT_EQ(surfA->getMipmapLevel(), 0);
        ASSERT_EQ(surfA->getWidth(), 3);
        ASSERT_EQ(surfA->getHeight(), 2);
        ASSERT_EQ(surfA->getType(), (GLenum)GL_UNSIGNED_BYTE);
        ASSERT_EQ(surfA->getFormat(), (GLenum)GL_RGBA);
        for (auto i = 0; i < surf->getWidth() * surf->getHeight(); ++i)
        {
            ASSERT_EQ(surf->getData()[i * 3 + 0], surfA->getData()[i * 4 + 0]);
            ASSERT_EQ(surf->getData()[i * 3 + 1], surfA->getData()[i * 4 + 1]);
            ASSERT_EQ(surf->getData()[i * 3 + 2], surfA->getData()[i * 4 + 2]);
            ASSERT_EQ((char)0xFF, surfA->getData()[i * 4 + 3]);
        }
    }

    for (std::string const& file : {"test-rgba.tga", //
                                    "test-rgb.gif"})
    {
        auto tex = TextureData::createFromFile(util::pathOf(__FILE__) + "/textures/" + file, ColorSpace::Linear);
        ASSERT_NE(tex, nullptr);
        ASSERT_EQ(tex->getWidth(), 3);
        ASSERT_EQ(tex->getHeight(), 2);
        ASSERT_EQ(tex->getSurfaces().size(), 1u);
        auto surf = tex->getSurfaces()[0];
        ASSERT_EQ(surf->getMipmapLevel(), 0);
        ASSERT_EQ(surf->getWidth(), 3);
        ASSERT_EQ(surf->getHeight(), 2);
        ASSERT_EQ(surf->getType(), (GLenum)GL_UNSIGNED_BYTE);
        ASSERT_EQ(surf->getFormat(), (GLenum)GL_RGBA);

        auto surfA = surf->convertTo(GL_RG);
        ASSERT_EQ(surfA->getMipmapLevel(), 0);
        ASSERT_EQ(surfA->getWidth(), 3);
        ASSERT_EQ(surfA->getHeight(), 2);
        ASSERT_EQ(surfA->getType(), (GLenum)GL_UNSIGNED_BYTE);
        ASSERT_EQ(surfA->getFormat(), (GLenum)GL_RG);
        for (auto i = 0; i < surf->getWidth() * surf->getHeight(); ++i)
        {
            ASSERT_EQ(surf->getData()[i * 4 + 0], surfA->getData()[i * 2 + 0]);
            ASSERT_EQ(surf->getData()[i * 4 + 1], surfA->getData()[i * 2 + 1]);
        }

        auto surfB = surfA->convertTo(GL_RGB);
        ASSERT_EQ(surfB->getMipmapLevel(), 0);
        ASSERT_EQ(surfB->getWidth(), 3);
        ASSERT_EQ(surfB->getHeight(), 2);
        ASSERT_EQ(surfB->getType(), (GLenum)GL_UNSIGNED_BYTE);
        ASSERT_EQ(surfB->getFormat(), (GLenum)GL_RGB);
        for (auto i = 0; i < surf->getWidth() * surf->getHeight(); ++i)
        {
            ASSERT_EQ(surfB->getData()[i * 3 + 0], surfA->getData()[i * 2 + 0]);
            ASSERT_EQ(surfB->getData()[i * 3 + 1], surfA->getData()[i * 2 + 1]);
            ASSERT_EQ(surfB->getData()[i * 3 + 2], 0x00);
        }

        auto surfC = surfA->convertTo(GL_RGBA);
        ASSERT_EQ(surfC->getMipmapLevel(), 0);
        ASSERT_EQ(surfC->getWidth(), 3);
        ASSERT_EQ(surfC->getHeight(), 2);
        ASSERT_EQ(surfC->getType(), (GLenum)GL_UNSIGNED_BYTE);
        ASSERT_EQ(surfC->getFormat(), (GLenum)GL_RGBA);
        for (auto i = 0; i < surf->getWidth() * surf->getHeight(); ++i)
        {
            ASSERT_EQ(surfC->getData()[i * 4 + 0], surfA->getData()[i * 2 + 0]);
            ASSERT_EQ(surfC->getData()[i * 4 + 1], surfA->getData()[i * 2 + 1]);
            ASSERT_EQ(surfC->getData()[i * 4 + 2], 0x00);
            ASSERT_EQ(surfC->getData()[i * 4 + 3], (char)0xFF);
        }
    }
}

TEST(TextureData, LodePNG)
{
    for (std::string const& file : {"test-rgb.png",  //
                                    "test-rgba.png", //
                                    "test-grey.png"})
    {
        auto tex = TextureData::createFromFile(util::pathOf(__FILE__) + "/textures/" + file, ColorSpace::Linear);
        ASSERT_NE(tex, nullptr);
        ASSERT_EQ(tex->getWidth(), 3);
        ASSERT_EQ(tex->getHeight(), 2);
        ASSERT_EQ(tex->getSurfaces().size(), 1u);
        auto surf = tex->getSurfaces()[0];
        ASSERT_EQ(surf->getMipmapLevel(), 0);
        ASSERT_EQ(surf->getWidth(), 3);
        ASSERT_EQ(surf->getHeight(), 2);
        ASSERT_EQ(surf->getType(), (GLenum)GL_UNSIGNED_BYTE);
        ASSERT_EQ(surf->getFormat(), (GLenum)GL_RGBA);
    }
}
